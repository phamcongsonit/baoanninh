<?php get_header();?>
    <div class="main_container">
        <div class="container">
            <div class="row">
                <div class="main_container_sidebar">
                    <h1 class="title_style2">
                        <?php _e('Tìm kiếm:','devvn');?> <?php echo get_search_query();?>
                    </h1>
                    <?php if(have_posts()):?>
                        <div class="archive_list_posts">
                            <?php
                            while(have_posts()):the_post();
                            ?>
                            <div class="news_box news_img_title_styl01">
                                <?php if(has_post_thumbnail()):?><div class="news_box_img"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a></div><?php endif;?>
                                <div class="news_title_desc">
                                    <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                                    <div class="news_box_date"><?php echo get_the_date('d/m/Y h:i');?></div>
                                    <div class="news_box_desc"><?php the_excerpt();?></div>
                                </div>
                            </div>
                            <?php endwhile;?>
                        </div>
                        <?php wp_corenavi_table();?>
                    <?php else:?>
                        <?php get_template_part('content','none');?>
                    <?php endif;?>
                </div>
                <div class="sidebar_container">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-sidebar')) : ?><?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>