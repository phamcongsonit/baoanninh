<footer class="footer" itemscope="" itemtype="http://schema.org/WPFooter">
    <div class="footer_menu">
        <div class="container">
            <?php if(has_nav_menu('footer')):?>
                <div class="left">
                    <?php wp_nav_menu(array('theme_location'  => 'footer','container'=> '', 'menu_id'=>'menu-footer-menu'));?>
                </div>
            <?php endif;?>
            <div class="right">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-right-sidebar')) : ?><?php endif; ?>
            </div>
        </div>
    </div>
    <div class="footer_infor">
        <div class="container">
            <div class="footer_infor_logo">
                <?php
                $logo = get_field('logo_footer','option');
                $logo = ($logo) ? $logo : TEMP_URL.'/images/logo.png';
                ?>
                <a href="<?php echo esc_url(home_url('/'))?>" title="<?php bloginfo('description')?>"><img src="<?=$logo?>" alt="<?php bloginfo('name')?>"/></a>
            </div>
            <div class="footer_infor_text">
                <?php echo get_field('footer_infor','option');?>
            </div>
            <div class="footer_infor_contact"><?php echo get_field('footer_contact','option');?></div>
        </div>
    </div>
    <?php
    $footer_copyright = get_field('footer_copyright','option');
    if($footer_copyright):
    ?>
    <div class="footer_copyright">
        <div class="container">
            <?php echo $footer_copyright;?>
        </div>
    </div>
    <?php endif;?>
</footer>
<div class="over_wrap"></div>
<a href="javascript:;" id="gototop"><img src="<?php echo get_template_directory_uri();?>/images/icon_gototop.png" alt="lên đầu trang"></a>
<?php wp_footer(); ?>
</body>
</html>