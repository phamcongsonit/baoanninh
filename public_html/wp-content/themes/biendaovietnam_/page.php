<?php get_header();?>
<?php while(have_posts()):the_post();?>
<div class="main_container">
    <div class="container">
        <div class="row">
            <div class="main_container_sidebar">
                <div class="single_main_container">
                    <div class="tinymce">
                        <?php the_content();?>
                    </div>
                </div>
            </div>
            <div class="sidebar_container">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-sidebar')) : ?><?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php endwhile;?>
<?php get_footer();?>