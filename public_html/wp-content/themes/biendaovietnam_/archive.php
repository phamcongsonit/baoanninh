<?php get_header();?>
<div class="main_container">
    <div class="container">
        <div class="row">
            <div class="main_container_sidebar">
                <?php if(have_posts()):?>
                    <div class="archive_list_posts">
                        <?php
                        global $wp_query;
                        $stt = 1;
                        $post_count = $wp_query->post_count;
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        while(have_posts()):the_post();
                        ?>
                        <?php if($stt == 1 && $paged == 1):?>
                        <div class="archive_posts_feature">
                        <?php elseif(($stt == 6 && $paged == 1 ) ||  ($paged > 1 && $stt == 1)):?>
                        <div class="archive_posts_items"><div class="archive_posts_items_left">
                        <?php endif;?>
                            <div class="news_box news_img_title_styl01">
                                <?php if(has_post_thumbnail()):?><div class="news_box_img"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a></div><?php endif;?>
                                <div class="news_title_desc">
                                    <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                                    <div class="news_box_date"><?php echo get_the_date('d/m/Y h:i');?></div>
                                    <div class="news_box_desc"><?php the_excerpt();?></div>
                                </div>
                            </div>
                        <?php if(($stt == 5 || ($stt == $post_count && $post_count <= 5 ))  && $paged == 1):?>
                        </div>
                        <?php elseif(($stt >= 6 && $stt == $post_count  && $paged == 1) || ($stt == $post_count  && $paged > 1)):?>
                        </div>
                        <div class="archive_posts_items_right">
                            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('archive-small-sidebar')) : ?><?php endif; ?>
                        </div>
                        </div>
                        <?php endif;?>
                        <?php $stt++; endwhile;?>
                    </div>
                    <?php wp_corenavi_table();?>
                <?php else:?>
                    <?php get_template_part('content','none');?>
                <?php endif;?>
            </div>
            <div class="sidebar_container">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-sidebar')) : ?><?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>