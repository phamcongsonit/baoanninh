<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head itemscope itemtype="http://schema.org/WebSite">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
	<link rel="icon" href="/favicon.png" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.png" type="image/x-icon" />
    <?php wp_head(); ?>
</head>
<body <?php body_class();?> itemscope itemtype="http://schema.org/WebPage">
<?php do_action('devvn_after_body');?>
<div class="header_top">
    <div class="container">
        <div class="header_top_left">
            <div class="header_tl_date"><?php get_current_day();?></div>
            <?php if(has_nav_menu('header_tl')):?>
                <div class="menu_header_tl">
                    <?php wp_nav_menu(array('theme_location'  => 'header_tl','container'=> ''));?>
                </div>
            <?php endif;?>
        </div>
        <div class="header_top_right">
            <?php do_action('header_top_right');?>
            <?php if(has_nav_menu('header_tr')):?>
                <div class="menu_header_tr">
                    <?php wp_nav_menu(array('theme_location'  => 'header_tr','container'=> ''));?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<header class="header" itemscope itemtype="http://schema.org/WPHeader">
    <div class="container">
        <div class="logo">
            <?php
            $logo = get_field('logo','option');
            $logo = ($logo) ? $logo : TEMP_URL.'/images/logo.png';
            ?>
            <a href="<?php echo esc_url(home_url('/'))?>" title="<?php bloginfo('description')?>"><img src="<?=$logo?>" alt="<?php bloginfo('name')?>"/></a>
        </div>
        <button type="button" class="button_menu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <?php
        $header_banner = get_field('header_banner','option');
        $header_banner_link = get_field('header_banner_link','option');
            ?>
            <div class="header_banner">
               <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- banner baoanninhvn.net -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="8846494209"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
					
            </div>
    </div>
</header>
<?php if(has_nav_menu('header')):?>
    <div class="header_nav">
        <div class="container">
            <div class="menu_header" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
                <div class="search">
                    <?php echo get_search_form();?>
                </div>
                <?php wp_nav_menu(array('theme_location'  => 'header','container'=> ''));?>
            </div>
        </div>
    </div>
<?php endif;?>
<div id="toolbar">
    <div class="container">
        <div class="left">
            <?php do_action('left_header_bottom');?>
        </div>
        <div class="right">
            <?php
            $page_for_posts = get_option( 'page_for_posts' );
            if($page_for_posts):
                ?>
                <a class="tin24h" href="<?php echo get_the_permalink($page_for_posts);?>" title="<?php echo get_the_title($page_for_posts);?>"><?php echo get_the_title($page_for_posts);?></a>
            <?php endif;?>
            <div class="search">
                <?php echo get_search_form();?>
            </div>
        </div>
    </div>
</div>
