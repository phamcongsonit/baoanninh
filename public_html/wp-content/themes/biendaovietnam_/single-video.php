<?php get_header();?>
<?php while(have_posts()):the_post();
$post_view_type = get_field('post_view_type');
$video_type = get_field('video_type');
$youtube_video_id = get_field('youtube_video_id');
$youtube_video_url = get_field('youtube_video_url');
if($video_type ==  'youtubeUrl' && $youtube_video_url) $youtube_video_id = devvn_getYouTubeVideoId($youtube_video_url);
$iframe_video = get_field('iframe_video');
?>
<div class="main_container">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="video_main_container">
                    <div class="videoWrapper">
                        <?php if($youtube_video_id):?>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_video_id;?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        <?php elseif($video_type ==  'iframe' && $iframe_video):?>
                            <?php echo $iframe_video;?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="video_infor">
                    <h1 class="single_title"><?php the_title();?></h1>
                    <div class="single_toolbar">
                        <div class="news_box_date"><?php echo get_the_date('d/m/Y h:i');?></div>
                        <div class="news_box_views"><?php echo do_shortcode('[post_view]')?></div>
                    </div>
                    <?php if(has_excerpt()):?>
                    <h2 class="excerpt_post"><?php the_excerpt();?></h2>
                    <?php endif;?>
                    <div class="tinymce">
                        <?php the_content();?>
                    </div>
                    <div class="like_share">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>&amp;display=popup&amp;ref=plugin" class="share_fb">Chia sẻ facebook</a>
                        <a target="_blank" href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>" class="share_gg">Chia sẻ google</a>
                    </div>
                </div>
                <div class="after_video_infor">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('single-video-sidebar')) : ?><?php endif; ?>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <?php do_action('devvn_single_video_before_ads');?>
                <div class="after_video_infor after_video_infor_2">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('single-video-sidebar-2')) : ?><?php endif; ?>
                </div>
                <?php do_action('devvn_single_video_after_ads');?>
            </div>
        </div>
    </div>
</div>
<?php
$categories = get_the_category(get_the_ID());
if ($categories){
    echo '<div class="relatedcat"><div class="container">';
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args = array(
        'category__in' => $category_ids,
        'post__not_in' => array(get_the_ID()),
        'posts_per_page' => 9
    );
    $my_query = new wp_query($args);
    if( $my_query->have_posts() ):
        echo '<h2 class="title_box_news"><span>'.__('Xem thêm','devvn').'</span></h2><div class="row">';
        while ($my_query->have_posts()):$my_query->the_post();
            ?>
            <div class="col-sm-4 col-xs-6">
                <div class="news_box">
                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a></div><?php endif;?>
                    <div class="news_title_desc">
                        <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                    </div>
                </div>
            </div>
            <?php
        endwhile;
        echo '</div>';
    endif; wp_reset_query();
    echo '</div></div>';
}
?>
<?php endwhile;?>
<?php get_footer();?>