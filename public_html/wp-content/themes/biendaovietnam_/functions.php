<?php
define( "TEMP_URL" , get_bloginfo('template_url'));
define( "DEVVN_VERSION" ,'1.0');
define( "DEVVN_DEV_MODE" ,true);
if( class_exists('acf') ) { 
	define('GOOLE_MAPS_API', get_field('google_maps_api','option'));
}

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';
require get_template_directory() . '/inc/aq_resizer.php';
require get_template_directory() . '/inc/copyright/copyright_svl.php';
//require get_template_directory() . '/inc/woocommerce_int/woo_int.php';
require get_template_directory() . '/inc/style_script_int.php';

function my_acf_google_map_api( $api ){	
	$api['key'] = GOOLE_MAPS_API;	
	return $api;	
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
/*
 * Setup theme
 */
function devvn_setup() {
	load_theme_textdomain( 'devvn', get_template_directory() . '/languages' );
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	//Add thumbnail to post
	add_theme_support( 'post-thumbnails' );
	//Shortcode in widget
	add_filter('widget_text', 'do_shortcode');
    add_editor_style();
	//menu
	/********
	 * Call: wp_nav_menu(array('theme_location'  => 'header','container'=> ''));
	 * *********/
	register_nav_menus( array(
		'header' => __( 'Header menu', 'devvn' ),
		'header_tl' => __( 'Header Top Left', 'devvn' ),
		'header_tr' => __( 'Header Top Right', 'devvn' ),
		'footer'  => __( 'Footer menu', 'devvn' ),
	));
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	//add Post format
    add_theme_support( 'post-formats',
        array(
            'video',
        )
    );
	//Remove version
	remove_action('wp_head', 'wp_generator');
	//Remove Default WordPress Image Sizes
	function svl_remove_default_image_sizes( $sizes) {
		//unset( $sizes['thumbnail']);
		//unset( $sizes['medium']);
		unset( $sizes['large']);
		unset( $sizes['medium_large']);
		 
		return $sizes;
	}
	add_filter('intermediate_image_sizes_advanced', 'svl_remove_default_image_sizes');
	if ( function_exists( 'add_image_size' ) ) {
		//add_image_size( 'homepage-thumb', 50, 50, true ); //(cropped)
	}
}
add_action( 'after_setup_theme', 'devvn_setup' );
//Sidebar
/*
 <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-right-sidebar')) : ?><?php endif; ?>
 */
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Home Sidebar', 'devvn' ),
        'id' => 'home-sidebar',
        'before_widget' => '<div id="%1$s" class="home-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title-home-sidebar">',
        'after_title'   => '</h3>',
    ));
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'devvn' ),
        'id' => 'main-sidebar',
        'before_widget' => '<div id="%1$s" class="home-widget main-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title-home-sidebar">',
        'after_title'   => '</h3>',
    ));
    register_sidebar( array(
        'name' => __( 'Archive Small Sidebar', 'devvn' ),
        'id' => 'archive-small-sidebar',
        'before_widget' => '<div id="%1$s" class="archive-small-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title-archive-small-sidebar">',
        'after_title'   => '</h3>',
    ));
    register_sidebar( array(
        'name' => __( 'Footer Top Right Sidebar', 'devvn' ),
        'id' => 'footer-top-right-sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title-sidebar">',
		'after_title'   => '</h3>',
    ));
    register_sidebar( array(
        'name' => __( 'Single video Sidebar', 'devvn' ),
        'id' => 'single-video-sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title-sidebar">',
		'after_title'   => '</h3>',
    ));
    register_sidebar( array(
        'name' => __( 'Single video Sidebar 2', 'devvn' ),
        'id' => 'single-video-sidebar-2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title-sidebar">',
		'after_title'   => '</h3>',
    ));
}
//Title
function svl_wp_title( $title, $sep ) {
	global $paged, $page;
	if ( is_feed() )
		return $title;
	$title .= get_bloginfo( 'name', 'display' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		$title = "$title $sep " . sprintf( __( 'Trang %s', 'devvn' ), max( $paged, $page ) );
	return $title;
}
add_filter( 'wp_title', 'svl_wp_title', 10, 2 );

// Add specific CSS class by filter
add_filter( 'body_class', 'devvn_mobile_class' );
function devvn_mobile_class( $classes ) {
	if(wp_is_mobile()){
		$classes[] = 'devvn_mobile';
	}else{
		$classes[] ="devvn_desktop";
	}
	return $classes;
}
/* ACF 4
//Theme Options
function my_acf_options_page_settings( $settings )
{
	$settings['title'] = 'Theme Options';
	$settings['pages'] = array('General');

	return $settings;
}

add_filter('acf/options_page/settings', 'my_acf_options_page_settings');
*/
//Theme Options
if( function_exists('acf_add_options_page') ) {
 
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title' 	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
	/*acf_add_options_sub_page(array(
		'page_title' 	=> 'Shop Page Setting',
		'menu_title' 	=> 'Social',
		'parent_slug' 	=> $parent['menu_slug'],
	));*/
 
}

//Code phan trang
function wp_corenavi_table($main_query = null) {
		global $wp_query;
		if(!$main_query) $main_query = $wp_query;
		$big = 999999999; 
		$total = $main_query->max_num_pages;
		if($total > 1) echo '<div class="paginate_links">';
		echo paginate_links( array(
			'base' 		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' 	=> '?paged=%#%',
			'current' 	=> max( 1, get_query_var('paged') ),
			'total' 	=> $total,
			'mid_size'	=> '10',
			'prev_text'    => __('Trang trước','devvn'),
			'next_text'    => __('Trang tiếp','devvn'),
		) );
		if($total > 1) echo '</div>';
}
function div_wrapper($content) {
    $pattern = '~<iframe.*src=".*(youtube.com|youtu.be).*</iframe>|<embed.*</embed>~'; //only iframe youtube
    preg_match_all($pattern, $content, $matches);
    foreach ($matches[0] as $match) { 
        $wrappedframe = '<div class="videoWrapper">' . $match . '</div>';
        $content = str_replace($match, $wrappedframe, $content);
    }
    return $content;    
}
add_filter('the_content', 'div_wrapper');

function get_thumbnail($img_size = 'thumbnail', $w = '', $h = ''){
	global $post;
	$url_thumb_full = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  		
  	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size );
	$url_thumb = $thumb['0'];
	$w_thumb = $thumb['1'];
	$h_thumb = $thumb['2'];
	if(($w_thumb != $w || $h_thumb != $h) && $url_thumb_full && $w != "" && $h != "") $url_thumb = aq_resize($url_thumb_full,$w,$h,true,true,true);
	if(!$url_thumb) $url_thumb = TEMP_URL.'/images/no-image-featured-image.png';
	return $url_thumb;
}

function get_excerpt($limit = 130){
	$excerpt = get_the_excerpt();
	if(!$excerpt) $excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	if($excerpt){
		$permalink = get_the_permalink();
		$excerpt = $excerpt.'... <a href="'.$permalink.'" title="" rel="nofollow">'.__('Xem thêm','devvn').'</a>';
	}
	return $excerpt;
}

function devvn_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'devvn_excerpt_more' );

function devvn_custom_excerpt_length( $length ) {
    return 34;
}
add_filter( 'excerpt_length', 'devvn_custom_excerpt_length', 999 );

if ( ! function_exists( 'devvn_ilc_mce_buttons' ) ) {
    function devvn_ilc_mce_buttons($buttons){
        array_push($buttons,
            "alignjustify",
            "subscript",
            "superscript"
        );
        return $buttons;
    }
    add_filter("mce_buttons", "devvn_ilc_mce_buttons");
}
if ( ! function_exists( 'devvn_ilc_mce_buttons_2' ) ) {
    function devvn_ilc_mce_buttons_2($buttons){
        array_push($buttons,
            "backcolor",
            "anchor",
            "fontselect",
            "fontsizeselect",
            "cleanup"
        );
        return $buttons;
    }
    add_filter("mce_buttons_2", "devvn_ilc_mce_buttons_2");
}
// Customize mce editor font sizes
if ( ! function_exists( 'devvn_mce_text_sizes' ) ) {
    function devvn_mce_text_sizes( $initArray ){
        $initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 17px 18px 19px 20px 21px 24px 28px 32px 36px";
        return $initArray;
    }
    add_filter( 'tiny_mce_before_init', 'devvn_mce_text_sizes' );
}

add_action( 'tgmpa_register', 'devvn_register_required_plugins' );
function devvn_register_required_plugins() {
	$plugins = array(
		array(
			'name'               => 'Advanced Custom Fields PRO',
			'slug'               => 'advanced-custom-fields-pro',
			'source'             => get_template_directory() . '/inc/plugins/advanced-custom-fields-pro.zip',
			'required'           => true,
			'force_activation'   => true,
			'force_deactivation' => true
		)
	);
	$config = array(
		'id'           => 'devvn',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);
	tgmpa( $plugins, $config );
}
//Get time
function get_current_day(){
    $weekday = strtolower(date('l'));
    switch($weekday) {
        case 'monday':
            $weekday = 'Thứ hai';
            break;
        case 'tuesday':
            $weekday = 'Thứ ba';
            break;
        case 'wednesday':
            $weekday = 'Thứ tư';
            break;
        case 'thursday':
            $weekday = 'Thứ năm';
            break;
        case 'friday':
            $weekday = 'Thứ sáu';
            break;
        case 'saturday':
            $weekday = 'Thứ bảy';
            break;
        default:
            $weekday = 'Chủ nhật';
            break;
    }
    echo $weekday.', '.date('d/m/Y');
}

add_action('header_top_right', 'social_list_func');
function social_list_func($exclude = ''){
    ob_start();
    $facebook_social = get_field('facebook_social','option');
    $twitter_social = get_field('twitter_social','option');
    $youtube_social = get_field('youtube_social','option');
    $google_plus_social = get_field('google_plus_social','option');
    $email_social = get_field('email_social','option');
    if($facebook_social || $youtube_social || $google_plus_social || $email_social):
    ?>
    <ul class="social_list">
        <?php if($facebook_social):?><li class="facebook_social"><a href="<?php echo esc_url($facebook_social);?>" title="" target="_blank"><i class="fa fa-facebook"></i></a></li><?php endif;?>
        <?php if($twitter_social):?><li class="twitter_social"><a href="<?php echo esc_url($twitter_social);?>" title="" target="_blank"><i class="fa fa-twitter"></i></a></li><?php endif;?>
        <?php if($youtube_social):?><li class="youtube_social"><a href="<?php echo esc_url($youtube_social);?>" title="" target="_blank"><i class="fa fa-youtube-play"></i></a></li><?php endif;?>
        <?php if($google_plus_social):?><li class="google_plus_social"><a href="<?php echo esc_url($google_plus_social);?>" title="" target="_blank"><i class="fa fa-google-plus"></i></a></li><?php endif;?>
        <?php if($email_social && $exclude != "mail"):?><li class="email_social"><a href="mailto:<?php echo sanitize_email($email_social);?>" title=""><i class="fa fa-envelope"></i></a></li><?php endif;?>
    </ul>
    <?php
    endif;
    echo ob_get_clean();
}

add_shortcode('social_list', 'social_list_shortcode_func');
function social_list_shortcode_func($atts){
    $atts = shortcode_atts( array(
        'exclude' => '',
    ), $atts, 'social_list' );
    ob_start();
    social_list_func($atts['exclude']);
    return ob_get_clean();
}

add_action('left_header_bottom','title_left_header_bottom');
function title_left_header_bottom(){
    $page_for_posts = get_option( 'page_for_posts' );
    if(is_category() || is_archive() || is_tax()){
        echo '<h2 class="title_archive">'.get_the_archive_title().'</h2>';
    }elseif (is_single()){
        //the_category(', ');
    }elseif (is_page() && !is_front_page()){
        echo '<h1 class="title_archive">'.get_the_title().'</h1>';
    }elseif (is_home()){
        echo '<h1 class="title_archive">'.get_the_title($page_for_posts).'</h1>';
    }
}

add_filter('get_the_archive_title', 'devvn_get_the_archive_title', 99);
function devvn_get_the_archive_title($title) {
    return preg_replace('/.+: /', '', $title);
}

add_action('after_post_excerpt', 'devvn_after_post_excerpt');
function devvn_after_post_excerpt(){
    ob_start();
    $tags = wp_get_post_tags(get_the_ID());
    if ($tags){
        $tag_ids = array();
        foreach($tags as $individual_tag){
            $tag_ids[] = $individual_tag->term_id;
        }
        $args = array(
            'tag__in' 			=>	$tag_ids,
            'post__not_in' 		=>	array(get_the_ID()),
            'posts_per_page'    =>	5,
            'orderby'			=>	'rand'
        );
        $my_query = new wp_query($args);
        if( $my_query->have_posts() ):
            echo '<div class="bvlienquan"><ul>';
            while ($my_query->have_posts()):$my_query->the_post();
                ?>
                <li><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
                <?php
            endwhile;
            echo '</ul></div>';
        endif; wp_reset_query();
    }
    echo ob_get_clean();
}

function devvn_comment($comment, $args, $depth)    {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class();?> id="li-comment-<?=get_comment_ID();?>">
        <div id="comment-<?=get_comment_ID();?>" class="clearfix">
            <div class="comment-author vcard">
                <?php echo get_avatar($comment, $size='70', ''); ?>
            </div><!-- end comment autho vcard-->

            <div class="commentBody">
                <div class="comment-meta commentmetadata">
                    <?php printf(__('<p class="fn">%s</p>'), get_comment_author_link()); ?>
                </div><!--end .comment-meta-->
                <?php if($comment->comment_approved == '0') : ?>
                    <em><?php _e('Bình luận của bạn đang chờ xét duyệt.','devvn');?></em>
                <?php endif; ?>
                <div class="noidungcomment">
                    <?php comment_text(); ?>
                </div>
                <div class="tools_comment">
                    <?php comment_reply_link(array_merge($args,array('respond_id' => 'formcmmaxweb','depth' => $depth, 'max_depth'=> $args['max_depth'])));?>
                    <?php edit_comment_link(__('Sửa'),' ',''); ?>
                    <?php printf(__('<a href="#comment-%d" class="ngaythang">%s</a>'),get_comment_ID(),get_comment_date('d/m/Y'));?>
                </div>

            </div><!--end #commentBody-->
        </div><!--end #comment-author-vcard-->
    </li>
<?php }
//validate comments
function comment_validation_init() {
    if(is_singular() && comments_open() ) { ?>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('#commentform').validate({
                    onfocusout: function(element) {
                        this.element(element);
                    },
                    rules: {
                        author: {
                            required: true,
                            minlength: 2
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        comment: {
                            required: true,
                        }
                    },
                    messages: {
                        author: "Họ và tên là bắt buộc.",
                        email: "Email là bắt buộc.",
                        comment: "Hãy viết bình luận."
                    },
                    errorElement: "div",
                    errorPlacement: function(error, element) {
                        element.after(error);
                    }
                });
            });
        </script>
        <?php
    }
}
add_action('wp_footer', 'comment_validation_init');

function devvn_single_video_temp($single_template) {
    global $post;

    if (get_post_format($post->ID) == 'video') {
        $single_template = dirname( __FILE__ ) . '/single-video.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'devvn_single_video_temp' );

function devvn_getYouTubeVideoId($url)
{
    $video_id = false;
    $url = parse_url($url);
    if (strcasecmp($url['host'], 'youtu.be') === 0)
    {
        #### (dontcare)://youtu.be/<video id>
        $video_id = substr($url['path'], 1);
    }
    elseif (strcasecmp($url['host'], 'www.youtube.com') === 0)
    {
        if (isset($url['query']))
        {
            parse_str($url['query'], $url['query']);
            if (isset($url['query']['v']))
            {
                #### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
                $video_id = $url['query']['v'];
            }
        }
        if ($video_id == false)
        {
            $url['path'] = explode('/', substr($url['path'], 1));
            if (in_array($url['path'][0], array('e', 'embed', 'v')))
            {
                #### (dontcare)://www.youtube.com/(whitelist)/<video id>
                $video_id = $url['path'][1];
            }
        }
    }else{
        return false;
    }
    return $video_id;
}

add_action('devvn_single_video_after_ads','devvn_comment_single');
add_action('devvn_after_single_main_content','devvn_comment_single');
function devvn_comment_single(){
    $active_facebook_comment = get_field('active_facebook_comment','option');
    $active_cmt = get_field('active_cmt','option');
    $width_facecmt = get_field('width_facecmt','option');
    $color = get_field('color','option');
    $num_posts = get_field('num_posts','option');
    if($active_facebook_comment):
    ?>
    <div class="single_face_cmt">
        <div class="fb-comments" data-href="<?php echo get_the_permalink();?>" data-colorscheme="<?php echo $color;?>" data-width="<?php echo $width_facecmt;?>" data-numposts="<?php echo $num_posts;?>"></div>
    </div>
    <?php elseif ( (comments_open() || get_comments_number()) && $active_cmt) :?>
        <div class="single_comment">
            <?php comments_template();?>
        </div>
    <?php endif;?>
    <?php
}


add_action('wp_head','devvn_code_html_head_func',99);
function devvn_code_html_head_func(){
    if(is_admin()) return;
    $code_html_head = get_field('code_html_head','option');
    if($code_html_head) echo $code_html_head;
}
add_action('wp_footer','devvn_code_html_footer_func',99);
function devvn_code_html_footer_func(){
    if(is_admin()) return;
    $code_html_footer = get_field('code_html_footer','option');
    if($code_html_footer) echo $code_html_footer;
}
add_action('devvn_after_body','devvn_code_html_body_func',99);
function devvn_code_html_body_func(){
    if(is_admin()) return;
    $code_html_body = get_field('code_html_body','option');
    if($code_html_body) echo $code_html_body;
}




////Insert ads after second paragraph of single post content 1.
add_filter( 'the_content', 'prefix_insert_post_ads' );
function prefix_insert_post_ads( $content ) {
$ad_code = '<div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>';
if ( is_single() && ! is_admin() ) {
return prefix_insert_after_paragraph( $ad_code, 2, $content );
}
return $content;
}
// Parent Function that makes the magic happen
function prefix_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
$closing_p = '</p>';
$paragraphs = explode( $closing_p, $content );
foreach ($paragraphs as $index => $paragraph) {
if ( trim( $paragraph ) ) {
$paragraphs[$index] .= $closing_p;
}
if ( $paragraph_id == $index + 1 ) {
$paragraphs[$index] .= $insertion;
}
}
return implode( '', $paragraphs );
}



////Insert ads after second paragraph of single post content 2.
add_filter( 'the_content', 'qt_insert_post_ads' );
function qt_insert_post_ads( $content ) {
$ad_code = '<div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh+Tex 2 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="3881780434"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>';
if ( is_single() && ! is_admin() ) {
return qt_insert_after_paragraph( $ad_code, 6, $content );
}
return $content;
} 
function qt_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
$closing_p = '</p>'; 
$paragraphs = explode( $closing_p, $content );
foreach ($paragraphs as $index => $paragraph) {
if ( trim( $paragraph ) ) {
$paragraphs[$index] .= $closing_p;
}
 
if ( $paragraph_id == $index + 1 ) {
$paragraphs[$index] .= $insertion;
}
}
return implode( '', $paragraphs );
}

////Insert ads after second paragraph of single post content 3.
add_filter( 'the_content', 'qt2_insert_post_ads' );
function qt2_insert_post_ads( $content ) {
$ad_code = '<div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh 3 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="2652173838"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>';
if ( is_single() && ! is_admin() ) {
return qt_insert_after_paragraph( $ad_code, 10, $content );
}
return $content;
} 
function qt2_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
$closing_p = '</p>'; 
$paragraphs = explode( $closing_p, $content );
foreach ($paragraphs as $index => $paragraph) {
if ( trim( $paragraph ) ) {
$paragraphs[$index] .= $closing_p;
}
 
if ( $paragraph_id == $index + 1 ) {
$paragraphs[$index] .= $insertion;
}
}
return implode( '', $paragraphs );
}




/// Pham Cong Son Edit
/// Pham Cong Son Edit
add_filter( 'the_content', 'prefix_insert_post_ads14' );
function prefix_insert_post_ads14( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh+Tex 2 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="3881780434"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 14, $content );
	}
	return $content;
}
add_filter( 'the_content', 'prefix_insert_post_ads18' );
function prefix_insert_post_ads18( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh 3 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="2652173838"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 18, $content );
	}
	return $content;
}
add_filter( 'the_content', 'prefix_insert_post_ads22' );
function prefix_insert_post_ads22( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh 4 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="4727958242"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 22, $content );
	}
	return $content;
}


add_filter( 'the_content', 'prefix_insert_post_ads26' );
function prefix_insert_post_ads26( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh 4 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="4727958242"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 26, $content );
	}
	return $content;
}

add_filter( 'the_content', 'prefix_insert_post_ads30' );
function prefix_insert_post_ads30( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 30, $content );
	}
	return $content;
}
add_filter( 'the_content', 'prefix_insert_post_ads34' );
function prefix_insert_post_ads34( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 34, $content );
	}
	return $content;
}
add_filter( 'the_content', 'prefix_insert_post_ads38' );
function prefix_insert_post_ads38( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 38, $content );
	}
	return $content;
}

add_filter( 'the_content', 'prefix_insert_post_ads42' );
function prefix_insert_post_ads42( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 42, $content );
	}
	return $content;
}
add_filter( 'the_content', 'prefix_insert_post_ads46' );
function prefix_insert_post_ads46( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 46, $content );
	}
	return $content;
}




add_filter( 'the_content', 'prefix_insert_post_ads50' );
function prefix_insert_post_ads50( $content ) {
	$ad_code = '<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- QC Đáp Ứng BV Ảnh -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2209582575280167"
     data-ad-slot="7420380820"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>';
	if ( is_single() && ! is_admin() ) {
		return prefix_insert_after_paragraph( $ad_code, 50, $content );
	}
	return $content;
}







	




@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '300' );

