<?php get_header();?>
<?php if(have_rows('noi_dung_frontpage')):?>
    <?php while(have_rows('noi_dung_frontpage')):the_row();?>
        <?php if( get_row_layout() == 'tin_moi_nhat' ):?>
            <?php include get_template_directory().'/templates/blocks/tin_moi_nhat.php';?>
        <?php elseif( get_row_layout() == 'hinh_anh' ):?>
            <?php include get_template_directory().'/templates/blocks/image.php';?>
        <?php elseif( get_row_layout() == 'videos' ):?>
            <?php include get_template_directory().'/templates/blocks/videos.php';?>
        <?php elseif( get_row_layout() == 'news_theo_category_2col' ):?>
            <?php include get_template_directory().'/templates/blocks/news_theo_category_2col.php';?>
        <?php elseif( get_row_layout() == 'news_theo_category_3col' ):?>
            <?php include get_template_directory().'/templates/blocks/news_theo_category_3col.php';?>
        <?php elseif( get_row_layout() == 'multi_image' ):?>
            <?php include get_template_directory().'/templates/blocks/multi_image.php';?>
        <?php elseif( get_row_layout() == 'news_slider' ):?>
            <?php include get_template_directory().'/templates/blocks/news_slider.php';?>
        <?php endif;?>
    <?php endwhile;?>
<?php endif;?>
<?php get_footer();?>