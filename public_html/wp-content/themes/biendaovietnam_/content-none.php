<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

    <p><?php printf( __( 'Hãy viết bài đầu tiên <a href="%1$s">tại đây</a>.', 'devvn' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

<?php elseif ( is_search() ) : ?>

    <p><?php _e( 'Xin lỗi, không tìm thấy kết quả nào phù hợp. Hãy thử lại với từ khóa khác.', 'devvn' ); ?></p>
    <?php get_search_form(); ?>

<?php else : ?>

    <p><?php _e( 'Có vẻ như chúng tôi không thể tìm thấy những gì bạn đang tìm kiếm. Hãy thử tìm kiếm gì đó.', 'devvn' ); ?></p>
    <?php get_search_form(); ?>

<?php endif; ?>