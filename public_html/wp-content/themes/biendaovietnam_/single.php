<?php get_header();?>
<?php while(have_posts()):the_post();
$post_view_type = get_field('post_view_type');
?>
<div class="main_container">
    <div class="container">
        <div class="row">
            <?php if($post_view_type == 'fullwidth'):?>
            <div class="main_container_full">
            <?php else:?>
            <div class="main_container_sidebar">
            <?php endif;?>
                <div class="single_main_container">
                    <h1 class="single_title"><?php the_title();?></h1>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- QC ÐC baoanninhvn.net -->
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="ca-pub-2209582575280167"
						 data-ad-slot="5100612892"
						 data-ad-format="auto"
						 data-full-width-responsive="true"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
                    <div class="single_toolbar">
                        <div class="news_box_date"><?php echo get_the_date('d/m/Y h:i');?></div>
                        <div class="news_box_views"><?php echo do_shortcode('[post_view]')?></div>
                    </div>
                    <?php if(has_excerpt()):?>
                    <h2 class="excerpt_post"><?php the_excerpt();?></h2>
                    <?php endif;?>
                    <?php do_action('after_post_excerpt');?>
                    <div class="tinymce">
                        <?php the_content();?>
                    </div>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- QC ÐC baoanninhvn.net -->
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="ca-pub-2209582575280167"
						 data-ad-slot="5100612892"
						 data-ad-format="auto"
						 data-full-width-responsive="true"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
                    <?php the_tags('<div class="single_tags">', ' ', '</div>');?>
                    <div class="like_share">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>&amp;display=popup&amp;ref=plugin" class="share_fb">Chia sẻ facebook</a>
                        <a target="_blank" href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>" class="share_gg">Chia sẻ google</a>
                        <div class="xs-clear"></div>
                        <a href="javascript:void(0);" onclick="window.print();" target="_blank" class="print"></a>
                        <a href="mailto://?Subject=<?php echo get_the_title();?>&amp;Body=<?php echo get_the_permalink();?>" class="send_email"></a>
                    </div>
                    <?php do_action('devvn_after_single_main_content');?>
                </div>
            </div>
            <?php if($post_view_type != 'fullwidth'):?>
            <div class="sidebar_container">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('main-sidebar')) : ?><?php endif; ?>
            </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php
$categories = get_the_category(get_the_ID());
if ($categories){
    echo '<div class="relatedcat"><div class="container">';
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args = array(
        'category__in' => $category_ids,
        'post__not_in' => array(get_the_ID()),
        'posts_per_page' => 9
    );
    $my_query = new wp_query($args);
    if( $my_query->have_posts() ):
        echo '<h2 class="title_box_news"><span>'.__('Đọc thêm','devvn').'</span></h2><div class="row">';
        while ($my_query->have_posts()):$my_query->the_post();
            ?>
            <div class="col-sm-4 col-xs-6">
                <div class="news_box">
                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a></div><?php endif;?>
                    <div class="news_title_desc">
                        <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                    </div>
                </div>
            </div>
            <?php
        endwhile;
        echo '</div>';
    endif; wp_reset_query();
    echo '</div></div>';
}
?>
<?php endwhile;?>
<?php get_footer();?>