<?php
$select_category_col1 = get_sub_field('select_category_col1');
$select_category_col2 = get_sub_field('select_category_col2');
$select_category_col3 = get_sub_field('select_category_col3');
if($select_category_col1 || $select_category_col2 || $select_category_col3):
$cat_3col = array($select_category_col1, $select_category_col2, $select_category_col3);
?>
<div class="news_home_3col">
    <div class="container">
        <div class="row">
            <?php foreach($cat_3col as $cat_ID):?>
                <?php if($cat_ID):?>
                    <div class="col-sm-4 col-xs-12">
                        <?php
                        if($cat_ID):
                            $news_small = new WP_Query(array(
                                'post_type' =>  'post',
                                'posts_per_page' =>  3,
                                'cat'   =>  $cat_ID
                            ));
                            ?>
                            <div class="box_news_2col box_news_2col_small">
                                <h2 class="title_box_news">
                                    <a href="<?php echo get_category_link($cat_ID);?>" title="<?php echo get_cat_name($cat_ID);?>">
                                        <span><?php echo get_cat_name($cat_ID);?></span>
                                    </a>
                                </h2>
                                <div class="box_news_2col_list">
                                    <?php
                                    if($news_small->have_posts()):
                                        while($news_small->have_posts()):$news_small->the_post();?>
                                            <div class="news_box news_img_title_desc">
                                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                                    <div class="news_box_title"><?php the_title();?></div>
                                                </a>
                                                <div class="news_box_desc"><?php the_excerpt();?></div>
                                            </div>
                                        <?php endwhile;?>
                                    <?php else:?>
                                        <p><?php _e('Đang cập nhật...','devvn');?></p>
                                    <?php endif; wp_reset_query();?>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="border_bottom"></div>
    </div>
</div>
<?php endif;?>
