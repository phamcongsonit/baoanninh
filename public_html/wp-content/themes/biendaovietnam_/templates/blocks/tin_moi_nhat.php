<?php
$active_sidebar = get_sub_field('active_sidebar');
?>
<div class="home_news_sidebar">
    <div class="container">
        <div class="row">
            <?php if($active_sidebar):?>
            <div class="main_container_sidebar">
            <?php else:?>
            <div class="main_container_full">
            <?php endif;?>
                <?php
                $posts_per_page = 12;
                $posts_per_page_row2 = 4;
                $post_row2 = ($posts_per_page <= $posts_per_page_row2) ? 0 : $posts_per_page - $posts_per_page_row2;
                $start_row2 = $post_row2 + 1;
                $latest_news = new WP_Query(array(
                    'post_type' =>  'post',
                    'posts_per_page' => $posts_per_page,
                ));
                $post_count = $latest_news->post_count;
                if($latest_news->have_posts()):
                ?>
                <div class="news_latest">
                    <?php $stt = 1; while($latest_news->have_posts()):$latest_news->the_post();?>
                    <?php if($stt <= $post_row2 || $post_count <= $post_row2 || $post_row2 == 0):?>
                    <?php if($stt == 1):?><div class="news_latest_top"><?php endif;?>
                        <?php if($stt == 1):?>
                        <div class="nlt_left">
                            <div class="news_box news_img_title_desc">
                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                    <div class="news_box_title"><?php the_title();?></div>
                                </a>
                                <div class="news_box_desc"><?php the_excerpt();?></div>
                            </div>
                        </div>
                        <?php else:?>
                            <?php if($stt == 2):?><div class="nlt_right scrollbar-inner"><?php endif;?>
                                <div class="news_box news_img_title">
                                    <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                        <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                        <div class="news_box_title"><?php the_title();?></div>
                                    </a>
                                </div>
                            <?php if($stt == $post_row2 || $stt == $post_count):?></div><?php endif;?>
                        <?php endif;?>
                    <?php if($stt == $post_row2 || $stt == $post_count):?></div><?php endif;?>
                    <?php else:?>
                    <?php if($stt == $start_row2):?><div class="news_latest_bottom"><div class="row"><?php endif;?>
                        <div class="col-sm-3 col-xs-6">
                            <div class="news_box news_img_title">
                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                    <div class="news_box_title"><?php the_title();?></div>
                                </a>
                            </div>
                        </div>
                    <?php if($stt == $post_count):?></div></div><?php endif;?>
                    <?php endif;?>
                    <?php $stt++; endwhile;?>
                </div>
                <?php endif; wp_reset_query();?>
            </div>
            <?php if($active_sidebar):?>
            <div class="sidebar_container">
                <div class="sidebar">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('home-sidebar')) : ?><?php endif; ?>
                </div>
            </div>
            <?php endif;?>
        </div>
    </div>
</div>
