<?php
$image = get_sub_field('image_url');
$link_to = get_sub_field('image_link_href');
$link_target = get_sub_field('open_new_tab');
if($image):
?>
<div class="section_oneimage">
    <div class="container">
        <?php if($link_to):?><a href="<?php echo esc_url($link_to);?>" title="" target="_<?php echo ($link_target)?'blank':'self'?>"><?php endif;?>
            <?php echo wp_get_attachment_image($image,'full');?>
        <?php if($link_to):?></a><?php endif;?>
    </div>
</div>
<?php
endif;