<?php
/*
 * Template Name: Full width
*/
get_header();?>
<?php while(have_posts()):the_post();?>
    <div class="main_container">
        <div class="container">
            <div class="row">
                <div class="main_container_full">
                    <div class="single_main_container">
                        <div class="tinymce">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile;?>
<?php get_footer();?>