��    /      �  C                   (     0  	   <  	   F  �   P  �        �     �  
   �     �     �     �               !  
   0  
   ;  i   F     �  	   �  
   �     �     �     �  
   �     �  
                  2     C  
   O     Z  	   f     p     ~  	   �  9   �  %   �       9   $  e   ^     �  q   �  >   M	  P  �	     �  	   �     �            �   *  �        �     �  
                  .     G     U     h     |     �  u   �           /     =     P     V  
   b     m     �     �     �     �     �     �     �     �       
     !     	   A  P   K  9   �  &   �  m   �  �   k  %     �   5  M   �            .   #   )      (               	                          *   ,      "                                   &             -   
                                      %                                    !   '   +   /   $              404 Error Page Add URL Add new URL All Pages All Posts An error occurred while connecting to the license server. Error: %s. Please login to thrivethemes.com, report this error message on the forums and we'll get this sorted for you An error occurred while receiving the license status. The response was: %s. Please login to thrivethemes.com, report this error message on the forums and we'll get this sorted for you. Archive Pages Basic Settings Blog Index Cancel Categories etc. Current selection Default Display Logic Email Address: Exclusions Front Page License activation error - please contact support copying this message and we'll get this sorted for you. Load Saved Options Logged in Logged out Other Page Templates Pages Post Types Posts Remove URL Save & Close Save Display Template Save as Template Search page Select All Select None Taxonomy: Template name Template saved with success ! Thank you Thank you - You have successfully validated your license! Thrive Clever Widgets Display Options Thrive Widget Display Options Use this form to set when your widgets will be displayed. Use this form to set when your widgets will not be displayed. This overrides the display logic above. Validate your License: You can save the display configuration that you've created if you'd like a template to re-use with other widgets. Your license for the Thrive Clever Widgets Plugin is activated Project-Id-Version: Thrive Clever Widgets
POT-Creation-Date: 2015-08-13 15:50+0300
PO-Revision-Date: 2017-06-02 14:08+0700
Last-Translator: 
Language-Team: 
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
X-Poedit-SearchPathExcluded-1: .sass-cache
X-Poedit-SearchPathExcluded-2: .idea
X-Poedit-SearchPathExcluded-3: .git
 404 trang lỗi Thêm URL Thêm mới URL Mọi trang Tất cả bài viết Lỗi xuất hiện khi kết nối với máy chủ đã được cấp phép. Lỗi: %s. Vui lòng đăng nhập thrivethemes.com, báo cáo thông báo lỗi này trên các diễn đàn và chúng tôi sẽ hỗ trợ bạn Lỗi xảy ra khi nhận trạng thái  giấy phép. Biểu hiện là: %s. Vui lòng đăng nhập  thrivethemes.com, báo cáo thông báo lỗi này trên các diễn đàn và chúng tôi sẽ hỗ trợ bạn. Trang lưu trữ Cài đặt cơ bản Blog Index Hủy Danh mục vv. Vùng chọn hiện tái Mặc định Hiển thị Logic Địa chỉ email: Ngoại lệ Trang trước Giấy phép lỗi - vui lòng liên hệ hỗ trợ, hay sao chép tin nhắn này và chúng tôi sẽ giúp bạn. Tải các tùy chọn đã lưu Đăng nhập Đã đăng xuất Khác Mẫu Trang Các trang Loại bài viết Bài viết Xóa bỏ URL Lưu và đóng Lưu mẫu hiển thị Lưu thành mẫu Trang tìm kiếm Chọn tất cả Không chọn Phân loại: Tên mẫu Mẫu được lưu thành công! Cảm ơn Cảm ơn bạn - Bạn đã xác nhận thành công  giấy phép của bạn! Tùy chọn hiển thị bài viết Movan Clever Widgets Tùy chọn hiển thị Thrive Widget Sử dụng mẫu này để thiết lập khi nào các biểu mẫu của bạn sẽ được hiển thị. Sử dụng biểu mẫu này để đặt khi các biểu mẫu của bạn sẽ không được hiển thị. Điều này ghi đè logic hiển thị ở trên. Xác thực giấy phép của bạn: Bạn có thể lưu cấu hình hiển thị mà bạn đã tạo nếu bạn muốn mẫu sử dụng lại với các biểu mẫu khác. Giấy phép của bạn về Movan Clever Widgets đã được kích hoạt 