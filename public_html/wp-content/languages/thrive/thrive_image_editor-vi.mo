��    M      �  g   �      �     �     �     �     �  5   �               (     G     S     g     �     �     �     �     �     �     �     �     �  G        W     o     u     {     �     �  Y   �     �     �     	  	   	  	   (	     2	     I	     Z	     f	     m	     r	     �	  
   �	     �	  6   �	     �	     
  5   !
     W
     c
  5   }
     �
     �
     �
     �
     �
            (        A     F     R  	   o     y     �     �     �  1   �  /        1  
   9  
   D     O  
   _     j     {     �     �  9  �     �  $   �  6        P  ;   ]     �  
   �     �     �     �     �          0     >     D     Y     o     �     �     �  \   �     4     G  
   N     Y     j     s  �   x            +   *     V     b     ~     �     �  	   �  
   �  )   �  )        .     C  Y   R     �     �  \   �     C  -   U  \   �  
   �     �  #         $     A     N     `  B   {     �     �     �     �        "     '   6     ^  B   y  S   �               5     K     e  (   w     �  =   �     �                1   *   !   H          8   B   '      .       7      =   L       "       /                          K   @   J   M              C   3   $   %   <   	   (      &                 ,   -   F                5   +   A      I             ;   2   
              6            0           G         E          ?                 :   D       )   4      #       9                >    - Select Font - Align Image to Canvas All changes saved! Architectural Are you sure you want to delete this background image Background Image Background color Background image crop position Bad request Badge content empty Badge could not be saved Badge saved Blank Template Cancel Canvas Properties Canvas Size Canvas size: %s X %s Changes could not be saved! Changes saved Choose Template Choose a Social Share Badge template or start creating one from scratch Choose background image Close Color Color Overlay Copy Delete Drag the image to place it on your canvas. Use the corners handlers to resize your image. Dynamic Dynamic result Enter your text here... Font face Font size Generate Random Result Got it, let's go Green Style Height Help Image Post could not be deleted Image Post could not be saved Image Size Influential Invalid height, accepted value is between 100 and 1200 Invalid parameter Invalid request. Invalid width, accepted value is between 100 and 1200 Line height Method %s not implemented No image to remove, you have to to choose one before. Opacity Paragraph/Text Please choose an image. Please select a template Preview Replace Image Resize Resize the image by dragging the corners Save Save & Exit Save and generate the image. Saving... Select Background Image Set Custom Dimensions Simple Content Elements Size & Position Social Share Badge Template you would like to use Start Dragging to move your image on the canvas Stylish Text align Text color Text formatting Text style Use default size Width Your changes are auto-saved none Project-Id-Version: thrive-image-editor 1.0.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-05-10 09:13+0000
PO-Revision-Date: 2017-06-02 12:08+0700
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.0.3
 - Chọn Phông chữ - Căn chỉnh hình ảnh vào Canvas Tất cả thay đổi của bạn đã được lưu! Kiến trúc Bạn có chắc chắn muốn xóa hình nền này không Ảnh nền Màu nền Vị trí ảnh nền Yêu cầu xấu Nội dung huy hiệu trống Không thể lưu huy hiệu Huy hiệu đã được lưu Mẫu trống Hủy Thuộc tính Canvas Kích thước Canvas Kích thước Canvas:%s X%s Không thể lưu thay đổi! Thay đổi đã được lưu Chọn Mẫu Chọn mẫu Huy hiệu Cộng đồng Xã hội hoặc bắt đầu tạo mẫu từ đầu Chọn ảnh nền Đóng Màu sắc Lớp phủ màu Sao lưu Xóa Kéo hình để đặt nó lên canvas của bạn. Sử dụng các góc xử lý để thay đổi kích thước hình ảnh của bạn. Dynamic/Động Kết quả động Nhập văn bản của bạn ở đây ... Mặt chữ Kích thước phông chữ Tạo kết quả ngẫu nhiên Got it, Hãy đi nào Phong cách Xanh Độ Cao Hỗ trợ Không thể xóa Bài đăng hình ảnh Không thể lưu Hình ảnh bài đăng Kích thước ảnh Ảnh hưởng Chiều cao không hợp lệ, giá trị được chấp nhận là từ 100 đến 1200 Tham số không hợp lệ Yêu cầu không hợp lệ. Chiều rộng không hợp lệ, giá trị được chấp nhận là từ 100 đến 1200 Chiều cao dòng Phương pháp%s không được triển khai Không có hình ảnh nào cần xoá, bạn phải chọn một hình ảnh trước đó. Độ mờ Đoạn / Văn bản Vui lòng chọn một hình ảnh. Vui lòng chọn một mẫu Xem trước Thay đổi ảnh Thay đổi kích thước Thay đổi kích thước hình ảnh bằng cách kéo các góc Lưu Lưu Và Thoát Lưu và tạo ra hình ảnh. Đang lưu... Chọn hình nền Đặt kích thước tùy chỉnh Các yếu tố nội dung đơn giản Kích thước & Vị trí Mẫu Huy hiệu Cộng đồng Xã hội bạn muốn sử dụng Bắt đầu Kéo để di chuyển hình ảnh của bạn trên khung làm việc Thời trang Căn chỉnh văn bản Màu chữ văn bản Định dạng văn bản Kiểu văn bản Sử dụng Định dạng mặc định Bề rộng Tất cả thay đổi của bạn được lưu tự động trống 