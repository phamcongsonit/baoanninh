<?php
include('geoip.class_helper.php');
$ip = '';
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

$data = GeoIP::LookupIP($ip);
if(isset($data['cc'])){
	echo $data['cc'];
} else {
	echo "NULL";
}
