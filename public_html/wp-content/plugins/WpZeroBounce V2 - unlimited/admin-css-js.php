<?php
/* Load CSS and JS files for admin */



//CSS for admin - settings page
function zerobounce_admin_css() {
  //wp_register_script('zerobounce-admin-css', plugin_dir_url( __FILE__ )."admin.css");
  wp_enqueue_style( 'zerobounce-admin-css', plugin_dir_url( __FILE__ )."admin.css");
  wp_enqueue_style( 'zerobounce-jgrowl-css', plugin_dir_url( __FILE__ )."/res/css/jquery.jgrowl.min.css");

}

add_action('admin_enqueue_scripts', 'zerobounce_admin_css');

//JS for admin
function zerobounce_load_admin_js() {
    wp_register_script('zerobounce-url-validation', plugin_dir_url( __FILE__ )."zerobounce.js", array('jquery'),'', true);
    wp_enqueue_script( 'jquery-ui-autocomplete' );
    wp_enqueue_script( 'zerobounce-url-validation');

    wp_register_script('zerobounce-jgrowl', plugin_dir_url( __FILE__ )."/res/js/jquery.jgrowl.min.js", array('jquery'),'', true);
    wp_enqueue_script( 'zerobounce-jgrowl');

    wp_register_script('zerobounce-md5', plugin_dir_url( __FILE__ )."/res/js/md5.js", array('jquery'),'', true);
    wp_enqueue_script( 'zerobounce-md5');

   // wp_register_script('zerobounce-popupoverlay', plugin_dir_url( __FILE__ )."/res/js/jquery.popupoverlay.js", array('jquery'),'', true);
   //wp_enqueue_script( 'zerobounce-popupoverlay');
    

    $availableTags = array();
    $tags = get_tags( array() );
		foreach ($tags as $tag) {
			$availableTags[] = $tag->name;
		}

	$availableCategories = array();
	$args = array('orderby' => 'id',
        'hide_empty'=> 0,

		);
	   $cats = get_categories( $args );

		foreach ($cats as $cat) {
			$availableCategories[] = $cat->name;
		}


    $ajax_url = admin_url( 'admin-ajax.php' );
    if(zerobounce_string_endsWith($ajax_url, '/')){
      $ajax_url = substr($ajax_url , 0, -1);
    }

    $isSimpleLicense = '0';
    if(ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
      $isSimpleLicense = '1';
    }


    $data = array('availableTags' => $availableTags,
			'availableCategories' => $availableCategories,
			'ajaxurl' => $ajax_url,
      'isSimpleLicense' => $isSimpleLicense );

    wp_localize_script( 'zerobounce-url-validation', 'zb_js', $data );
}
add_action( 'admin_enqueue_scripts', 'zerobounce_load_admin_js' );

function zerobounce_load_visitor__js(){
  
}