<?php
/* Adds a metabox in posts and pages. Contains validation funcitons 

Removes all exisiting .js file by settings update
*/



/* Adds a box for zerobounce on post/ page edit site */
function zerobounce_add_meta_box() {
    // All public post types
    $post_types = array_merge(
        array('page' => 'page', 'post' => 'post'),
        get_post_types(array('_builtin' => FALSE))
    );

    // Add meta box for each post type
    foreach ($post_types as $post_type)
    {
         add_meta_box(
            'zerobounce_sectionid',
            'Zero Bounce',
            'zerobounce_meta_box_callback',
            $post_type,
            'advanced',
            'high'
        );
    } 
}
add_action( 'add_meta_boxes', 'zerobounce_add_meta_box' );

/* Prints the box content. */
function zerobounce_meta_box_callback( $post ) {
    wp_nonce_field( 'zerobounce_meta_box', 'zerobounce_meta_box_nonce' );

    $zerobounce_individual_disable = get_post_meta( $post->ID, '_zerobounce_individual_disable', true );
    $zerobounce_specific_url = get_post_meta( $post->ID, '_zerobounce_specific_url', true );
    $zerobounce_force_enable = get_post_meta( $post->ID, '_zerobounce_force_enable', true );
    $zerobounce_disable = get_post_meta( $post->ID, '_zerobounce_disable', true );
    $ext_site_url = menu_page_url( 'zerobounce-external-sites', false);
    $post_id = get_the_ID();
    

    if(ZeroBounceDB_ExtSites::urlExist(strval($post_id))){

        $id = ZeroBounceDB_ExtSites::getId($post_id);
        $ext_site_url .= "&zb_extsite_change=$id";
    } else {
        $ext_site_url .= "&zb_extsite_add_url=$post_id";
    }

    echo "<table id='zerobounce_metabox'><tbody>
            <tr>
                <td><label for='zerobounce_individual_disable'><b>Disable:</b></td>
                <td><input id='zerobounce_individual_disable' type='checkbox' name='zerobounce_individual_disable' value='1' ".checked( $zerobounce_individual_disable, 1, false )."/></td>
                <td>If activated, Zero Bounce will be disabled for this post/page.</td>
            </tr>
            <tr>
                <td><label for='zerobounce_force_enable'><b>Force enable:</b></td>
                <td><input id='zerobounce_force_enable' type='checkbox' name='zerobounce_force_enable' value='1' ".checked( $zerobounce_force_enable, 1, false )."/></td>
                <td>If activated, the visitor will get redirected even if Zero Bounce itself is deactivated inside Zero Bounce settings.</td>
            </tr>
            <tr>
                <td><label for='zerobounce_specific_url'><b>Individual url:</b></td>
                <td><input type='text' id='zerobounce_specific_url' name='zerobounce_specific_url' value='" . esc_attr( $zerobounce_specific_url ) . "' /></td>
                <td>You can set a custom redirect url for this post/page.</td>
            </tr>";
    if(ZEROBOUNCE_ITEM_NAME != ZEROBOUNCE_SIMPLE_LICENSE){
        echo "<tr>
                <td><b>Advanced Settings:</b></td>
                <td><a href='$ext_site_url&zb_extSite_is_post=1' target='_blank'>Show Advanced Settings</a></td>
                <td>You can set custom settings for this post/page.</td>
            </tr>";
    }

             
    echo "</tbody></table>";

}

/* Save meta box data if post/page is saved */
function zerobounce_save_meta_box_data( $post_id ) {
    //check nounce field
    if ( ! isset( $_POST['zerobounce_meta_box_nonce'] ) ) {
        return;
    }
    //verify nounce
    if ( ! wp_verify_nonce( $_POST['zerobounce_meta_box_nonce'], 'zerobounce_meta_box' ) ) {
        return;
    }
    //is not autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    //check user permission
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    if(isset( $_POST['zerobounce_force_enable']) && strlen($_POST['zerobounce_specific_url']) > 0){
        update_post_meta( $post_id, '_zerobounce_force_enable', '1' );
        update_post_meta( $post_id, '_zerobounce_individual_disable', '0' );
    } else {
        update_post_meta( $post_id, '_zerobounce_force_enable', '0' );
    }


    if(isset( $_POST['zerobounce_individual_disable']) && get_post_meta( $post_id, '_zerobounce_force_enable', true ) == '0'){
        update_post_meta( $post_id, '_zerobounce_individual_disable', '1' );
        update_post_meta( $post_id, '_zerobounce_specific_url', '' );
    } else {
        update_post_meta( $post_id, '_zerobounce_individual_disable', '0' );
        $zerobounce_specific_url = sanitize_text_field( $_POST['zerobounce_specific_url'] );

        if(zerobounce_is_url($zerobounce_specific_url) == false){
            update_post_meta( $post_id, '_zerobounce_specific_url', '' );
        } else {
            update_post_meta( $post_id, '_zerobounce_specific_url', $zerobounce_specific_url );
        }
    }
  
    zerobounce_delete_js_files(); 
}
add_action( 'save_post', 'zerobounce_save_meta_box_data' );