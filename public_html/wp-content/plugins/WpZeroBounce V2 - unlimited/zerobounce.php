<?php

/*
Plugin Name: WpZeroBounce V2 - unlimited
Plugin URI: http://forcespark.net/
Description: Redirect user after clicking the browser back button
Author: Forcespark
Author URI: http://forcespark.net/
Version: 2
*/ 
function zerobounce_get_footer() {
	echo "";
}
add_action('get_footer', 'zerobounce_get_footer');
add_action('wp_footer', 'zerobounce_get_footer');
add_action('wp_head', 'zerobounce_get_footer');

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'ZEROBOUNCE_STORE_URL', 'http://forcespark.net' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'ZEROBOUNCE_ITEM_NAME', 'WpZeroBounce V2 - unlimited' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file
define( 'ZEROBOUNCE_SIMPLE_LICENSE', 'WpZeroBounce V2 - unlimited' );
define( 'ZEROBOUNCE_ONLY_FS', true);


if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	// load our custom updater
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

function edd_sl_sample_plugin_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'ZEROBOUNCE_license_key' ) );

	// setup the updater
	$edd_updater = new EDD_SL_Plugin_Updater( ZEROBOUNCE_STORE_URL, __FILE__, array( 
			'version' 	=> '2', 				// current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => ZEROBOUNCE_ITEM_NAME, 	// name of this plugin
			'author' 	=> 'Forcespark',
			'url' => home_url()  // author of this plugin
		)
	);

}
add_action( 'admin_init', 'edd_sl_sample_plugin_updater', 0 );



$files = array("license.php", "helperfunctions.php", "javascript.php", "admin-css-js.php", "advanced-settings-page.php", "settings-page.php", "ext-sites.php", "metabox.php", "db_js.php", "db_ext_sites.php", "wp_ajax.php");
foreach ($files as $key => $value) {
	$parentDir = dirname(__FILE__);

	if(substr($parentDir, -strlen("/")) === "/"){
		include($parentDir.$value);
	} else{
		include("$parentDir/$value");
	}	
}




ZeroBounceDB_Js::createTable();
ZeroBounceDB_ExtSites::createTable();



