<?php

if(!class_exists('ZeroBounceDB_ExtSites')){
	class ZeroBounceDB_ExtSites{
		public static function getTableName(){
			global $wpdb;
			 
			return $wpdb->prefix . "zerobounce_ext_sites";
		}


		public static function createTable(){
			global $wpdb;
			$table_name = self::getTableName();
			if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
				 $sql = "CREATE TABLE IF NOT EXISTS `".self::getTableName()."` (
						`id` INT NOT NULL AUTO_INCREMENT,
					  `url` VARCHAR(2000) NOT NULL,
					  `settings` LONGTEXT NOT NULL,
					  `md5` VARCHAR(32) NOT NULL,
					  `is_active` TINYINT(1) NULL DEFAULT 1,
					  PRIMARY KEY (`id`))
					ENGINE = InnoDB";

				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}
		}

		public static function isActive($id){
			global $wpdb;

			$query = "SELECT is_active FROM ".self::getTableName()." WHERE id = %d";
			$sql = $wpdb->prepare($query, $id);
			$result = $wpdb->get_var($sql);

			return $result;
		}
		

		public static function addEmpty($url){
			global $wpdb;
			$url = esc_sql($url);
			$settings = serialize(array());
			$md5 = md5($url);

			$result = $wpdb->insert(self::getTableName(),
				array('url' => $url,
					'settings' => $settings,
					'md5' => $md5
					));

			if($result != false){
				return $wpdb->insert_id;
			} 

			return false;			
		}

		public static function remove($id){
			global $wpdb;
			$id = intval($id);
			$wpdb->delete( self::getTableName(), array( 'id' => $id ) );
		}

		public static function getAll(){
			global $wpdb;
			$sql = "SELECT * FROM ".self::getTableName();
			return $wpdb->get_results($sql);
		}


		public static function getUrl($id){
			global $wpdb;
			$url = $wpdb->get_var( "SELECT url FROM ".self::getTableName()." WHERE id = '".esc_sql($id)."';" );
			
			return $url;
		}

		public static function getId($url){
			global $wpdb;
			$id = $wpdb->get_var( "SELECT id FROM ".self::getTableName()." WHERE url = '".esc_sql($url)."';" );
			if($id != null){
				return intval($id);
			}

			return $id;
		}

		public static function getSettings($id){
			global $wpdb;
			$settings = $wpdb->get_var( "SELECT settings FROM ".self::getTableName()." WHERE id = '".esc_sql($id)."';" );
			if($settings == null){
				return false;
			}

			if(strlen($settings) == 0){
				return $settings;
			}
			$settings = unserialize($settings);

			return $settings;
		}


		public static function getMD5($id){
			global $wpdb;
			$md5 = $wpdb->get_var( "SELECT md5 FROM ".self::getTableName()." WHERE id = '".esc_sql($id)."';" );
			if($md5 == null){
				return false;
			}

			return $md5;
		}

		

		public static function setSettings($id, $settings){
			global $wpdb;
			$settings = serialize($settings);

			$wpdb->query(
				"UPDATE ".self::getTableName()." SET settings = '".esc_sql($settings)."' WHERE id = ".esc_sql($id)."	");
		}

		public static function urlExist($url){
			global $wpdb;

			$result = $wpdb->get_var("SELECT id FROM ".self::getTableName()." WHERE url='".esc_sql($url)."'");
			if($result == null){
				return false;
			}

			return true;

		}

		public static function toggleStatus($id){
			global $wpdb;

			if(!is_int($id)){
				$id = intval($id);
			}

			$wpdb->query("UPDATE ".self::getTableName()." SET is_active = !is_active WHERE id = $id");
		}

		
	}
}


