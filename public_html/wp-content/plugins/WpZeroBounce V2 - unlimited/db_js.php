<?php

class ZeroBounceDB_Js{
	public static function getTableName(){
		global $wpdb;
		 
		return $wpdb->prefix . "zerobounce_js";
	}

	public static function createTable(){
		global $wpdb;
		$table_name = self::getTableName();
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		    $sql = "CREATE TABLE IF NOT EXISTS `".self::getTableName()."` (
				  `id` INT NOT NULL AUTO_INCREMENT,
				  `md5` VARCHAR(32) NOT NULL,
				  `js` LONGTEXT NOT NULL,
				  PRIMARY KEY (`id`),
				  UNIQUE INDEX `md5_UNIQUE` (`md5` ASC))
				ENGINE = InnoDB";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
	}

	public static function add($md5, $js){
		global $wpdb;
		 $wpdb->show_errors();
		$sql = "INSERT IGNORE INTO ".self::getTableName()." (md5,js) VALUES ('".esc_sql($md5)."', '".base64_encode($js)."');";
		$wpdb->query($sql);
	}

	public static function get($md5){
		global $wpdb;
		$js = $wpdb->get_var( "SELECT js FROM ".self::getTableName()." WHERE md5 = '".esc_sql($md5)."';" );
		if($js == null){
			return false;
		}

		return base64_decode($js);
	}

	public static function deleteMd5($md5){
		global $wpdb;
		$wpdb->delete( self::getTableName(), array( 'md5' => $md5 ) );
	}


	public static function deleteAll(){
		 global $wpdb;
    	$wpdb->query('DELETE FROM '.self::getTableName());
	}

	public static function md5Exist($md5){
		global $wpdb;

		$result= $wpdb->get_row( "SELECT id FROM FROM ".self::getTableName()." WHERE md5 = '".esc_sql($md5)."' LIMIT 1;"  );

		return ($result == null) ? false : true;
	}



}