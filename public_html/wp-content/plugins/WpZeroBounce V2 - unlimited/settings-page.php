<?php

/* 
    Settings Page of the plugin

    Removes all exisiting .js file by settings update
*/


/*  
    Init settings page
*/


//add option page
add_action( 'admin_menu', 'zerobounce_admin_menu' );
function zerobounce_admin_menu() {
    if(ZEROBOUNCE_ONLY_FS){
        add_menu_page("Zero Bounce", "Zero Bounce", 'manage_options' ,'zerobounce-plugin', 'zerobounce_options_page', plugin_dir_url( __FILE__ ).'images/icon.jpg' );
    } else if(ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
        add_menu_page("Zero Bounce", "Zero Bounce", 'manage_options' ,'zerobounce-plugin', 'zerobounce_options_page', plugin_dir_url( __FILE__ ).'images/icon.jpg' );
    } else {
        add_menu_page("Zero Bounce Pro", "Zero Bounce Pro", 'manage_options' ,'zerobounce-plugin', 'zerobounce_options_page', plugin_dir_url( __FILE__ ).'images/icon.jpg' );
    }

    if('valid' == get_option( 'ZEROBOUNCE_license_status' )){
        add_submenu_page( 'zerobounce-plugin', 'Ext Sites', 'External sites', 'manage_options', 'zerobounce-external-sites', array('ZeroBounceExtSites', "echo_site") );
    }
    
    
}


//html for options page
function zerobounce_options_page() {
    $license    = get_option( 'ZEROBOUNCE_license_key' );
    $status     = get_option( 'ZEROBOUNCE_license_status' );
    $active_tab = 'plugin_options';

    if($status != 'valid'){
        $active_tab = 'license';        
    } else if(isset( $_GET[ 'tab' ] )) {
        $active_tab = $_GET[ 'tab' ];
    
    }  ?>
    <div class="wrap">
        <h2><?php 
    if(ZEROBOUNCE_ONLY_FS){
        echo "Zero Bounce";
    } else if (ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
        echo "Zero Bounce";
    } else {
        echo "Zero Bounce Pro";
    }


        ?></h2>
    
        <p><a href="http://forcespark.net/faq/" title="ONLINE FAQ" target="_blank">Online FAQ</a> <?php if(ZEROBOUNCE_ONLY_FS) {?>| <a href="http://forcespark.net/affiliate-program/" title="Affiliate" target="_blank">Affiliate</a> <?php } ?>| <a href="http://forcespark.net/support/index.php?a=add" title="Contact" target="_blank">Contact</a>  | <?php if(ZEROBOUNCE_ITEM_NAME != ZEROBOUNCE_SIMPLE_LICENSE) { ?><a href="https://forcespark.uservoice.com/forums/271234-new-feature-suggestions" title="Suggest New Feature" target="_blank">Suggest New Feature</a> | <?php } ?><a href="?page=zerobounce-plugin&tab=debug" title="Debug Information">Debug</a> </p>
        <h2 class="nav-tab-wrapper">
            <?php 
                if($status == 'valid') {?>
                    <a href="?page=zerobounce-plugin&tab=plugin_options" class="nav-tab <?php echo $active_tab == 'plugin_options' ? 'nav-tab-active' : ''; ?>">Plugin Options</a>
                   <?php if(ZEROBOUNCE_ITEM_NAME != ZEROBOUNCE_SIMPLE_LICENSE) {?>
                   <a href="?page=zerobounce-plugin&tab=advanced_options" class="nav-tab <?php echo $active_tab == 'advanced_options' ? 'nav-tab-active' : ''; ?>">Advanced Options</a>
                   <?php }                ?>
                 
    
                <?php } ?>
            
            <a href="?page=zerobounce-plugin&tab=license" class="nav-tab <?php echo $active_tab == 'license' ? 'nav-tab-active' : ''; ?>">License</a>
        </h2>

       

        <form action="options.php" method="POST">
            <?php 
             if( $active_tab == 'plugin_options' ) {
                settings_fields( 'zerobounce-settings-group' ); 
                do_settings_sections( 'zerobounce-plugin' );
                submit_button('Save', 'primary', 'SavePluginOptions');
            } else if($active_tab == 'license'){
                ZEROBOUNCE_license_page();
            } else if($active_tab == 'debug'){
                zerobounce_debug_callback();
            } else if($active_tab == 'advanced_options' && ZEROBOUNCE_ITEM_NAME != ZEROBOUNCE_SIMPLE_LICENSE){
                settings_fields( 'zerobounce-settings-group' ); 
                do_settings_sections( 'zerobounce-plugin-advanced' );
            } 

            ?>
        </form>
        <?php 
            if(ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
                echo '<iframe src="http://wpzerobounce.com/pluginiframe.php" style="width:500px; height:500px"></iframe>';
            }
        ?>
        
    </div>
    <?php
}


/*
    Init setting fields for settings page -- tab "Main"
*/
add_action( 'admin_init', 'zerobounce_settings_page_main_init' );
function zerobounce_settings_page_main_init() {
    /* register settings */
    register_setting( 'zerobounce-settings-group', 'zerobounce-main-settings-group', 'zerobounce_main_field_validation');
    register_setting( 'zerobounce-settings-group', 'zerobounce-condition-settings-group', 'zerobounce_condition_field_validation');

    /* section "activation" */
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $url = ZeroBounceDB_ExtSites::getUrl($id);
        add_settings_section( 'section-main', 'Change settings for: '.$url, array('ZeroBounceExtSites', 'section_globalUrl_callback'), 'zerobounce-plugin' );
    } else {
        add_settings_section( 'section-main', 'Explanation', 'zerobounce_section_explanation_callback', 'zerobounce-plugin' );
    }
    
   
     if(!ZeroBounceExtSites::isChange()){
         add_settings_field( 'zerobounce-IsActivated', 'Activate: ', 'reboounce_field_IsActivated_callback', 'zerobounce-plugin', 'section-main' );
        }

    if(ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
        add_settings_field( 'zerobounce-global-url', 'Global Redirect Url: ', 'zerobounce_field_global_url_callback', 'zerobounce-plugin', 'section-main' );
    } else {
        add_settings_field( 'zerobounce-global-url', 'Global Url/s: ', 'zerobounce_field_global_url_callback', 'zerobounce-plugin', 'section-main' );
    }
    
    
if(ZEROBOUNCE_ITEM_NAME != ZEROBOUNCE_SIMPLE_LICENSE){
    if(!ZeroBounceExtSites::isChange()){
        add_settings_field( 'zerobounce-DisableFor', 'Disable for: ', 'zerobounce_field_DisableFor_url_callback', 'zerobounce-plugin', 'section-main' );
    }
}
    if(!ZeroBounceExtSites::isChange()){
        add_settings_field( 'zerobounce-PutJSInsideHtml', 'Load Javascript inside Html: ', 'zerobounce_field_PutJSInsideHtml_callback', 'zerobounce-plugin', 'section-main' );
    }
    

   
    /*section "zerobounce only if" */
    if(ZEROBOUNCE_ITEM_NAME != ZEROBOUNCE_SIMPLE_LICENSE){
        add_settings_section( 'section-conditions', 'Conditions', 'zerobounce_section_zerobounceOnlyIf_callback', 'zerobounce-plugin' );
        add_settings_field( 'zerobounce-conditionReferrerContains', 'Activate if referer contains: ', 'zerobounce_field_conditionReferrerContains_callback', 'zerobounce-plugin', 'section-conditions' );
        add_settings_field( 'zerobounce-conditionDisableIfRefererContains', 'Disable if referer contains: ', 'zerobounce_conditionDisableIfRefererContains_callback', 'zerobounce-plugin', 'section-conditions' );

        add_settings_field( 'zerobounce-conditionTimeout', 'Deactivate after x seconds: ', 'zerobounce_field_conditionTimeout_callback', 'zerobounce-plugin', 'section-conditions' );
        add_settings_field( 'zerobounce-conditionEnableOnlyIfUrlContains', 'Enable only if url contains: ', 'zerobounce_conditionEnableOnlyIfUrlContains_callback', 'zerobounce-plugin', 'section-conditions' );
        add_settings_field( 'zerobounce-conditionDisableIfUrlContains', 'Disable if url contains: ', 'zerobounce_conditionDisableIfUrlContains_callback', 'zerobounce-plugin', 'section-conditions' );
       
        
        add_settings_field( 'zerobounce-conditionByDevice', 'Redirect by device: ', 'zerobounce_field_conditionByDevice_callback', 'zerobounce-plugin', 'section-conditions' );
        add_settings_field( 'zerobounce-conditionRedirectMobileUrls', 'Redirect mobile devices to: ', 'zerobounce_field_conditionRedirectMobileUrls', 'zerobounce-plugin', 'section-conditions' );

        add_settings_field( 'zerobounce-conditionByPercentOfVisitors', 'Redirect only x percent of visitors: ', 'zerobounce_field_condititionByPercentOfVisitors_callback', 'zerobounce-plugin', 'section-conditions' );
        add_settings_field( 'zerobounce-conditionByVisitorCloseIntention', "Redirect if visitor want's to close the website/browser. ", 'zerobounce_field_condititionByVisitorCloseIntention_callback', 'zerobounce-plugin', 'section-conditions' );
        add_settings_field( 'zerobounce-conditionRedirectVisitorOnlyOnce', 'Redirect visitor only once in x days', 'zerobounce_field_conditionRedirectVisitorOnlyOnce_callback', 'zerobounce-plugin', 'section-conditions' );
    }
    



}


/* 
    Callbacks for settings page "Main" -- section "Explanation" 
*/
function zerobounce_section_explanation_callback() {
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $url = ZeroBounceDB_ExtSites::getUrl($id);
     
        if(!zerobounce_string_startsWith($url, 'http')){ //change settings für blog post
            $title = get_the_title( intval($url) );

            echo "<h4>Change for Post ($id): $title</h4>";
        } else {
             echo "<h4>Change for: ".ZeroBounceDB_ExtSites::getUrl(ZeroBounceExtSites::getChangeNumber())."</h4>";
        }
       
    }


    echo "<div style='float:left;width:50%'>The global url is used to redirect the visitor when the browser back button is clicked. If in a post or page an other url is set, this will be used instead. If none global and individual url is set, Zero Bounce is disabled.</div>";
    if(ZEROBOUNCE_ONLY_FS){
        echo "<div style='float:right'><a class='zerobounce_documentation_button' href='http://forcespark.net/documentation/' target='_blank'>Documentation</a></div>";
    } else if (ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
        echo "<div style='float:right'><a class='zerobounce_documentation_button' href='http://wpzerobounce.com/documentation-simple.html' target='_blank'>Documentation</a></div>";
    } else {
        echo "<div style='float:right'><a class='zerobounce_documentation_button' href='http://wpzerobounce.com/documentation-pro.html' target='_blank'>Documentation</a></div>";
    }

    
}

function reboounce_field_IsActivated_callback(){
    $options = get_option( 'zerobounce-main-settings-group' );
    if(!isset($options['IsActivated'])){
        $options['IsActivated'] = 0;
    }

    echo "<table class='zerobounce_table'><tbody>
        <tr id='zerobounce_tr_zerobounce_IsActivated'>
            <td><input type='checkbox' id='zerobounce_IsActivated' name='zerobounce-main-settings-group[IsActivated]' value='1'  ".checked( $options['IsActivated'], 1, false )."/></td>
            <td></td>
            <td>If disabled, zerobounce will not redirect the visitor.</td>
        </tr>
        </tbody></table>";
}
function zerobounce_field_global_url_callback() {
    $options = null;
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-main-settings-group' );
    }
    
    
    $array = array('globalUrl', 'globalUrls', 'globalUrlIsActivated', 'globalUrlsIsActivated');
    foreach($array as $entry){
        if(!isset($options[$entry])){
            $options[$entry] = '';
        }
    }

    $textarea = str_replace('<br/>', "\n", $options['globalUrls']);




    if(ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
        echo "<table class='zerobounce_table'><tbody>
        <tr id='zerobounce_tr_globalUrl'>
            <td><input type='text' id='zerobounce_globalUrl' name='zerobounce-main-settings-group[globalUrl]' value='".$options['globalUrl']."' /></td>
            <td></td>
            <td>Set your redirect url here.</td>
        </tr></tbody></table>
        <input type='hidden' id='globalUrlIsActivated' name='zerobounce-main-settings-group[globalUrlIsActivated]' value='1'/>
        <input type='hidden' id='globalUrlsIsActivated' name='zerobounce-main-settings-group[globalUrlsIsActivated]' value='0'/>";

    } else {
        echo "<table class='zerobounce_table'><tbody>
        <tr id='zerobounce_tr_globalUrl'>
            <td><input type='text' id='zerobounce_globalUrl' name='zerobounce-main-settings-group[globalUrl]' value='".$options['globalUrl']."' /></td>
            <td><a href='#' id='zerobounce_addMoreUrls'>Add more Urls</a></td>
            <td>If you add more than one url, Zero Bounce will rotate them for each visitor. Every url in a new line.</td>
        </tr>
        <tr id='zerobounce_tr_globalUrls'>
            <td><textarea type='text' id='zerobounce_globalUrls' name='zerobounce-main-settings-group[globalUrls]' >".$textarea."</textarea></td>
            <td><a href='#' id='zerobounce_useOneUrl'>Use one url</a></td>
            <td>If you use one url, Zero Bounce will use this to redirect the visitor.<br> By adding an '\\' at the end of an url followed by a number, you can set how many percentage the url should be used for redirect.<br> Example: http://www.amazon.de\\50</td>
        </tr>
        </tbody></table>
        <input type='hidden' id='globalUrlIsActivated' name='zerobounce-main-settings-group[globalUrlIsActivated]' value='".$options['globalUrlIsActivated']."' ".checked( $options['globalUrlIsActivated'], 1, false )."/>
        <input type='hidden' id='globalUrlsIsActivated' name='zerobounce-main-settings-group[globalUrlsIsActivated]' value='".$options['globalUrlsIsActivated']."' ".checked( $options['globalUrlsIsActivated'], 1, false )."/>";

    }
    
}

function zerobounce_field_DisableFor_url_callback(){
    $options = null;
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
       $options = get_option( 'zerobounce-main-settings-group' );
    }
    
    $fields = array('DeactivateForHomepage', 'DeactivateForPosts', 'DeactivateForPages');
    foreach($fields as $field){
        if(!isset($options[$field])){
            $options[$field] = 0;
        }
    }

    echo "<table class='zerobounce_table'><tbody>
        <tr>
            <td>
                <input type='checkbox' name='zerobounce-main-settings-group[DeactivateForHomepage]' value='1' ".checked( $options['DeactivateForHomepage'], 1, false )."/> Homepage
            </td>
            <td>
                <input type='checkbox' name='zerobounce-main-settings-group[DeactivateForPosts]' value='1' ".checked( $options['DeactivateForPosts'], 1, false )."/> Posts
            </td>
            <td>
                <input type='checkbox' name='zerobounce-main-settings-group[DeactivateForPages]' value='1' ".checked( $options['DeactivateForPages'], 1, false )."/> Pages
            </td>
        </tr>
        </tbody></table>";
}

function zerobounce_field_PutJSInsideHtml_callback(){
    $options = get_option( 'zerobounce-main-settings-group' );
    $array = array('PutJSInsideHtml', 'PutJSInsideHtmlOption');
    foreach($array as $entry){
        if(!isset($options[$entry])){
            $options[$entry] = '';
        }
    }

    
    if(!isset($options['PutJSInsideHtml'])) {
        $options['PutJSInsideHtml'] = 0;
    }
    $getFooter = ($options['PutJSInsideHtmlOption'] == 'get_footer') ? 'selected=\'selected\'' : '';
    $wpFooter = ($options['PutJSInsideHtmlOption'] == 'wp_footer') ? 'selected=\'selected\'' : '';
    $wpHead = ($options['PutJSInsideHtmlOption'] == 'wp_head') ? 'selected=\'selected\'' : '';
    
    echo "<table class='zerobounce_table'><tbody>
        <tr id='zerobounce_tr_zerobounce_IsActivated'>
            <td><input type='checkbox' id='zerobounce_InsideHtml' name='zerobounce-main-settings-group[PutJSInsideHtml]' value='1'  ".checked( $options['PutJSInsideHtml'], 1, false )."/></td>
            <td></td>
            <td><select id='zerobounce_InsideHtmlOption' name='zerobounce-main-settings-group[PutJSInsideHtmlOption]' >
      <option value='get_footer' $getFooter>get_footer</option>
      <option value='wp_footer' $wpFooter>wp_footer</option>
      <option value='wp_head' $wpHead>wp_head</option>    
    </select></td>
    <td>
        Don't change it! Only if Zero Bounce seems not to work or the <br>support mention it, activate it and try all three options.
    </td>
        </tr>
        </tbody></table>";
}





/* 
    Callbacks for settings page "Main" -- section "Conditions"

*/
function zerobounce_section_zerobounceOnlyIf_callback() {
    echo 'In default Zero Bounce is always activated. As soon you activate one of the conditions the default settings will get ignored.';
}

function zerobounce_field_conditionReferrerContains_callback(){
    $options = null;
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-condition-settings-group' );
    }

    $fields = array("conditionReferrerContainsGoogle", "conditionReferrerContainsFacebook", "conditionReferrerContainsBing");
    foreach ($fields as $field) {
        if(!isset($options[$field])){
            $options[$field] = 0;
        }
    }
    

   
    $settingStr = '';
    $explanationStr = "Check if the visitor's referrer contains this text. If yes Zero Bounce will get activated. Every referer in a new line.";
    $text = (isset($options['conditionReferrerContains'])) ? str_replace('<br/>', "\n", $options['conditionReferrerContains']) : '';

    $settingStr .= '<input type="checkbox" id="zerobounce_conditionReferrerContainsGoogle" name="zerobounce-condition-settings-group[conditionReferrerContainsGoogle]" value="1" '.checked( $options['conditionReferrerContainsGoogle'], 1, false ).' /><span name="conditionReferrer">Google</span>
                    <input type="checkbox" id="zerobounce_conditionReferrerContainsFacebook" name="zerobounce-condition-settings-group[conditionReferrerContainsFacebook]" value="1" '.checked( $options['conditionReferrerContainsFacebook'], 1, false ).' /><span name="conditionReferrer">Facebook</span>
                    <input type="checkbox" id="zerobounce_conditionReferrerContainsBing" name="zerobounce-condition-settings-group[conditionReferrerContainsBing]" value="1" '.checked( $options['conditionReferrerContainsBing'], 1, false ).' /><span name="conditionReferrer">Bing</span><br><br>
                    <textarea id="zerobounce_conditionReferrerContains" type="text" name="zerobounce-condition-settings-group[conditionReferrerContains]" placeholder="Additional Referer, one per line" />'.$text.'</textarea>';
    zerobounce_create_settings_table('conditionReferrerContains', $settingStr, $explanationStr);
    //zerobounce_create_conditionField('conditionReferrerContains', true, 'Check if the visitor\'s referrer contains this text. If yes zerobounce will get activated. Every referer in a new line.', true);

}

function zerobounce_field_conditionTimeout_callback(){
    zerobounce_create_conditionField('conditionTimeout', true, 'For the first x seconds Zero Bounce is activated. After this time zerobounce will be deactivated and not redirect the user.');
}

function zerobounce_conditionEnableOnlyIfUrlContains_callback(){
     zerobounce_create_conditionField('conditionEnableOnlyIfUrlContains', true, 'Enable Zero Bounce only if url contains', true);
}

function zerobounce_conditionDisableIfUrlContains_callback(){
     zerobounce_create_conditionField('conditionDisableIfUrlContains', true, 'Disable Zero Bounce if url contains', true);
}



function zerobounce_conditionDisableIfRefererContains_callback(){
    zerobounce_create_conditionField('conditionDisableIfRefererContains', true, 'Disable Zero Bounce if referer contains', true);
}

function zerobounce_field_conditionVisitedSeveralPages_callback(){
    zerobounce_create_conditionField('conditionVisitedSeveralPages', false,  'If it is enabled plugin will work for users who surf inside site for more then one page. If it is disabled plugin will not work for users who visit more then one page');
}

function zerobounce_field_conditionByDevice_callback(){
    $options = null;
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-condition-settings-group' );
    }
    $redirectFor = null;
    if(!isset($options['conditionByDevice'])){
        $redirectFor = "";
    } else {
        $redirectFor = $options['conditionByDevice'];
    }
   
   

    $settingStr = "<input type='radio' id='zerobounce_conditionByDeviceMobile' name='zerobounce-condition-settings-group[conditionByDevice]' value='Mobil' ".checked( $redirectFor, 'Mobil', false )."/>
                   <span name='conditionByDevice'>Mobile</span>
                   <input type='radio' id='zerobounce_conditionByDeviceDesktop' name='zerobounce-condition-settings-group[conditionByDevice]' value='Desktop' ".checked( $redirectFor, 'Desktop', false )."/>
                   <span name='conditionByDesktop'>Desktop</span>";

    $explanationStr ="If activated, the user of the specific device will get redirected.";

    zerobounce_create_settings_table('conditionByDevice', $settingStr, $explanationStr);
}

function zerobounce_field_conditionRedirectMobileUrls(){
    zerobounce_create_conditionField('conditionRedirectMobileUrls', true, 'Redirect mobile devices to one of the urls', true);
}

function zerobounce_field_condititionByPercentOfVisitors_callback(){
    zerobounce_create_conditionField('conditionPercentOfVisitors', true, 'By default Zero Bounce redirects 100% of your visitors. With this settings you can set a custom percentage.');
}

function zerobounce_field_condititionByVisitorCloseIntention_callback(){
    zerobounce_create_settings2_table('conditionVisitorCloseIntention', "As soon the mouse leave the browser window the visitor get's redirected.");
}

function zerobounce_field_conditionRedirectVisitorOnlyOnce_callback(){
    zerobounce_create_conditionField('conditionRedirectVisitorOnlyOnce', true, 'If activated and the visitor was already redirected on time in the past, then Zero Bounce not redirect again.');
}

/* 
    Callbacks for page debug
*/

function zerobounce_debug_callback(){
    if ( ! function_exists( 'get_plugins' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    $main_options = get_option( 'zerobounce-main-settings-group' );
    $condition_options = get_option( 'zerobounce-condition-settings-group' );
    $redByReferer_options = get_option( 'zerobounce-redirectByReferer-group' );
    $redByCountry_options = get_option( 'zerobounce-redirectByCountry-group' );
    $plugins = get_plugins();
    $license = get_option( 'ZEROBOUNCE_license_key' );
    $license_status = get_option( 'ZEROBOUNCE_license_status' );
    $license = array('LicenseKey' => $license, 'license_status' => $license_status);
    $siteurl = get_site_url();
    $db_entries = array("ZEROBOUNCE_randomStr", "ZEROBOUNCE_JS_CachePath", "ZEROBOUNCE_JS_CacheUrl");
    $db_array = array();
    foreach($db_entries as $entry){
        $db_array[$entry] = get_option($entry);
    }


    $array = array('MainSettings' => $main_options, 'ConditionSettings' => $condition_options, 'plugins' => $plugins, 'license' => $license,
        'AdvancedOptions' => array(
            'Redirect by Rererer' => $redByReferer_options,
            'Redirect by Country' => $redByReferer_options),
        'DB Entries' => $db_array,
        'Site url' => $siteurl

        );
    $html = htmlspecialchars(json_encode($array));

    echo "<p><textarea id='zerobounce_textarea_debug' readonly='readonly'>".$html."</textarea></p>
        <p>
            <b>instructions:</b>
            <ol>
                <li>Select the hole text inside the textbox.</li>
                <li>Copy it inside your support request</li>
                <li>We will check it and send you a response asap.</li>
            </ol>
        </p>
    ";
}



// settings page -- section "Conditions": helper function to create condition fields
function zerobounce_create_conditionField($name,$enableInput, $explanation, $useTextarea = false){
    $options = null;
    
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-condition-settings-group' );
    }

    $settingStr = "";
    $explanationStr = "";
    $options[$name] = (isset($options[$name])) ? $options[$name] : '';
    if($enableInput) {
        if($useTextarea){
            $text = str_replace('<br/>', "\n", $options[$name]);
            $settingStr = "<textarea id='zerobounce_$name' type='text' name='zerobounce-condition-settings-group[".$name."]' />".$text."</textarea>";
        } else {
            $settingStr = "<input id='zerobounce_$name' type='text' name='zerobounce-condition-settings-group[".$name."]' value='".$options[$name]."' />";
        }
    } 

    
    if($explanation != null) {
        $explanationStr = $explanation;
    }

    zerobounce_create_settings_table($name, $settingStr, $explanationStr);
    
}

// settings page -- section "Conditions": create table, which contains the condition fields
function zerobounce_create_settings_table($name, $settingStr, $explanationStr){
    $options = null;
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-condition-settings-group' );
    }

    
    if(!isset($options[$name.'IsActivated'])){
        $options[$name.'IsActivated'] = 0;
    }

    echo "<table class='zerobounce_settings_table'>
            <tbody>
                <tr>
                    <td name='zerobounce_td_settings'>$settingStr</td>
                    <td name='zerobounce_td_checkbox'><input id='zerobounce_".$name."IsActivated' type='checkbox' name='zerobounce-condition-settings-group[".$name."IsActivated]' value='1' ".checked( $options[$name.'IsActivated'], 1, false )." /></td>
                    <td name='zerobounce_td_explanation'>$explanationStr</td>
                </tr>
            </tbody>
        </table>";
}
//for the last two condition fields
function zerobounce_create_settings2_table($name, $explanationStr){
    $options = null;
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-condition-settings-group' );
    }


    
    if(!isset($options[$name.'IsActivated'])){
        $options[$name.'IsActivated'] = 0;
    }

    echo "<table class='zerobounce_settings_table'>
            <tbody>
                <tr>
                    <td name='zerobounce_td_settings'><input type='checkbox' name='zerobounce-condition-settings-group[".$name."IsActivated]' value='1' ".checked( $options[$name.'IsActivated'], 1, false )." /></td>
                    <td name='zerobounce_td_checkbox'></td>
                    <td name='zerobounce_td_explanation'>$explanationStr</td>
                </tr>
            </tbody>
        </table>";
}


/*
    Validation functions
*/

function zerobounce_main_field_validation($input){
    $options = null;
    $defaultOptions = get_option( 'zerobounce-main-settings-group' );
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = $defaultOptions;
    }


    if(!isset($_POST['SavePluginOptions'])) return $options;

    $options['IsActivated'] = (isset($input['IsActivated']) && $input['IsActivated'] == '1') ? 1 : 0;
    if($options['IsActivated'] == 0){
        add_settings_error( 'zerobounce-main-settings-group', 'error', 'Zero bOunce is disabled!' );
    }

    //set Url field
    if(isset($input['globalUrlIsActivated']) && $input['globalUrlIsActivated'] == '1'){
        $options['globalUrlIsActivated'] = 1;
        $options['globalUrlsIsActivated'] = 0;
    } else {
        $options['globalUrlIsActivated'] = 0;
        $options['globalUrlsIsActivated'] = 1;
    }

    //field check
    if(isset($input['globalUrl'])){
        if(strlen($input['globalUrl']) == 0){
            $options['globalUrl'] = '';
        } else if(zerobounce_is_url($input['globalUrl'])){
            $options['globalUrl'] = sanitize_text_field($input['globalUrl']);
        } else {
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'Invalid Global Url! Only use a full url like "http://www.google.com/".' );
        }
    }

    if(isset($input['globalUrls'])){
        $urls = trim($input['globalUrls']);
        if(strlen($urls) == 0){
            $options['globalUrls'] = '';
        } else {
            $urls = explode("\n", $urls);
            $urlsOk = true;
            $urls_clean = '';

            foreach($urls as $url){
                $urlOk = true;
                if(strpos($url, "\\") > -1){
                    $split = explode("\\", $url);

                    if(count($split) == 2){

                        $tmpUrl = trim($split[0]);
                        if(strlen($tmpUrl) > 0 && !zerobounce_is_url($tmpUrl)){
                            add_settings_error( 'zerobounce-main-settings-group', 'error', 'Url Error: '.$url.'<br>Only use a full url like "http://www.google.com/".' );
                            $urlsOk = false;
                        } else {
                            $urls_clean .= $url."<br/>";
                        }
                            
                    } else {
                        add_settings_error( 'zerobounce-main-settings-group', 'error', 'Url Error: '.$url.'<br>Only use a full url like "http://www.google.com/".' );
                        $urlOk = false;
                    }
                } else {
                    $url = trim($url);

                    if(!zerobounce_is_url($url)){
                        if(strlen($url) > 0){
                            add_settings_error( 'zerobounce-main-settings-group', 'error', 'Url Error: '.$url.'<br>Only use a full url like "http://www.google.com/".' );
                            $urlsOk = false;
                        }
                        
                    } else {
                        $urls_clean .= trim($url)."<br/>";
                    }
                }
                
                if(!$urlOk){
                    $urlsOk = false;
                }
            }

            if(zerobounce_string_endsWith($urls_clean, "<br/>")){
                $urls_clean = substr($urls_clean, 0, -5);
            }

            $urls_clean = str_replace('<br/><br/>', '<br/>', $urls_clean);
            $urls_clean = str_replace('<br/><br/>', '<br/>', $urls_clean);

            if($urlsOk){
                $options['globalUrls'] = $urls_clean;
            }
        }
    }

    //throw an error if by Global/s Url none url is set
    if(isset($input['IsActivated']) && $input['IsActivated'] == '1' && $options['globalUrlIsActivated'] == '1' && strlen($options['globalUrl']) == 0){
        add_settings_error( 'zerobounce-main-settings-group', 'error', 'The Global Url field is empty! Zerobounce is deactivated now.' );
        $options['IsActivated'] = 0;
    } else if(isset($input['IsActivated']) && $input['IsActivated'] == '1' && $options['globalUrlsIsActivated'] == '1' && strlen($options['globalUrls']) == 0){
        add_settings_error( 'zerobounce-main-settings-group', 'error', 'The Global Urls field is empty! Zerobounce is deactivated now.' );
        $options['IsActivated'] = 0;
    }

    $disableForFields = array('DeactivateForHomepage', 'DeactivateForPosts', 'DeactivateForPages');
    foreach($disableForFields as $field){
        $options[$field] = (isset($input[$field]) && $input[$field] == '1') ? '1' : '0';
    }

    $options['PutJSInsideHtml'] = (isset($input['PutJSInsideHtml']) && $input['PutJSInsideHtml'] == '1') ? 1 : 0; 

    $options['PutJSInsideHtmlOption'] = (isset($input['PutJSInsideHtml'])) ? $input['PutJSInsideHtmlOption'] : '';


    
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        ZeroBounceDB_ExtSites::setSettings($id, $options);

        return $defaultOptions;
    } 

    return $options;
}

function zerobounce_condition_field_validation($input){
    $options = null;
    $defaultOptions = get_option( 'zerobounce-condition-settings-group' );
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = $defaultOptions;
    }
   

    if(!isset($_POST['SavePluginOptions'])) return $options;

    //Activate if referrer contains
    $options['conditionReferrerContainsGoogle'] = (isset($input['conditionReferrerContainsGoogle']) && $input['conditionReferrerContainsGoogle'] == '1') ? 1 : 0;
    $options['conditionReferrerContainsFacebook'] = (isset($input['conditionReferrerContainsFacebook']) && $input['conditionReferrerContainsFacebook'] == '1') ? 1 : 0;
    $options['conditionReferrerContainsBing'] = (isset($input['conditionReferrerContainsBing']) && $input['conditionReferrerContainsBing'] == '1') ? 1 : 0;

    if(isset($input['conditionReferrerContains'])){
        $input['conditionReferrerContains'] = trim($input['conditionReferrerContains']);

        if(strpos($input['conditionReferrerContains'], "\n") != -1){
            $strings = explode("\n", $input['conditionReferrerContains']);
           
            $strings_clean = '';
            foreach($strings as $string){
                $strings_clean .= sanitize_text_field(trim($string)).'<br/>';
            }
            if(zerobounce_string_endsWith($strings_clean, "<br/>")){
                $strings_clean = substr($strings_clean, 0, -5);
            }

            $input['conditionReferrerContains'] = $strings_clean;
        }
        $text = $input['conditionReferrerContains'];
        $text = str_replace('<br/><br/>', '<br/>', $text);
        $text = str_replace('<br/><br/>', '<br/>', $text);

        $options['conditionReferrerContains'] = esc_sql($text);
    }

    if(isset($input['conditionReferrerContainsIsActivated'])) {
        if($input['conditionReferrerContainsIsActivated'] == '1' && strlen($input['conditionReferrerContains']) == 0 && !isset($input['conditionReferrerContainsGoogle']) && 
            !isset($input['conditionReferrerContainsFacebook']) && !isset($input['conditionReferrerContainsBing'])){
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'If "Activate if referer contains: " is activated, you  need to select at least a checkbox or enter a text. Setting is disabled now!' );
            $options['conditionReferrerContainsIsActivated'] = 0;
        } else {
            $options['conditionReferrerContainsIsActivated'] = ($input['conditionReferrerContainsIsActivated'] == '1') ? 1 : 0;          
        }
    } else {
        $options['conditionReferrerContainsIsActivated'] = 0;
    }

    if(isset($input['conditionDisableIfRefererContainsIsActivated'])) {
       
        $options['conditionDisableIfRefererContainsIsActivated'] = ($input['conditionDisableIfRefererContainsIsActivated'] == '1') ? 1 : 0; 

         if(isset($input['conditionDisableIfRefererContains'])){
            $input['conditionDisableIfRefererContains'] = trim($input['conditionDisableIfRefererContains']);

            if(strpos($input['conditionDisableIfRefererContains'], "\n") != -1){
                $strings = explode("\n", $input['conditionDisableIfRefererContains']);
               
                $strings_clean = '';
                foreach($strings as $string){
                    $strings_clean .= sanitize_text_field(trim($string)).'<br/>';
                }
                if(zerobounce_string_endsWith($strings_clean, "<br/>")){
                    $strings_clean = substr($strings_clean, 0, -5);
                }

                $input['conditionDisableIfRefererContains'] = $strings_clean;
            }
            $text = $input['conditionDisableIfRefererContains'];
            $text = str_replace('<br/><br/>', '<br/>', $text);
            $text = str_replace('<br/><br/>', '<br/>', $text);

            $options['conditionDisableIfRefererContains'] = esc_sql($text);
        }
    } else {
        $options['conditionDisableIfRefererContainsIsActivated'] = 0;
    }


    //Deactivate after x seconds
    if(isset($input['conditionTimeout'])){
        $int = intval($input['conditionTimeout']);
        if($int == 0){
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'For "Deactivate after x seconds" only numbers are allowed. For example: 20. Setting is set to 20 now!' );
            $options['conditionTimeout'] = 20;
        } else {
            $options['conditionTimeout'] = sanitize_text_field($input['conditionTimeout']);
        }
    }

    if(isset($input['conditionTimeoutIsActivated'])){
        if($input['conditionTimeoutIsActivated'] == '1' && strlen($input['conditionTimeout']) == 0){
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'For "Deactivate after x seconds" is empty. Setting is disabled now!' );
            $options['conditionTimeoutIsActivated'] = 0;
        } else {
            $options['conditionTimeoutIsActivated'] = ($input['conditionTimeoutIsActivated'] == '1') ? 1 : 0;
        }
    } else {
        $options['conditionTimeoutIsActivated'] = 0;
    }
    
    if(isset($input['conditionEnableOnlyIfUrlContainsIsActivated'])) {
       
        $options['conditionEnableOnlyIfUrlContainsIsActivated'] = ($input['conditionEnableOnlyIfUrlContainsIsActivated'] == '1') ? 1 : 0;   
        if(isset($input['conditionEnableOnlyIfUrlContains'])){
            $input['conditionEnableOnlyIfUrlContains'] = trim($input['conditionEnableOnlyIfUrlContains']);

            if(strpos($input['conditionEnableOnlyIfUrlContains'], "\n") != -1){
                $strings = explode("\n", $input['conditionEnableOnlyIfUrlContains']);
               
                $strings_clean = '';
                foreach($strings as $string){
                    $strings_clean .= sanitize_text_field(trim($string)).'<br/>';
                }
                if(zerobounce_string_endsWith($strings_clean, "<br/>")){
                    $strings_clean = substr($strings_clean, 0, -5);
                }

                $input['conditionEnableOnlyIfUrlContains'] = $strings_clean;
            }
            $text = $input['conditionEnableOnlyIfUrlContains'];
            $text = str_replace('<br/><br/>', '<br/>', $text);
            $text = str_replace('<br/><br/>', '<br/>', $text);

            $options['conditionEnableOnlyIfUrlContains'] = esc_sql($text);
        }
        
    } else {
        $options['conditionEnableOnlyIfUrlContainsIsActivated'] = 0;
    }

    if(isset($input['conditionDisableIfUrlContainsIsActivated'])) {
       
        $options['conditionDisableIfUrlContainsIsActivated'] = ($input['conditionDisableIfUrlContainsIsActivated'] == '1') ? 1 : 0;   
        if(isset($input['conditionDisableIfUrlContains'])){
            $input['conditionDisableIfUrlContains'] = trim($input['conditionDisableIfUrlContains']);

            if(strpos($input['conditionDisableIfUrlContains'], "\n") != -1){
                $strings = explode("\n", $input['conditionDisableIfUrlContains']);
               
                $strings_clean = '';
                foreach($strings as $string){
                    $strings_clean .= sanitize_text_field(trim($string)).'<br/>';
                }
                if(zerobounce_string_endsWith($strings_clean, "<br/>")){
                    $strings_clean = substr($strings_clean, 0, -5);
                }

                $input['conditionDisableIfUrlContains'] = $strings_clean;
            }
            $text = $input['conditionDisableIfUrlContains'];
            $text = str_replace('<br/><br/>', '<br/>', $text);
            $text = str_replace('<br/><br/>', '<br/>', $text);

            $options['conditionDisableIfUrlContains'] = esc_sql($text);
        }
        
    } else {
        $options['conditionDisableIfUrlContainsIsActivated'] = 0;
    }


   


    //Activate if wants to exit the site after visiting several pages
    $options['conditionVisitedSeveralPagesIsActivated'] = 0;/*
    if(isset($input['conditionVisitedSeveralPagesIsActivated'])){
        $options['conditionVisitedSeveralPagesIsActivated'] = ($input['conditionVisitedSeveralPagesIsActivated'] == '1') ? 1 : 0;
    } else {
        $options['conditionVisitedSeveralPagesIsActivated'] = 0;
    }*/

    //Redirect by device
    if(isset($input['conditionByDevice'])){
        if($input['conditionByDevice'] == 'Mobil' || $input['conditionByDevice'] == 'Desktop'){
            $options['conditionByDevice'] = $input['conditionByDevice'];
        } else {
            $options['conditionByDevice'] = '';
        }
    } else  {
        $options['conditionByDevice'] = '';
    }
   

    if(isset($input['conditionByDeviceIsActivated'])){
        if($input['conditionByDeviceIsActivated'] == '1' && strlen($options['conditionByDevice']) == 0){
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'Redirect by device: Choose one option! Setting is disabled now!' );
            $options['conditionByDeviceIsActivated'] = 0;
        } else {
            $options['conditionByDeviceIsActivated'] = ($input['conditionByDeviceIsActivated'] == '1') ? 1 : 0;
        }
    } else {
        $options['conditionByDeviceIsActivated'] = 0;
    }


    //redirect for mobile deivces
    if(isset($input['conditionRedirectMobileUrlsIsActivated'])) {
       
        $options['conditionRedirectMobileUrlsIsActivated'] = ($input['conditionRedirectMobileUrlsIsActivated'] == '1') ? 1 : 0;   
        if(isset($input['conditionRedirectMobileUrls'])){
            $input['conditionRedirectMobileUrls'] = trim($input['conditionRedirectMobileUrls']);

            if(strpos($input['conditionRedirectMobileUrls'], "\n") != -1){
                $strings = explode("\n", $input['conditionRedirectMobileUrls']);
               
                $strings_clean = '';
                foreach($strings as $string){
                    $strings_clean .= sanitize_text_field(trim($string)).'<br/>';
                }
                if(zerobounce_string_endsWith($strings_clean, "<br/>")){
                    $strings_clean = substr($strings_clean, 0, -5);
                }

                $input['conditionRedirectMobileUrls'] = $strings_clean;
            }
            $text = $input['conditionRedirectMobileUrls'];
            $text = str_replace('<br/><br/>', '<br/>', $text);
            $text = str_replace('<br/><br/>', '<br/>', $text);

            $options['conditionRedirectMobileUrls'] = esc_sql($text);
        }
        
    } else {
        $options['conditionRedirectMobileUrlsIsActivated'] = 0;
    }


    if(isset($input['conditionPercentOfVisitors'])){
        if($input['conditionPercentOfVisitorsIsActivated'] == '1' && strlen($input['conditionPercentOfVisitors']) == 0){
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'Redirect x percent of visitors: Please write a number in the field! Setting is disabled now!' );
            $options['conditionPercentOfVisitorsIsActivated'] = 0;
        } else {
            $int = intval($input['conditionPercentOfVisitors']);
            if($int == 0 || $int > 100){
                add_settings_error( 'zerobounce-main-settings-group', 'error', 'Redirect x percent of visitors: Please write a number between 1 - 100 in the field! For example "20". Setting is disabled now!' );
                $options['conditionPercentOfVisitorsIsActivated'] = 0;
            } else {
                $options['conditionPercentOfVisitors'] = sanitize_text_field($input['conditionPercentOfVisitors']);
                $options['conditionPercentOfVisitorsIsActivated'] = ($input['conditionPercentOfVisitorsIsActivated'] == '1') ? 1 : 0;
            }
        }
    }else {
        $options['conditionPercentOfVisitorsIsActivated'] = 0;
    }

    $options['conditionVisitorCloseIntentionIsActivated'] = (isset($input['conditionVisitorCloseIntentionIsActivated']) && $input['conditionVisitorCloseIntentionIsActivated'] == '1') ? 1 : 0;

    if(isset($input['conditionRedirectVisitorOnlyOnceIsActivated'])) {
        if($input['conditionRedirectVisitorOnlyOnceIsActivated'] == '1' && strlen($input['conditionRedirectVisitorOnlyOnce']) == 0){
            add_settings_error( 'zerobounce-main-settings-group', 'error', 'Redirect visitor only once: You need to write a number like \'23\' in the field! Setting is disabled now!' );
            $options['conditionRedirectVisitorOnlyOnceIsActivated'] = 0;
        } else {
            $int = intval($input['conditionRedirectVisitorOnlyOnce']);
            if($int == 0){
                add_settings_error( 'zerobounce-main-settings-group', 'error', 'Redirect visitor only once: You need to write a valid number like \'23\' in the field! Setting is disabled now!' );
                $options['conditionRedirectVisitorOnlyOnceIsActivated'] = 0;
            } else {
                $options['conditionRedirectVisitorOnlyOnceIsActivated'] = 1;
                $options['conditionRedirectVisitorOnlyOnce'] = $input['conditionRedirectVisitorOnlyOnce'];
            }
        }
        
    } else {
        $options['conditionRedirectVisitorOnlyOnceIsActivated'] = 0;
    }

    zerobounce_delete_js_files();


    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        ZeroBounceDB_ExtSites::setSettings($id, $options);

        return $defaultOptions;
    } 
    return $options;
}


function zerobounce_is_url($url) {
    $url = trim($url);
    if(!zerobounce_string_startsWith($url, 'http://') && !zerobounce_string_startsWith($url, 'https://')){
        return false;
    } else if(!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)){
        return false;
    }
    return true;
}


