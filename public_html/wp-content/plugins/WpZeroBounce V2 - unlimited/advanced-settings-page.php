<?php

$parentDir = dirname(__FILE__);

if(substr($parentDir, -strlen("/")) === "/"){
	$parentDir = substr($parentDir, 0, -1);
} 

if(!class_exists('ZeroBounceDB_ExtSites')){
	require_once($parentDir.'/db_ext_sites.php');
}
if(!class_exists('ZeroBounceExtSites')){
	require_once($parentDir.'/ext-sites.php');
}


if(isset($_GET['zb-del-redirectbyreferer'])){
	$md5 = $_GET['zb-del-redirectbyreferer'];

	$groups = null;
	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options = ZeroBounceDB_ExtSites::getSettings($id);
		$groups = $options['groupsReferer'];
	} else {
		$options = get_option( 'zerobounce-redirectByReferer-group' );
		$groups = $options['groups'];
	}


	unset($groups[$md5]);

	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options['groupsReferer'] = $groups;
		ZeroBounceDB_ExtSites::setSettings($id, $options);
	} else {
		$options['groups'] = $groups;
		update_option('zerobounce-redirectByReferer-group', $options);
	}
	



	zerobounce_delete_js_files();

	$redirect = zerobounce_GetCurrentUrl();
	$redirect = substr($redirect, 0, strpos($redirect, '&zb-del-redirectbyreferer'));

	zerobounce_redirect($redirect);
}

if(isset($_GET['zb-del-redirectbycountry'])){
	$md5 = $_GET['zb-del-redirectbycountry'];


	$groups = null;
	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options = ZeroBounceDB_ExtSites::getSettings($id);
		$groups = $options['groupsCountry'];
	} else {
		$options = get_option( 'zerobounce-redirectByCountry-group' );
		$groups = $options['groups'];
	}

	unset($groups[$md5]);


	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options['groupsCountry'] = $groups;
		ZeroBounceDB_ExtSites::setSettings($id, $options);
	} else {
		$options['groups'] = $groups;
		update_option('zerobounce-redirectByCountry-group', $options);
	}

	zerobounce_delete_js_files();

	$redirect = zerobounce_GetCurrentUrl();
	$redirect = substr($redirect, 0, strpos($redirect, '&zb-del-redirectbycountry'));
	zerobounce_redirect($redirect);
}

if(isset($_GET['zb-del-redirectbyTag'])){
	$md5 = $_GET['zb-del-redirectbyTag'];

	$groups = null;
	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounce_ExtSites::getChangeNumber());
		$options = ZeroBounceDB_ExtSites::getSettings($id);
		$groups = $options['groupsTag'];
	} else {
		$options = get_option( 'zerobounce-redirectByTag-group' );
		$groups = $options['groups'];
	}


	unset($groups[$md5]);


	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options['groupsTag'] = $groups;
		ZeroBounceDB_ExtSites::setSettings($id, $options);
	} else {
		$options['groups'] = $groups;
		update_option('zerobounce-redirectByTag-group', $options);
	}

	
	zerobounce_delete_js_files();

	$redirect = zerobounce_GetCurrentUrl();
	$redirect = substr($redirect, 0, strpos($redirect, '&zb-del-redirectbyTag'));

	zerobounce_redirect($redirect);
}

if(isset($_GET['zb-del-redirectbyCategory'])){
	$md5 = $_GET['zb-del-redirectbyCategory'];

	$groups = null;
	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options = ZeroBounceDB_ExtSites::getSettings($id);
		$groups = $options['groupsCategory'];
	} else {
		$options = get_option( 'zerobounce-redirectByCategory-group' );
		$groups = $options['groups'];
	}


	unset($groups[$md5]);

	if(ZeroBounceExtSites::isChange()){
		$id = intval(ZeroBounceExtSites::getChangeNumber());
		$options['groupsCategory'] = $groups;
		ZeroBounceDB_ExtSites::setSettings($id, $options);
	} else {
		$options['groups'] = $groups;
		update_option('zerobounce-redirectByCategory-group', $options);
	}


	zerobounce_delete_js_files();

	$redirect = zerobounce_GetCurrentUrl();
	$redirect = substr($redirect, 0, strpos($redirect, '&zb-del-redirectbyCategory'));

	zerobounce_redirect($redirect);
}



//advanced_options page
add_action( 'admin_init', 'zerobounce_advanced_options_page_init' );
function zerobounce_advanced_options_page_init(){
	if(ZEROBOUNCE_ITEM_NAME == ZEROBOUNCE_SIMPLE_LICENSE){
		return;
	}

	//redirect by referer
    register_setting( 'zerobounce-settings-group', 'zerobounce-redirectByReferer-group', 'zerobounce_redirectByReferer_validation');
    add_settings_section( 'section-redirectByReferer', 'Redirect by referer', 'zerobounce_section_redirectByReferer_callback', 'zerobounce-plugin-advanced' );
    add_settings_field( 'zerobounce-refererAdd', 'Add: ', 'zerobounce_field_redirectByReferer_Add_callback', 'zerobounce-plugin-advanced', 'section-redirectByReferer' );
    add_settings_field( 'zerobounce-refererAll', 'Existing redirects: ', 'zerobounce_field_redirectByReferer_All_callback', 'zerobounce-plugin-advanced', 'section-redirectByReferer' );

    //country
    register_setting( 'zerobounce-settings-group', 'zerobounce-redirectByCountry-group', 'zerobounce_redirectByCountry_validation');
    add_settings_section( 'section-redirectByCountry', 'Redirect by Country', 'zerobounce_section_redirectByCountry_callback', 'zerobounce-plugin-advanced' );
    add_settings_field( 'zerobounce-refererAdd', 'Add: ', 'zerobounce_field_redirectByCountry_Add_callback', 'zerobounce-plugin-advanced', 'section-redirectByCountry' );
    add_settings_field( 'zerobounce-refererAll', 'Existing redirects: ', 'zerobounce_field_redirectByCountry_All_callback', 'zerobounce-plugin-advanced', 'section-redirectByCountry' );

    if(!isset($_POST['zb_extsite_change'])){
		//tag
	    register_setting( 'zerobounce-settings-group', 'zerobounce-redirectByTag-group', 'zerobounce_redirectByTag_validation');
	    add_settings_section( 'section-redirectByTag', 'Redirect by Tag', 'zerobounce_section_redirectByTag_callback', 'zerobounce-plugin-advanced' );
	    add_settings_field( 'zerobounce-TagAdd', 'Add: ', 'zerobounce_field_redirectByTag_Add_callback', 'zerobounce-plugin-advanced', 'section-redirectByTag' );
	    add_settings_field( 'zerobounce-TagAll', 'Existing redirects: ', 'zerobounce_field_redirectByTag_All_callback', 'zerobounce-plugin-advanced', 'section-redirectByTag' );

	    //category
	    register_setting( 'zerobounce-settings-group', 'zerobounce-redirectByCategory-group', 'zerobounce_redirectByCategory_validation');
	    add_settings_section( 'section-redirectByCategory', 'Redirect by Category', 'zerobounce_section_redirectByCategory_callback', 'zerobounce-plugin-advanced' );
	    add_settings_field( 'zerobounce-CategoryAdd', 'Add: ', 'zerobounce_field_redirectByCategory_Add_callback', 'zerobounce-plugin-advanced', 'section-redirectByCategory' );
	    add_settings_field( 'zerobounce-CategoryAll', 'Existing redirects: ', 'zerobounce_field_redirectByCategory_All_callback', 'zerobounce-plugin-advanced', 'section-redirectByCategory' );
    }
    
	

   
}

/* Functions redirect by Referer */

function zerobounce_section_redirectByReferer_callback(){
    echo "<div style='float:left;width:50%'>Here you can set custom redirects for each referer.</div>";
    if(!ZeroBounceExtSites::isChange()){
    	echo "<div style='float:right'><a class='zerobounce_documentation_button' href='http://forcespark.net/documentation/' target='_blank'>Documentation</a></div>";
    }
    

}

function zerobounce_field_redirectByReferer_Add_callback(){
     	echo "<table class='zerobounce_advanced_add'>
            <tbody>
                <tr >
                    <td name='zerobounce_td_field'>Referer</td>
                    <td name='zerobounce_td_explanation'>Target Url</td>
                 </tr>
                 <tr>
                    <td name='zerobounce_td_field'><input id='referer' type='text' placeholder='referer' name='zerobounce-redirectByReferer-group[refererContains]' /></td>
                    <td name='zerobounce_td_explanation'><input type='text' id='inputRefererTargetUrl' placeholder='target url' name='zerobounce-redirectByReferer-group[refererTargetUrl]' /></td>
               </tr>
            </tbody>
        </table>";

        submit_button('Add', 'primary', 'addReferer');
}

function zerobounce_field_redirectByReferer_All_callback(){
	$options = null;
    
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-redirectByReferer-group' );
    }

	if(ZeroBounceExtSites::isChange() && !isset($options['groupsReferer'])){
		$options['groupsReferer'] = array();
	} else {
		if(!isset($options['groups'])){
       		$options['groups'] = array();
    	}
	}

    
	echo "<table class='zerobounce_advancedOptionsTable' id='zerobounce_advaced_settings_referer_table'>
        	<tbody>
        		<tr class='zerobounce_table_head'>
	        		<td>Referrer</td>
	        		<td>Target url</td>
	        		<td>Delete</td>
	        	<tr>	
        	";

    if(ZeroBounceExtSites::isChange()){  
		foreach($options['groupsReferer'] as $group){
			$referer = $group['referer'];
			$url = $group['url'];
			$md5 = md5($referer);
			echo "<tr class='zerobounce_advanced_entries' id='$md5'>
	        	<td><input type='text' value='$referer' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a group='groupsReferer' entryId='$md5' href='#' action='delete'>Delete</a></td>
	        </tr>";
		}
    } else {
    	foreach($options['groups'] as $group){
			$referer = $group['referer'];
			$url = $group['url'];
			$delete = zerobounce_GetCurrentUrl().'&zb-del-redirectbyreferer='.md5($referer);
			echo "<tr class='zerobounce_advanced_entries'>
	        	<td><input type='text' value='$referer' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a href='$delete'>Delete</a></td>
	        </tr>";
		}
    }



	echo "</tbody>
        </table>";
}

function zerobounce_redirectByReferer_validation($input){
	$options = null;
    $defaultOptions = get_option( 'zerobounce-redirectByReferer-group' );
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = $defaultOptions;
    }

	
	if(!isset($_POST['addReferer'])) return $options;


	$groups = null;
	if(isset($_POST['zb_extsite_change_id'])){
		if(isset($options['groupsReferer'])){
			$groups = $options['groupsReferer'];
		} else {
			$groups = array();
		}
	 	
	 } else {
	 	if(isset($options['groups'])){
			$groups = $options['groups'];
		} else {
			$groups = array();
		}
	 }
	

	$url = '';
	$referer = '';

	if(isset($input['refererTargetUrl'])){
		$url = $input['refererTargetUrl'];
	} else {
		$url = $_POST['zerobounce-redirectByReferer-group']['refererTargetUrl'];
	}

	if(isset($input['refererContains'])){
		$referer = $input['refererContains'];
	} else {
		$referer = $_POST['zerobounce-redirectByReferer-group']['refererContains'];
	}


	if(!zerobounce_is_url($url)){
		add_settings_error( 'zerobounce-redirectByReferer-group', 'error', 'Url is not valid!');
	} else if(strlen($referer) == 0){
		add_settings_error( 'zerobounce-redirectByReferer-group', 'error', 'Referer must have at least one char!' );
	} else {
		$groups[md5($referer)] = array('referer' =>  $referer, 'url' => $url);
	}


	 if(isset($_POST['zb_extsite_change_id'])){
	 	$options['groupsReferer'] = $groups;
	 } else {
	 	$options['groups'] = $groups;
	 }

	
	zerobounce_delete_js_files();


	if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        ZeroBounceDB_ExtSites::setSettings($id, $options);

        return $defaultOptions;
    } 

	return $options;
}

/* Functiond redirect by country */
       

function zerobounce_section_redirectByCountry_callback(){
    echo "<div style='float:left;width:50%'>Here you can set custom redirects for each country.</div>";
   

}

function zerobounce_field_redirectByCountry_Add_callback(){
     	echo "<table class='zerobounce_advanced_add' >
            <tbody>
                <tr >
                    <td  name='zerobounce_td_field'>Country</td>
                    <td  name='zerobounce_td_explanation'>Target Url</td>
                 </tr>
                 <tr >
                    <td>
	                    <select id='countries' name='zerobounce-redirectByCountry-group[country]' >
						<option value='AF'>Afghanistan</option>
						<option value='AX'>Åland Islands</option>
						<option value='AL'>Albania</option>
						<option value='DZ'>Algeria</option>
						<option value='AS'>American Samoa</option>
						<option value='AD'>Andorra</option>
						<option value='AO'>Angola</option>
						<option value='AI'>Anguilla</option>
						<option value='AQ'>Antarctica</option>
						<option value='AG'>Antigua and Barbuda</option>
						<option value='AR'>Argentina</option>
						<option value='AM'>Armenia</option>
						<option value='AW'>Aruba</option>
						<option value='AU'>Australia</option>
						<option value='AT'>Austria</option>
						<option value='AZ'>Azerbaijan</option>
						<option value='BS'>Bahamas</option>
						<option value='BH'>Bahrain</option>
						<option value='BD'>Bangladesh</option>
						<option value='BB'>Barbados</option>
						<option value='BY'>Belarus</option>
						<option value='BE'>Belgium</option>
						<option value='BZ'>Belize</option>
						<option value='BJ'>Benin</option>
						<option value='BM'>Bermuda</option>
						<option value='BT'>Bhutan</option>
						<option value='BO'>Bolivia</option>
						<option value='BA'>Bosnia and Herzegovina</option>
						<option value='BW'>Botswana</option>
						<option value='BV'>Bouvet Island</option>
						<option value='BR'>Brazil</option>
						<option value='IO'>British Indian Ocean Territory</option>
						<option value='BN'>Brunei Darussalam</option>
						<option value='BG'>Bulgaria</option>
						<option value='BF'>Burkina Faso</option>
						<option value='BI'>Burundi</option>
						<option value='KH'>Cambodia</option>
						<option value='CM'>Cameroon</option>
						<option value='CA'>Canada</option>
						<option value='CV'>Cape Verde</option>
						<option value='KY'>Cayman Islands</option>
						<option value='CF'>Central African Republic</option>
						<option value='TD'>Chad</option>
						<option value='CL'>Chile</option>
						<option value='CN'>China</option>
						<option value='CX'>Christmas Island</option>
						<option value='CC'>Cocos (Keeling) Islands</option>
						<option value='CO'>Colombia</option>
						<option value='KM'>Comoros</option>
						<option value='CG'>Congo</option>
						<option value='CD'>Congo, The Democratic Republic of The</option>
						<option value='CK'>Cook Islands</option>
						<option value='CR'>Costa Rica</option>
						<option value='CI'>Cote D'ivoire</option>
						<option value='HR'>Croatia</option>
						<option value='CU'>Cuba</option>
						<option value='CY'>Cyprus</option>
						<option value='CZ'>Czech Republic</option>
						<option value='DK'>Denmark</option>
						<option value='DJ'>Djibouti</option>
						<option value='DM'>Dominica</option>
						<option value='DO'>Dominican Republic</option>
						<option value='EC'>Ecuador</option>
						<option value='EG'>Egypt</option>
						<option value='SV'>El Salvador</option>
						<option value='GQ'>Equatorial Guinea</option>
						<option value='ER'>Eritrea</option>
						<option value='EE'>Estonia</option>
						<option value='ET'>Ethiopia</option>
						<option value='FK'>Falkland Islands (Malvinas)</option>
						<option value='FO'>Faroe Islands</option>
						<option value='FJ'>Fiji</option>
						<option value='FI'>Finland</option>
						<option value='FR'>France</option>
						<option value='GF'>French Guiana</option>
						<option value='PF'>French Polynesia</option>
						<option value='TF'>French Southern Territories</option>
						<option value='GA'>Gabon</option>
						<option value='GM'>Gambia</option>
						<option value='GE'>Georgia</option>
						<option value='DE'>Germany</option>
						<option value='GH'>Ghana</option>
						<option value='GI'>Gibraltar</option>
						<option value='GR'>Greece</option>
						<option value='GL'>Greenland</option>
						<option value='GD'>Grenada</option>
						<option value='GP'>Guadeloupe</option>
						<option value='GU'>Guam</option>
						<option value='GT'>Guatemala</option>
						<option value='GG'>Guernsey</option>
						<option value='GN'>Guinea</option>
						<option value='GW'>Guinea-bissau</option>
						<option value='GY'>Guyana</option>
						<option value='HT'>Haiti</option>
						<option value='HM'>Heard Island and Mcdonald Islands</option>
						<option value='VA'>Holy See (Vatican City State)</option>
						<option value='HN'>Honduras</option>
						<option value='HK'>Hong Kong</option>
						<option value='HU'>Hungary</option>
						<option value='IS'>Iceland</option>
						<option value='IN'>India</option>
						<option value='ID'>Indonesia</option>
						<option value='IR'>Iran, Islamic Republic of</option>
						<option value='IQ'>Iraq</option>
						<option value='IE'>Ireland</option>
						<option value='IM'>Isle of Man</option>
						<option value='IL'>Israel</option>
						<option value='IT'>Italy</option>
						<option value='JM'>Jamaica</option>
						<option value='JP'>Japan</option>
						<option value='JE'>Jersey</option>
						<option value='JO'>Jordan</option>
						<option value='KZ'>Kazakhstan</option>
						<option value='KE'>Kenya</option>
						<option value='KI'>Kiribati</option>
						<option value='KP'>Korea, Democratic People's Republic of</option>
						<option value='KR'>Korea, Republic of</option>
						<option value='KW'>Kuwait</option>
						<option value='KG'>Kyrgyzstan</option>
						<option value='LA'>Lao People's Democratic Republic</option>
						<option value='LV'>Latvia</option>
						<option value='LB'>Lebanon</option>
						<option value='LS'>Lesotho</option>
						<option value='LR'>Liberia</option>
						<option value='LY'>Libyan Arab Jamahiriya</option>
						<option value='LI'>Liechtenstein</option>
						<option value='LT'>Lithuania</option>
						<option value='LU'>Luxembourg</option>
						<option value='MO'>Macao</option>
						<option value='MK'>Macedonia, The Former Yugoslav Republic of</option>
						<option value='MG'>Madagascar</option>
						<option value='MW'>Malawi</option>
						<option value='MY'>Malaysia</option>
						<option value='MV'>Maldives</option>
						<option value='ML'>Mali</option>
						<option value='MT'>Malta</option>
						<option value='MH'>Marshall Islands</option>
						<option value='MQ'>Martinique</option>
						<option value='MR'>Mauritania</option>
						<option value='MU'>Mauritius</option>
						<option value='YT'>Mayotte</option>
						<option value='MX'>Mexico</option>
						<option value='FM'>Micronesia, Federated States of</option>
						<option value='MD'>Moldova, Republic of</option>
						<option value='MC'>Monaco</option>
						<option value='MN'>Mongolia</option>
						<option value='ME'>Montenegro</option>
						<option value='MS'>Montserrat</option>
						<option value='MA'>Morocco</option>
						<option value='MZ'>Mozambique</option>
						<option value='MM'>Myanmar</option>
						<option value='NA'>Namibia</option>
						<option value='NR'>Nauru</option>
						<option value='NP'>Nepal</option>
						<option value='NL'>Netherlands</option>
						<option value='AN'>Netherlands Antilles</option>
						<option value='NC'>New Caledonia</option>
						<option value='NZ'>New Zealand</option>
						<option value='NI'>Nicaragua</option>
						<option value='NE'>Niger</option>
						<option value='NG'>Nigeria</option>
						<option value='NU'>Niue</option>
						<option value='NF'>Norfolk Island</option>
						<option value='MP'>Northern Mariana Islands</option>
						<option value='NO'>Norway</option>
						<option value='OM'>Oman</option>
						<option value='PK'>Pakistan</option>
						<option value='PW'>Palau</option>
						<option value='PS'>Palestinian Territory, Occupied</option>
						<option value='PA'>Panama</option>
						<option value='PG'>Papua New Guinea</option>
						<option value='PY'>Paraguay</option>
						<option value='PE'>Peru</option>
						<option value='PH'>Philippines</option>
						<option value='PN'>Pitcairn</option>
						<option value='PL'>Poland</option>
						<option value='PT'>Portugal</option>
						<option value='PR'>Puerto Rico</option>
						<option value='QA'>Qatar</option>
						<option value='RE'>Reunion</option>
						<option value='RO'>Romania</option>
						<option value='RU'>Russian Federation</option>
						<option value='RW'>Rwanda</option>
						<option value='SH'>Saint Helena</option>
						<option value='KN'>Saint Kitts and Nevis</option>
						<option value='LC'>Saint Lucia</option>
						<option value='PM'>Saint Pierre and Miquelon</option>
						<option value='VC'>Saint Vincent and The Grenadines</option>
						<option value='WS'>Samoa</option>
						<option value='SM'>San Marino</option>
						<option value='ST'>Sao Tome and Principe</option>
						<option value='SA'>Saudi Arabia</option>
						<option value='SN'>Senegal</option>
						<option value='RS'>Serbia</option>
						<option value='SC'>Seychelles</option>
						<option value='SL'>Sierra Leone</option>
						<option value='SG'>Singapore</option>
						<option value='SK'>Slovakia</option>
						<option value='SI'>Slovenia</option>
						<option value='SB'>Solomon Islands</option>
						<option value='SO'>Somalia</option>
						<option value='ZA'>South Africa</option>
						<option value='GS'>South Georgia and The South Sandwich Islands</option>
						<option value='ES'>Spain</option>
						<option value='LK'>Sri Lanka</option>
						<option value='SD'>Sudan</option>
						<option value='SR'>Suriname</option>
						<option value='SJ'>Svalbard and Jan Mayen</option>
						<option value='SZ'>Swaziland</option>
						<option value='SE'>Sweden</option>
						<option value='CH'>Switzerland</option>
						<option value='SY'>Syrian Arab Republic</option>
						<option value='TW'>Taiwan, Province of China</option>
						<option value='TJ'>Tajikistan</option>
						<option value='TZ'>Tanzania, United Republic of</option>
						<option value='TH'>Thailand</option>
						<option value='TL'>Timor-leste</option>
						<option value='TG'>Togo</option>
						<option value='TK'>Tokelau</option>
						<option value='TO'>Tonga</option>
						<option value='TT'>Trinidad and Tobago</option>
						<option value='TN'>Tunisia</option>
						<option value='TR'>Turkey</option>
						<option value='TM'>Turkmenistan</option>
						<option value='TC'>Turks and Caicos Islands</option>
						<option value='TV'>Tuvalu</option>
						<option value='UG'>Uganda</option>
						<option value='UA'>Ukraine</option>
						<option value='AE'>United Arab Emirates</option>
						<option value='GB'>United Kingdom</option>
						<option value='US'>United States</option>
						<option value='UM'>United States Minor Outlying Islands</option>
						<option value='UY'>Uruguay</option>
						<option value='UZ'>Uzbekistan</option>
						<option value='VU'>Vanuatu</option>
						<option value='VE'>Venezuela</option>
						<option value='VN'>Viet Nam</option>
						<option value='VG'>Virgin Islands, British</option>
						<option value='VI'>Virgin Islands, U.S.</option>
						<option value='WF'>Wallis and Futuna</option>
						<option value='EH'>Western Sahara</option>
						<option value='YE'>Yemen</option>
						<option value='ZM'>Zambia</option>
						<option value='ZW'>Zimbabwe</option>
						</select>
					</td>
                    <td name='zerobounce_td_explanation'><input type='text' id='inputCountryTargetUrl' placeholder='target url' name='zerobounce-redirectByCountry-group[CountryTargetUrl]' /></td>
               </tr>
            </tbody>
        </table>";
        submit_button('Add', 'primary', 'addCountry');
}

function zerobounce_field_redirectByCountry_All_callback(){
	$options = null;
    
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-redirectByCountry-group' );
    }

	if(ZeroBounceExtSites::isChange() && !isset($options['groupsCountry'])){
		$options['groupsCountry'] = array();
	} else {
		if(!isset($options['groups'])){
       		$options['groups'] = array();
    	}
	}


	echo "<table class='zerobounce_advancedOptionsTable' id='zerobounce_advaced_settings_country_table'>
        	<tbody>
        		<tr class='zerobounce_table_head' >
	        		<td>Country</td>
	        		<td>Target url</td>
	        		<td>Delete</td>	
	        	</tr>
        	";


	if(ZeroBounceExtSites::isChange()){
		foreach($options['groupsCountry'] as $group){
			$country = $group['country'];
			$url = $group['url'];
			$country = zerobounce_getCountryForIso($country);
			$md5 = $country;


			echo "<tr class='zerobounce_advanced_entries' id='$country'>
	        	<td><input type='text' value='$country' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a group='groupsCountry' entryId='$country' href='#' action='delete'>Delete</a></td>
	        </tr>";
		}
	} else {
		foreach($options['groups'] as $group){
			$country = $group['country'];
			$url = $group['url'];
			$delete = zerobounce_GetCurrentUrl().'&zb-del-redirectbycountry='.$country;
			$country = zerobounce_getCountryForIso($country);

			echo "<tr class='zerobounce_advanced_entries' id='$md5'>
	        	<td><input type='text' value='$country' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a href='$delete'>Delete</a></td>
	        </tr>";
		}
	}


	

	echo "</tbody>
        </table>";
}

function zerobounce_redirectByCountry_validation($input){
	$options = null;
    $defaultOptions = get_option( 'zerobounce-redirectByCountry-group' );
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = $defaultOptions;
    }

	if(!isset($_POST['addCountry'])) return $options;
	
	
	$groups = null;
	if(isset($_POST['zb_extsite_change_id'])){
		if(isset($options['groupsCountry'])){
			$groups = $options['groupsCountry'];
		} else {
			$groups = array();
		}
	 	
	 } else {
	 	if(isset($options['groups'])){
			$groups = $options['groups'];
		} else {
			$groups = array();
		}
	 }




	$country = '';
	$url = '';

	if(isset($input['country'])){
		$country = $input['country'];
	} else {
		$country = $_POST['zerobounce-redirectByCountry-group']['country'];
	}
	

	if(isset($input['CountryTargetUrl'])){
		$url = $input['CountryTargetUrl'];
	} else {
		$url = $_POST['zerobounce-redirectByCountry-group']['CountryTargetUrl'];
	}


	if(!zerobounce_is_url($url)){
		add_settings_error( 'zerobounce-redirectByCountry-group', 'error', 'Url is not valid!' );
	} else {
		$groups[$country] = array('country' =>  $country, 'url' => $url);
	}


	if(isset($_POST['zb_extsite_change_id'])){
	 	$options['groupsCountry'] = $groups;
	 } else {
	 	$options['groups'] = $groups;
	 }


	zerobounce_delete_js_files();

	if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        ZeroBounceDB_ExtSites::setSettings($id, $options);

        return $defaultOptions;
    } 

	return $options;
}


/* Functions redirect by Tag */

function zerobounce_section_redirectByTag_callback(){
    echo "<div style='float:left;width:50%'>Here you can set custom redirects for each tag.</div>";
   

}

function zerobounce_field_redirectByTag_Add_callback(){
	$args = array();
	$options = '';
		
		
		$tags = get_tags( $args );
		foreach ($tags as $tag) {
			$tagName = $tag->name;
			$options .= "<option value='$tagName'>$tagName</option>";
		}

     	echo "<table class='zerobounce_advanced_add' >
            <tbody>
                <tr >
                    <td name='zerobounce_td_field'>Tag</td>
                    <td name='zerobounce_td_explanation'>Target Url</td>
                 </tr>
                 <tr>
                    <td name='zerobounce_td_field'><input id='tags' name='zerobounce-redirectByTag-group[TagContains]' type='text' placeholder='tags'></td>
                    <td name='zerobounce_td_explanation'><input type='text' id='inputTagTargetUrl' placeholder='target url' name='zerobounce-redirectByTag-group[TagTargetUrl]' /></td>
               </tr>
            </tbody>
        </table>";
        submit_button('Add', 'primary', 'addTag');
}

function zerobounce_field_redirectByTag_All_callback(){
	$options = null;
    
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-redirectByTag-group' );
    }

	if(ZeroBounceExtSites::isChange() && !isset($options['groupsTag'])){
		$options['groupsTag'] = array();
	} else {
		if(!isset($options['groups'])){
       		$options['groups'] = array();
    	}
	}
	


	
    if(!isset($options['groups'])){
       $options['groups'] = array();
    }
    //var_dump($options['groups']);
	echo "<table class='zerobounce_advancedOptionsTable' id='zerobounce_advaced_settings_tag_table'>
        	<tbody>
        		<tr class='zerobounce_table_head'>
	        		<td>Tag</td>
	        		<td>Target url</td>
	        		<td>Delete</td>
	        	<tr>	
        	";


    if(ZeroBounceExtSites::isChange()){  
    	foreach($options['groupsTag'] as $group){
			$Tag = $group['tag'];
			$url = $group['url'];
			$md5 = md5($Tag);

			echo "<tr class='zerobounce_advanced_entries' id='$md5'>
	        	<td><input type='text' value='$Tag' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a group='groupsTag' entryId='$md5' href='#' action='delete'>Delete</a></td>
	        </tr>";
		}
    } else {
    	foreach($options['groups'] as $group){
			$Tag = $group['tag'];
			$url = $group['url'];
			$delete = zerobounce_GetCurrentUrl().'&zb-del-redirectbyTag='.md5($Tag);
			echo "<tr class='zerobounce_advanced_entries'>
	        	<td><input type='text' value='$Tag' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a href='$delete'>Delete</a></td>
	        </tr>";
		}

    }

	echo "</tbody>
        </table>";
}

function zerobounce_redirectByTag_validation($input){
	$options = null;
    $defaultOptions = get_option( 'zerobounce-redirectByTag-group' );
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = $defaultOptions;
    }

	
	if(!isset($_POST['addTag'])) return $options;

	$groups = null;
	if(isset($_POST['zb_extsite_change_id'])){
		if(isset($options['groupsTag'])){
			$groups = $options['groupsTag'];
		} else {
			$groups = array();
		}
	 	
	 } else {
	 	if(isset($options['groups'])){
			$groups = $options['groups'];
		} else {
			$groups = array();
		}
	 }



	$url = '';
	$Tag = '';

	if(isset($input['TagTargetUrl'])){
		$url = $input['TagTargetUrl'];
	} else {
		$url = $_POST['zerobounce-redirectByTag-group']['TagTargetUrl'];
	}

	if(isset($input['TagContains'])){
		$Tag = $input['TagContains'];
	} else {
		$Tag = $_POST['zerobounce-redirectByTag-group']['TagContains'];
	}

	if(!zerobounce_is_url($url)){
		add_settings_error( 'zerobounce-redirectByTag-group', 'error', 'Url is not valid!' );
	} else {
		$Tag = explode(",", $Tag);
		foreach ($Tag as $value) {
			$value = trim($value);
			if(strlen($value) == 0){
				continue;
			}
			$groups[md5($value)] = array('tag' =>  $value, 'url' => $url);
		}
		
	}


	if(isset($_POST['zb_extsite_change_id'])){
	 	$options['groupsTag'] = $groups;
	 } else {
	 	$options['groups'] = $groups;
	 }


	zerobounce_delete_js_files();

	if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        ZeroBounceDB_ExtSites::setSettings($id, $options);

        return $defaultOptions;
    } 

	return $options;
}



/* Functions redirect by Category */

function zerobounce_section_redirectByCategory_callback(){
    echo "<div style='float:left;width:50%'>Here you can set custom redirects for each Category.</div>";
   

}

function zerobounce_field_redirectByCategory_Add_callback(){
     	echo "<table class='zerobounce_advanced_add' >
            <tbody>
                <tr >
                    <td name='zerobounce_td_field'>Category</td>
                    <td name='zerobounce_td_explanation'>Target Url</td>
                 </tr>
                 <tr>
                    <td name='zerobounce_td_field'><input type='text' id='categories' placeholder='Category' name='zerobounce-redirectByCategory-group[CategoryContains]' /></td>
                    <td name='zerobounce_td_explanation'><input type='text' id='inputCategoryTargetUrl' placeholder='target url' name='zerobounce-redirectByCategory-group[CategoryTargetUrl]' /></td>
               </tr>
            </tbody>
        </table>";
        submit_button('Add', 'primary', 'addCategory');
}

function zerobounce_field_redirectByCategory_All_callback(){
	$options = null;
    
    if(ZeroBounceExtSites::isChange()){
        $id = ZeroBounceExtSites::getChangeNumber();
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = get_option( 'zerobounce-redirectByCategory-group' );
    }

	if(ZeroBounceExtSites::isChange() && !isset($options['groupsCategory'])){
		$options['groupsCategory'] = array();
	} else {
		if(!isset($options['groups'])){
       		$options['groups'] = array();
    	}
	}


	echo "<table class='zerobounce_advancedOptionsTable' id='zerobounce_advaced_settings_category_table'>
        	<tbody>
        		<tr class='zerobounce_table_head'>
	        		<td>Category</td>
	        		<td>Target url</td>
	        		<td>Delete</td>
	        	<tr>	
        	";


     if(ZeroBounceExtSites::isChange()){  
     	foreach($options['groupsCategory'] as $group){
			$Category = $group['Category'];
			$url = $group['url'];
			$md5 = md5($Category);

			echo "<tr class='zerobounce_advanced_entries' id='$md5'>
	        	<td><input type='text' value='$Category' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a group='groupsCategory' entryId='$md5' href='#' action='delete'>Delete</a></td>
	        </tr>";
		}
     } else {
     	foreach($options['groups'] as $group){
			$Category = $group['Category'];
			$url = $group['url'];
			$delete = zerobounce_GetCurrentUrl().'&zb-del-redirectbyCategory='.md5($Category);
			echo "<tr class='zerobounce_advanced_entries'>
	        	<td><input type='text' value='$Category' readonly></td>
	        	<td><input type='text' value='$url' readonly></td>
	        	<td><a href='$delete'>Delete</a></td>
	        </tr>";
		}
     }

	

	echo "</tbody>
        </table>";
}

function zerobounce_redirectByCategory_validation($input){
	$options = null;
    $defaultOptions = get_option( 'zerobounce-redirectByCategory-group' );
    if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        $options = ZeroBounceDB_ExtSites::getSettings($id);
    } else {
        $options = $defaultOptions;
    }
	
	
	if(!isset($_POST['addCategory'])) return $options;

	$groups = null;
	if(isset($_POST['zb_extsite_change_id'])){
		if(isset($options['groupsCategory'])){
			$groups = $options['groupsCategory'];
		} else {
			$groups = array();
		}
	 	
	 } else {
	 	if(isset($options['groups'])){
			$groups = $options['groups'];
		} else {
			$groups = array();
		}
	 }


	$url = '';
	$Category = '';

	if(isset($input['CategoryTargetUrl'])){
		$url = $input['CategoryTargetUrl'];
	} else {
		$url = $_POST['zerobounce-redirectByCategory-group']['CategoryTargetUrl'];
	}

	if(isset($input['CategoryContains'])){
		$Category = $input['CategoryContains'];
	} else {
		$Category = $_POST['zerobounce-redirectByCategory-group']['CategoryContains'];
	}


	if(!zerobounce_is_url($url)){
		add_settings_error( 'zerobounce-redirectByCategory-group', 'error', 'Url is not valid!' );
	} else {
		$cats = explode(",", $Category);
		foreach ($cats as $value) {
			$value = trim($value);
			if(strlen($value) == 0){
				continue;
			}
			$groups[md5($value)] = array('Category' =>  $value, 'url' => $url);
		}
		
	}

	if(isset($_POST['zb_extsite_change_id'])){
	 	$options['groupsCategory'] = $groups;
	 } else {
	 	$options['groups'] = $groups;
	 }


	zerobounce_delete_js_files();

	if(isset($_POST['zb_extsite_change_id'])){
        $id = $_POST['zb_extsite_change_id'];
        $id = intval($id);
        ZeroBounceDB_ExtSites::setSettings($id, $options);

        return $defaultOptions;
    } 

	return $options;
}






