// JS for posts and pages
function zerobounce_is_url(url) {
    var urlregex = /^(ht|f)tp(s?)\:\/\/(([a-zA-Z0-9\-\._]+(\.[a-zA-Z0-9\-\._]+)+)|localhost)(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?([\d\w\.\/\%\+\-\=\&amp;\?\:\\\&quot;\'\,\|\~\;]*)$/;
    url = url.trim();

    var valide = true;

    if (url.length == 0) {

    } else if (!url.startsWith('http://') && !url.startsWith('https://')) {
        valide = false;
    } else if (!urlregex.test(url)) {
        valide = false;
    }

    return valide;
}


jQuery(function($) {
    function zerobounce_checkMetaFields() {

    }

    $('#post').submit(function(event) {
        var valide = true;
        if ($('#zerobounce_force_enable').is(':checked')) {
            if ($('#zerobounce_individual_disable').is(':checked')) {
                alert("Zerobounce can not be forced enabled and disabled at the same time!");
                valide = false;
            }
            if ($('#zerobounce_specific_url').val().length == 0) {
                alert("Please type a 'Individual Url in the field!");
                valide = false;
            }
        }

        if ($('#zerobounce_individual_disable').is(':checked')) {
            if ($('#zerobounce_force_enable').is(':checked')) {
                alert("Zerobounce can not be forced enabled and disabled at the same time!");
                valide = false;
            }
        }

        if (!valide) {
            event.preventDefault();
        }
    });



    $('#zerobounce_individual_disable').change(function() {
        if ($(this).attr('checked')) {
            $('#zerobounce_force_enable').removeAttr('checked');
            $('#zerobounce_specific_url').attr('disabled', 'disabled');
            $('#zerobounce_specific_url').attr('value', '');
        } else {
            $('#zerobounce_specific_url').removeAttr('disabled');
        }
    });

    $('#zerobounce_force_enable').change(function() {
        if ($(this).attr('checked')) {
            $('#zerobounce_individual_disable').removeAttr('checked');
            $('#zerobounce_specific_url').removeAttr('disabled');
        } else {
            $('#zerobounce_specific_url').removeAttr('disabled');
        }
    });


    $('#zerobounce_specific_url').change(function() {
        var urlregex = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
        var url = $(this).val().trim();

        var button = document.getElementById('publish');
        var urlbox = document.getElementById('zerobounce_specific_url');
        var valide = zerobounce_is_url(url);

        if (!valide) {
            button.setAttribute('disabled', 'disabled');
            urlbox.setAttribute('style', 'border:1px solid; border-color:red;');
            alert('Url for zerobounce is invalid. Only full urls like "http://www.google.de/" are allowed!');
        } else {
            button.removeAttribute("disabled");
            urlbox.setAttribute('style', '');
        }
    });
});




//JS for setting page
function zerobounce_cleanTextarea(textarea) {
    var string = textarea.val().replace(/\r/g, '');
    var array = string.split("\n");

    string = '';
    for (var i = 0; i < array.length; i++) {
        if (array[i].length != 0) {
            string += array[i] + "\n";
        }
    }

    textarea.val(string);
}

jQuery(function($) {
    function zerobounce_setFieldActivationStatus($checkbox) {
        var inputId = $checkbox.attr('id');
        inputId = inputId.replace('IsActivated', '');
        var selektor = "td[name=zerobounce_td_settings] [id^=" + inputId + "]";

        if ($checkbox.attr('checked')) {
            $(selektor).removeAttr('disabled');
        } else {
            $(selektor).attr('disabled', 'disabled');
        }
    }

    function zerobounce_markTextField($field, $message, valide) {
        if (valide) {
            $('#submit').removeAttr('disabled');
            $field.attr('style', '');
        } else {
            $('#submit').attr('disabled', 'disabled');
            $field.attr('style', 'border:1px solid; border-color:red;');
            alert($message);
        }
    }



    $('#zerobounce_addMoreUrls').click(function() {
        $('#globalUrlsIsActivated').val('1');
        $('#globalUrlIsActivated').val('0');

        $('#zerobounce_tr_globalUrl').attr('style', 'display:none');
        $('#zerobounce_tr_globalUrls').attr('style', '');
    });

    $('#zerobounce_useOneUrl').click(function() {
        $('#globalUrlsIsActivated').val('0');
        $('#globalUrlIsActivated').val('1');

        $('#zerobounce_tr_globalUrl').attr('style', '');
        $('#zerobounce_tr_globalUrls').attr('style', 'display:none');
    });



    $('.zerobounce_settings_table input[id$=IsActivated]').change(function() {
        zerobounce_setFieldActivationStatus($(this));
    });



    $(document).ready(function() {
        $('#zerobounce_textarea_debug').select();
        $("#zerobounce_textarea_debug").focus(function() {
            var $this = $(this);
            $this.select();

            $this.mouseup(function() {
                $this.unbind("mouseup");
                return false;
            });
        });




        $('input[id$=IsActivated]').each(function() {
            zerobounce_setFieldActivationStatus($(this));
        });

		if($('#zerobounce_tr_globalUrls').length > 0){
			if ($('#globalUrlIsActivated').attr('checked')) {
            $('#zerobounce_tr_globalUrl').attr('style', '');
            $('#zerobounce_tr_globalUrls').attr('style', 'display:none');
        } else {
            $('#zerobounce_tr_globalUrl').attr('style', 'display:none');
            $('#zerobounce_tr_globalUrls').attr('style', '');
        }
		}
        
    });

    $('#zerobounce_globalUrl').change(function() {
        var url = $(this).val();
        var valide = zerobounce_is_url(url);
        var message = 'Url for zerobounce is invalid. Only full urls like "http://www.google.de/" are allowed!';
        zerobounce_markTextField($(this), message, valide);
    });

    $('#zerobounce_globalUrls').change(function() {
        zerobounce_cleanTextarea($(this));
        var text = $(this).val();
        var valide = true;
        var alertStr = 'Please correct the following urls:\n';

        if (text.length == 0) {
            valide = true;
        } else {
            var url_array = text.split("\n");
            for (var i = 0; i < url_array.length; i++) {
                var url = null;
                if(url_array[i].indexOf('\\')){
                    url = url_array[i].split('\\');
                    url = url[0];
                }

                if (!zerobounce_is_url(url)) {
                    alertStr += url + "\n";
                }
            }
        }

        if (alertStr.length > 36) {
            valide = false;
        }

        zerobounce_markTextField($(this), alertStr, valide);
    });

    $('#zerobounce_conditionRedirectMobileUrls').change(function() {
        zerobounce_cleanTextarea($(this));
        var text = $(this).val();
        var valide = true;
        var alertStr = 'Please correct the following urls:\n';

        if (text.length == 0) {
            valide = true;
        } else {
            var url_array = text.split("\n");
            for (var i = 0; i < url_array.length; i++) {
                var url = url_array[i];
                if (!zerobounce_is_url(url)) {
                    alertStr += url + "\n";
                }
            }
        }

        if (alertStr.length > 36) {
            valide = false;
        }

        zerobounce_markTextField($(this), alertStr, valide);
    });


    $('#zerobounce_conditionReferrerContains').change(function() {
        zerobounce_cleanTextarea($(this));
    });

    $('#zerobounce_conditionTimeout').change(function() {
        var regex = /^[0-9]*?$/;
        var valide = true;
        var message = 'Please only write numbers (0-9) in the field! ';
        if (!regex.test($(this).val())) {
            valide = false;
        }

        zerobounce_markTextField($(this), message, valide);
    });

    $('#zerobounce_conditionPercentOfVisitors').change(function() {
        var regex = /^[0-9]{0,3}$/;
        var val = $('#zerobounce_conditionPercentOfVisitors').val();
        var message = 'Please only write a number between 1 and 100 in the field!';
        var valide = true;
        if (!regex.test(val)) {
            valide = false;
        } else if (val.length > 0) {
            if (0 == val || 100 < val) {
                valide = false;
            }
        }

        zerobounce_markTextField($(this), message, valide);
    });
    
    $('#zerobounce_conditionRedirectVisitorOnlyOnce').change(function() {
        var regex = /^[0-9]{0,}$/;
        var val = $('#zerobounce_conditionRedirectVisitorOnlyOnce').val();
        var message = 'Please only write a number like "23" in the field!';
        var valide = true;
        if (val.length > 0 && !regex.test(val)) {
            valide = false;
        }

        zerobounce_markTextField($(this), message, valide);
    });


    //advanced options
    $('input[placeholder="target url"]').change(function() {
        var url = $(this).val();
        if (url.length == 0) {
            zerobounce_markTextField($(this), '', true);
        }

        if (!zerobounce_is_url(url)) {
            zerobounce_markTextField($(this), 'Url ist not valid!', false);
        } else {
            zerobounce_markTextField($(this), '', true);
        }
    });

    $('input[type=submit]').click(function() {
        $('input[type=submit]').attr("clicked", "false");
        $(this).attr("clicked", "true");
    });

        //tags autocomplete
            function split( val ) {
              return val.split( /,\s*/ );
            }
            function extractLast( term ) {
              return split( term ).pop();
            }

        $( "#tags" ).autocomplete({
            source:zb_js.availableTags
        });

        $( "#categories" ).autocomplete({
            source: zb_js.availableCategories
        });

      if(window.location.href.indexOf('page=zerobounce-external-sites') > -1){
        $("tr:contains('Activate:')").hide();
        $("tr:contains('Disable for')").hide();
        $("tr:contains('Load Javascript inside Html')").hide();
        

        $('a[removeid]').click(function(){
            $id = $(this).attr('removeid');
            $.get( zb_js.ajaxurl + "?action=zbjs&task=delExtId&delid="+$id , function() {
              
            })
              .done(function() {
                $('a[removeid='+$id+']').parents("tr").remove();
                
              })
              .fail(function() {
                alert( "An error appeared. Please open a suppor ticket!" );
              });


        });

        function zerobounce_extSite_action_delete($item){
            var entryId = $item.attr('entryid');
              var group = $item.attr('group');
              var siteId = $('input[name=zb_extsite_change_id]').val();

              var jqxhr = $.get( zb_js.ajaxurl + "?action=zbjs&task=extSiteDelAdvancedSetting&siteId="+siteId+"&group="+group+"&entryid="+entryId, function() {
    
                })
                  .done(function() {
                    $('#'+entryId).remove();
                  })
                  .fail(function() {
                    alert( "An error appeared. Please open a suppor ticket!" );
                  });
        }

        //ext Sites change site
        if($('input[name=zb_extsite_change_id]').length > 0){
            $( "a[action=delete]" ).click(function( event ) {
              event.preventDefault();
              zerobounce_extSite_action_delete($(this));
            });
        }
      }

      //ext sites - change specific site
      if($('input[name=zb_extsite_change_id]').length > 0){
        $('input[value=Add]').click(function(event){
            event.preventDefault();
            var valid = true;
            var siteId = $('input[name=zb_extsite_change_id]').val();
            var action = $(this).attr("name");
            var tableId = null;
            var group = null;
            var conditionName = null;
            var conditionValue = null;
            var conditionShow = null;
            var targetName = null;
            var targetValue = null;
            var emptyConditionMessage = null;
            var emptyTargetUrlMessage = "target url is empty!";

            var phConditionValue = '##CONDITION_VALUE##';
            var phTargetValue = '##TARGET_VALUE##';
            var phDeleteUrl = '##DELETE_URL##';
            var phMd5 = '##md5##';
            var phGroup = '##group##';

            var addTemplate =  "<tr class='zerobounce_advanced_entries'><td><input type='text' value='##CONDITION_VALUE##' readonly></td><td><input type='text' value='##TARGET_VALUE##' readonly></td><td><a action='delete' entryid='##md5##' groupsTag='##group##'>Delete</a></td></tr>";

            switch(action){
                case 'addReferer':
                    tableId = '#zerobounce_advaced_settings_referer_table';
                    group = 'groupsReferer';
                    conditionName = 'zerobounce-redirectByReferer-group[refererContains]';
                    conditionValue = $('#referer').val();
                    targetName = 'zerobounce-redirectByReferer-group[refererTargetUrl]';
                    targetValue = $('#inputRefererTargetUrl').val();
                    emptyConditionMessage = 'Referer is empty!';
                    break;
                case 'addCountry':
                    tableId = '#zerobounce_advaced_settings_country_table';
                    group = 'groupsCountry'
                    conditionName = 'zerobounce-redirectByCountry-group[country]';
                    conditionValue = $('#countries').val();
                    conditionShow = $('#countries option[value=' + conditionValue + ']').html();
                    
                    targetName = 'zerobounce-redirectByCountry-group[CountryTargetUrl]';
                    targetValue = $('#inputCountryTargetUrl').val();

                    emptyConditionMessage = "None country choosen!";
                    break;
                case 'addTag':
                    tableId = '#zerobounce_advaced_settings_tag_table';
                    group = 'groupsTag';
                    conditionName = 'zerobounce-redirectByTag-group[TagContains]';
                    conditionValue = $('#tags').val();
                    targetName = 'zerobounce-redirectByTag-group[TagTargetUrl]';
                    targetValue = $('#inputTagTargetUrl').val();

                    emptyConditionMessage = "Tags is empty";
                    break;
                case 'addCategory':
                    tableId = '#zerobounce_advaced_settings_category_table';
                    group = 'groupsCategory';
                    conditionName = 'zerobounce-redirectByCategory-group[CategoryContains]';
                    conditionValue = $('#categories').val();
                    targetName = 'zerobounce-redirectByCategory-group[CategoryTargetUrl]';
                    targetValue = $('#inputCategoryTargetUrl').val();

                    emptyConditionMessage = "Category is empty";
                    break;
            }

            if(conditionValue.length == 0 ){
                alert(emptyConditionMessage);
                return;
            } else if(targetValue.length == 0){
                alert(emptyTargetUrlMessage);
            }


            $(tableId + ' td:first-child input').each(function(){
                if(!valid){
                    return
                }
                var value = $(this).val();
                
                if(conditionValue == value){
                    valid = false;
                    alert("Entry already exist!");
                    console.log(value);
                }

            });

            if(!valid){
                return;
            }

            var str = 'zb_extsite_change_id=' + siteId + '&' + action +'=' + '&' + conditionName + '=' + conditionValue + '&' + targetName + '=' + targetValue;

            var request = $.ajax({
               type: 'POST',    
                url:zb_js.ajaxurl + "?action=zbjs&task=extSiteChangeAddAdvancedSettings",
                data:str,
                success: function(msg){
                    var md5 = CryptoJS.MD5(conditionValue);
                    var url = zb_js.ajaxurl + "?action=zbjs&task=extSiteChangeAddAdvancedSettings&md5=" + md5;

                    if(conditionShow == null){
                        addTemplate = addTemplate.replace(phConditionValue, conditionValue);
                    } else {
                        addTemplate = addTemplate.replace(phConditionValue, conditionShow);
                    }
                    
                    addTemplate = addTemplate.replace(phTargetValue, targetValue);
                    addTemplate = addTemplate.replace(phDeleteUrl, url);
                    addTemplate = addTemplate.replace(phGroup, group);
                    addTemplate = addTemplate.replace(phMd5, md5);
                    $(tableId + ' tr:last').after(addTemplate);
                    $( "a[action=delete]" ).click(function( event ) {
                      event.preventDefault();
                      zerobounce_extSite_action_delete($(this));
                    });
                }
            });

            request.fail(function(msg){
                alert("An error appeared. Please open a suppor ticket!" );
            });
        });
      }



    if ($('#addReferer').length > 0) {
        $('form').submit(function(event) {
            var valide = true;
            var button = $("input[clicked=true]").attr("id");
            if (button == 'addReferer') {
                var val = $('input[placeholder=referer]').val();
                if (val.length == 0) {
                    valide = false;
                    alert('The field "referer" can not be empty!');
                } else {
                    var url = $('#inputRefererTargetUrl').val();
                    if (url.length == 0) {
                        alert('Please input a valid url!');
                        valide = false;
                    } else if (!zerobounce_is_url(url)) {
                        alert('Please input a valid url!');
                        valide = false;
                    }
                }

            } else if (button == 'addCountry') {
                var url = $('#inputCountryTargetUrl').val();
                if (url.length == 0) {
                    alert('Please input a valid url!');
                    valide = false;
                } else if (!zerobounce_is_url(url)) {
                    alert('Please input a valid url!');
                    valide = false;
                }
            }

            if (!valide) {
                event.preventDefault();
            }
        });
    }


    function zerobounce_extSites_checkFields(){
        var valide = true;
        var url = $('#zb_extsite_add').val();
            if(url.length == 0){
                $.jGrowl("Please enter an url!", { life: 5000 });
                showError = true;
                valide = false;
            }

            if(!zerobounce_is_url(url)){
                $.jGrowl("Url Format is wrong!", { life: 5000 });
                showError = true;
                valide = false;
            } 

            $('td[contains=url]').each(function(){
                var extUrl= $(this).html();
                if(url == extUrl){
                    $.jGrowl("Url already exist!", { life: 5000 });
                    showError = true;
                    valide = false;

                }
            });

            return valide;
    }

    if($('#ext_site_add_url').length > 0){

        $('#zb_extsite_add').change(function(){
            var valide = true;
            var val = $(this).val();
            if(val.length == 0){
                $(this).attr("style", "")
                return;
            }

            valide= zerobounce_extSites_checkFields();
            
            if(!valide){
                $(this).attr("style", "border-color:red; border-style:solid;");
            } else {
                 $(this).attr("style", "");
            }
        }); 

        $('#ext_site_add_url').submit(function(){
            if(zb_js.isSimpleLicense == '1'){

                var amountItems = $('#ext_sites_table tr').length;

                if(amountItems == 11){
                    alert('Please upgrade your license for more then ten external sites.');
                    return false;
                }
            }
            
            return zerobounce_extSites_checkFields();
        });

        $('a[action=showJs]').click(function(){
            var md5 = $(this).attr("md5");
            var url = $(this).attr("for");
            var siteid = $(this).attr("siteid");

            var template = "&lt;script type='text/javascript'&gt;\n"+
                "(function() {\n"+
                    "\t var s = document.createElement('script');\n" +
                    "\t s.type = 'text/javascript';\n" +
                    "\t s.async = true; \n" +
                    "\t s.src ='" + zb_js.ajaxurl + "'+ '?action=zbjs&esid=" + siteid + "&task=getjs&md5=" + md5 + "';\n" +
                    "\t document.body.appendChild(s);\n" +
                "})();\n" +
                "&lt;/script&gt;";

            $('#ext_sites_jsfor').html(url);
            $('#ext_sites_js').html(template);
            $('#ext_sites_showjs').show();
        });
        
    }


});