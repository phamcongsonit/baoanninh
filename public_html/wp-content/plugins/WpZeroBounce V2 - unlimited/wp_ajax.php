<?php

class ZeroBounce_ajax{
	public static function init(){
		add_action( 'wp_ajax_zbjs', array( 'ZeroBounce_ajax', 'zbjs' )   );
		add_action( 'wp_ajax_nopriv_zbjs', array( 'ZeroBounce_ajax', 'zbjs' )  );
	}

	public static function zbjs() {
		$reponse = "";

		$task = "";
		if(isset($_POST['task'])){
			$task = $_POST['task'];
		} else if(isset($_GET['task'])){
			$task = $_GET['task'];
		}
	
		switch($task){
			case 'getjs':
				$hash = $_GET['md5'];

				if(strlen($hash) === 32 && ctype_xdigit($hash)){
					if(isset($_GET['esid'])){
						$isActive = ZeroBounceDB_ExtSites::isActive($_GET['esid']);
						if($isActive == null || $isActive == false || $isActive == '0'){
							echo " ";
							wp_die();
						}
					}


					$js = ZeroBounceDB_Js::get($hash);
					if(!$js){ //JS does not exist
						if(isset($_GET['esid'])){

							zerobounce_createJS_File($_GET['esid']);
							$js = ZeroBounceDB_Js::get($hash);
						}
					}

					if(-1 < strpos($js, 'redByCountry')){
						$path = plugin_dir_path( __FILE__ )."iptocountry/geoip.class_helper.php";
						include($path);
						$ip = '';
						if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
						    $ip = $_SERVER['HTTP_CLIENT_IP'];
						} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
						    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
						} else {
						    $ip = $_SERVER['REMOTE_ADDR'];
						}

						$data = GeoIP::LookupIP($ip);
						if(isset($data['cc'])){
							$js= "var country_backup = '".$data['cc']."';

								$js";
							
						}
					}

					header('Content-Type: application/javascript');
					header('Access-Control-Allow-Origin: *');
					echo $js;
					wp_die();
				}
				break;
			case 'getloc':
				$path = plugin_dir_path( __FILE__ )."iptocountry/geoip.class_helper.php";
				include($path);
				$ip = '';
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				    $ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
				    $ip = $_SERVER['REMOTE_ADDR'];
				}

				$data = GeoIP::LookupIP($ip);
				if(isset($data['cc'])){
					echo $data['cc'];
					wp_die();
				} else {
					echo "NULL";
					wp_die();
				}
				break;
			case 'delExtId':
				$id = intval($_GET['delid']);
				ZeroBounceDB_ExtSites::remove($id);
				ZeroBounceDB_Js::deleteAll();
				break;
			case 'extSiteDelAdvancedSetting':
				$siteId = intval($_GET['siteId']);
				$group = $_GET['group'];
				$entryId = $_GET['entryid'];
				$settings = ZeroBounceDB_ExtSites::getSettings($siteId);

				if($group == 'groupsCountry'){

					$entryId = zerobounce_convertCountryToIso($entryId);
					var_dump($entryId);

				}

				if(isset($settings[$group][$entryId])){
					unset($settings[$group][$entryId]);
				}
				
				ZeroBounceDB_ExtSites::setSettings($siteId,  $settings);
				ZeroBounceDB_Js::deleteAll();

				break;
			case 'extSiteChangeAddAdvancedSettings': 
				$input = array();
				if(isset($_POST['addReferer'])){
					zerobounce_redirectByReferer_validation($input);
				} else if(isset($_POST['addCountry'])){
					zerobounce_redirectByCountry_validation($input);
				} else if(isset($_POST['addTag'])){
					zerobounce_redirectByTag_validation($input);
				} else if(isset($_POST['addCategory'])){
					zerobounce_redirectByCategory_validation($input);
				}
				ZeroBounceDB_Js::deleteAll();


				break;
		}
		
					


	    echo json_encode($reponse);
		flush();
	    wp_die();
	}


}

ZeroBounce_ajax::init();