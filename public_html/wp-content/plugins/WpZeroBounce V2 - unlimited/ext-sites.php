<?php


if(!class_exists('ZeroBounceExtSites')){
	class ZeroBounceExtSites{
		private static $changeId = null;

		public static function init(){
		
		}

		public static function isChange(){
			if(self::$changeId == null){
				return false;
			} else {
				return true;
			}
		}

		public static function getChangeNumber(){
			return self::$changeId;
		}

		public static function setChangeNumber($id){
			self::$changeId = $id;
		}

		public static function echo_site(){
			//toggle activate/deactivate
			if(isset($_POST['zb_extsite_status'])){
				$id = $_POST['zb_extsite_status'];
				ZeroBounceDB_ExtSites::toggleStatus($id);
			}

			//remove
			if(isset($_POST['zb_extsite_remove'])){
				$id = $_POST['zb_extsite_remove'];
				$md5 = ZeroBounceDB_ExtSites::getMD5($id);
				ZeroBounceDB_Js::deleteMd5($md5);
				ZeroBounceDB_ExtSites::remove($id);
			}

			//change
			if(isset($_POST['zb_extsite_change'])){
				self::setChangeNumber(intval($_POST['zb_extsite_change']));

			} else if(isset($_GET['zb_extsite_change'])){
				self::setChangeNumber(intval($_GET['zb_extsite_change']));
			}


			//add
			if(isset($_POST['zb_extsite_add_url']) || isset($_GET['zb_extsite_add_url'])){
				$url = isset($_POST['zb_extsite_add_url']) ? $_POST['zb_extsite_add_url'] : $_GET['zb_extsite_add_url'];

				$id = false;
				if(isset($_GET['zb_extSite_is_post'])){
					if(!ZeroBounceDB_ExtSites::urlExist($url)){
						$id = ZeroBounceDB_ExtSites::addEmpty($url);
						if($id != false){
							self::setChangeNumber($id);
						}
					} else{
						$id = ZeroBounceDB_ExtSites::getId($url);

						self::setChangeNumber($id); //prevent user mistakes
					}

					
				} else if(!filter_var($url, FILTER_VALIDATE_URL) === false) {
					if(!ZeroBounceDB_ExtSites::urlExist($url)){
						$id = ZeroBounceDB_ExtSites::addEmpty($url);
						if($id != false){
							self::setChangeNumber($id);
						}
					}
				}
			}

			//var_dump(self::$changeId);
			//var_dump(self::isChange());
			if(self::isChange()){
				ZeroBounceExtSites::echoSettingsChange();
			} else {
				ZeroBounceExtSites::echo_table();
			}
		}


		public static function echo_table(){
			?>
					<div class="wrap">
				   <h2>Zero Bounce - External Sites</h2>
				   <h3>Explanation</h3>
				   <div style="float:left;width:50%">Here you have the possbility to use Zero Bounce for external sites which maybe aren't a Wordpress site.</div>
				   <div style="float:right"><a target="_blank" href="http://forcespark.net/documentation/" class="zerobounce_documentation_button">Documentation</a></div>
				   <div style="clear:both"></div>
				   <div>
				   		<form method="POST" action="" id="ext_site_add_url">
					   		<label for="zb_extsite_add" style="margin-right:50px;">Add your site url</label><input id="zb_extsite_add" name="zb_extsite_add_url" placeholder="http://www.yourwebsite.com" style="width:400px;">
							<?php
					        
				            submit_button('Submit', 'primary', 'zb_extsite_add');
				            ?>
		       		 </form>

				   </div>
				   <h3>Manage sites</h3>
				   <table id="ext_sites_table">
				      <tr>
				         <th>Url</th>
				         <th>Status</th>
				         <th>Configure</th>
				         <th>Delete</th>
				         <th>Show Code</th>
				      </tr>
				      <?php
				      $results = ZeroBounceDB_ExtSites::getAll();
				      foreach ($results as $result) {
				      	$url = $result->url;
				      	$id = $result->id;
				      	$md5 = $result->md5;
						$activate = $result->is_active;
						
						if(!is_int($activate)){
							$activate = intval($activate);
						}
					
						if($activate == 1){
							$activate = "<form method='POST' action='' id='toggleid$id'><input type='hidden' name='zb_extsite_status' value='$id'><a href='javascript:;' changeid='$id' onclick='document.getElementById(\"toggleid$id\").submit()'>Deactivate</a></form>";
						} else {
							$activate = "<form method='POST' action='' id='toggleid$id'><input type='hidden' name='zb_extsite_status' value='$id'><a href='javascript:;' changeid='$id' onclick='document.getElementById(\"toggleid$id\").submit()'>Activate</a></form>";

						}

				      	$change = "<form method='POST' action='' id='changeid$id'><input type='hidden' name='zb_extsite_change' value='$id'><a href='javascript:;' changeid='$id' onclick='document.getElementById(\"changeid$id\").submit()'>Configure</a></form>";
				      	$delete = "<form method='POST' action='' id='deleteid$id'><input type='hidden' name='zb_extsite_remove' value='$id'><a href='javascript:;' changeid='$id' onclick='
				      	var result =confirm(\"Do you really want to delete this site?\");
				      	if(result){
				      		document.getElementById(\"deleteid$id\").submit();
				      	}'>Delete</a></form>";
				      	$jscode = "<a href='#ext_sites_showjs' md5='$md5' action='showJs' for='$url' siteid='$id'>Show code</a>";


				      	if(!zerobounce_string_startsWith($url, 'http')){
				      		$jscode = 'Disabled';
				      		$post_id = intval($url);
				      		$title = get_the_title($post_id);
				      		$url = 'Post/Page: '.htmlspecialchars($title);
				      		$url = "<a href='".get_edit_post_link($post_id)."' target='_blank'>".$url."</a>";
				      	} else {
				      		$url = "<a href='$url' target='_blank'>$url</a>";
				      	}

				      	echo "<tr>
					      		<td contains='url'>$url</td>
					      		<td>$activate</td>
					      		<td>$change</td>
					      		<td>$delete</td>
					      		<td>$jscode</td>
					      	</tr>";


				      }

				      
				      ?>
				   </table>
				   <div id="ext_sites_showjs" style="display:none">
				   		<h4 >JS Code for: <span id="ext_sites_jsfor"></span></h4>
				   		Explanation: Search in your html code for "&lt;/body&gt;" and place the following Javascript Code before this tag.<br><br>
				   		<textarea readonly="readonly" id="ext_sites_js" onclick="this.select()" style="width:400px;height:200px;">

				   		</textarea>
				   </div>
				</div>
		<?php
				


		}



		public static function echoSettingsChange(){
			?>
			<div class="wrap">
				<form method="POST" action="options.php">
					<?php
			        settings_fields( 'zerobounce-settings-group' ); 
		            do_settings_sections( 'zerobounce-plugin' );
		            settings_fields( 'zerobounce-settings-group' ); 
		            do_settings_sections( 'zerobounce-plugin-advanced' );
		            submit_button('Save', 'primary', 'SavePluginOptions');
		            ?>
		            <input type="hidden" name="zb_extsite_change_id" value="<?php echo self::getChangeNumber();?>">
		        </form>

	        </div><?php
		}

		public static function section_globalUrl_callback(){
			  echo "<div style='float:left;width:50%'>Change the global url</div>";
			  echo "<div style='float:right'><a class='zerobounce_documentation_button' href='http://forcespark.net/documentation/' target='_blank'>Documentation</a></div>";
			   

		}


	}
}