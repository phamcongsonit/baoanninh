<?php
/* Contains all function to generate and enqueue the JS Code for zerobounce  
    Hooks into enqueue scripts -> checks if script exist, create it if necessay and enqueue it
*/


//removes unnecesary js files
function zerobounce_delete_js_files(){
    if(!class_exists("ZeroBounceDB_Js")){
        $parentDir = dirname(__FILE__);

        if(substr($parentDir, -strlen("/")) === "/"){
            $parentDir = substr($parentDir, 0, -1);
        } 
        
        include($parentDir.'/db_js.php');
    }
   ZeroBounceDB_Js::deleteAll();
}




/* 
    Create and attach JS File 
*/

function zerobounce_attach_js() {
    $status     = get_option( 'ZEROBOUNCE_license_status' );

    if($status != 'valid') return;

    $post_id = get_the_ID();
    
    //disabled for homepage/post/pages
    $options = get_option( 'zerobounce-main-settings-group' );
    if(is_front_page() && isset($options['DeactivateForHomepage']) && $options['DeactivateForHomepage'] == '1'){
        return;
    } else if(is_single() && isset($options['DeactivateForPosts']) && $options['DeactivateForPosts'] == '1'){
        return;
    } else if(is_page() && isset($options['DeactivateForPages']) && $options['DeactivateForPages'] == '1'){
        return;
    }

    if(!isset($options['IsActivated'])){
        $options['IsActivated'] = null;
    }

    
    $settings = array(
            'isActivated' => $options['IsActivated'],
            'isIndividualDisabled' => get_post_meta( $post_id, '_zerobounce_individual_disable', true ),
            'isForceEnabled' => get_post_meta( $post_id, '_zerobounce_force_enable', true ),

        );

    if($settings['isForceEnabled'] == '1'){

    } else if($settings['isIndividualDisabled'] == '1' || $settings['isActivated'] == '0'){
        return;
    }


    //create JS code
    $md5_of_settings = zerobounce_createJS_File();     
    if($md5_of_settings == false){
        return;
    }
   
        
    if($options['PutJSInsideHtml'] == 1){
       zerobounce_addJsToFooter();
    } else {     
        $url = plugins_url( 'res/js/zb.js', __FILE__ );

        wp_register_script('zerobounce.js', $url , array(),'1.0', true);
        wp_enqueue_script( 'zerobounce.js');
 

        wp_localize_script( 'zerobounce.js', 'zbjs', 
                zerobounce_getJS_cdata($md5_of_settings)
        );
    }
}

$options = get_option( 'zerobounce-main-settings-group' );
if(isset($options['PutJSInsideHtml']) && $options['PutJSInsideHtml'] == 1){
 
    switch ($options['PutJSInsideHtmlOption']){
        case 'get_footer' : add_action('get_footer', 'zerobounce_attach_js');
            break;
        case 'wp_footer' : add_action('wp_footer', 'zerobounce_attach_js');
            break;
        case 'wp_head' : 
          add_action('wp_head', 'zerobounce_attach_js');
            break;
    }
    
} else {
    add_action( 'wp_enqueue_scripts', 'zerobounce_attach_js' ); 
}


function zerobounce_getJS_cdata($md5_of_settings){
        $post_id = get_the_ID();

        $postCategories = array();
        $categories = get_the_category($post_id);
        foreach ( $categories as $category ) { 
            $postCategories[] = $category->name ; 
        }

        $tmp_post_tags = wp_get_post_tags($post_id);
        $post_tags = array();
        foreach ($tmp_post_tags as $value) {
            $post_tags[] = $value->name;
        }

        $ajax_url = admin_url( 'admin-ajax.php' );
        if(zerobounce_string_endsWith($ajax_url, '/')){
          $ajax_url = substr($ajax_url , 0, -1);
        }


        $array = array(
                'ajaxurl' => $ajax_url,
                'md5' => $md5_of_settings,
                'tags' => $post_tags ,
                'categories' => $postCategories
                );
        return $array;
}




function zerobounce_addJsToFooter(){
    $md5_of_settings = zerobounce_createJS_File();
    $js = ZeroBounceDB_Js::get($md5_of_settings);
    $jsFilename = get_option("ZEROBOUNCE_JS_CachePath").$md5_of_settings.'.js';
    $array = json_encode(zerobounce_getJS_cdata($md5_of_settings));

    $cdata = '/* <![CDATA[ */
                var zbjs = '.$array.';
            /* ]]> */';


    echo $cdata;
    echo '<script type="text/javascript">';
    echo $js;
    echo '</script>';
}




//build the js_settings Array. The array is used to create the js code
function zerobounce_get_js_settingsArray($extSiteId = null){
    if($extSiteId != null && !is_int($extSiteId)){
        $extSiteId = intval($extSiteId);
    }

    $post_id = get_the_ID();
    $main_options = null;
    $condition_options = null;
    $advanced_options = null;

    if(ZeroBounceDB_ExtSites::urlExist(strval($post_id))){ //for custom post settings
        $id = ZeroBounceDB_ExtSites::getId(strval($post_id));
        $main_options = ZeroBounceDB_ExtSites::getSettings($id);
        $condition_options = $main_options;
        $advanced_options = $main_options;
    } else if($extSiteId != null){ //external sites
        $main_options = ZeroBounceDB_ExtSites::getSettings($extSiteId);
        if(!$main_options){
            return false;
        }

        $condition_options = $main_options;
        $advanced_options = $main_options;
    }  else {
        $main_options = get_option( 'zerobounce-main-settings-group' );
        $condition_options = get_option( 'zerobounce-condition-settings-group' );
    }

 
    

    $js_settings = array(
        'individual_url' => false,
        'global_url' => false,
        'global_urls' => false,
        'conditionReferrerContainsGoogle' => false,
        'conditionReferrerContainsFacebook' => false,
        'conditionReferrerContainsBing' => false,
        'conditionReferrerContains' => false,
        'conditionDisableIfRefererContains' => false,
        'conditionTimeout' => false,
        'conditionEnableOnlyIfUrlContains' => false,
        'conditionDisableIfUrlContains' => false,
        'conditionVisitedSeveralPages' => false,
        'conditionByDevice' => false,
        'conditionRedirectMobileUrls' => false, 
        'conditionPercentOfVisitors' => false,
        'conditionVisitorCloseIntention' => false,
        'conditionRedirectVisitorOnlyOnce' => false,
        'advancedSettingsRedirectByReferer' => false,
        'advancedSettingsRedirectByCountry' => false,
        'advancedSettingsRedirectByTag' => false,
        'advancedSettingsRedirectByCategory' => false
        );
    
   
   
   

    /* 
        Main Options
    */

    //get individual url
    $js_settings['individual_url'] = get_post_meta($post_id, '_zerobounce_specific_url', true );
    
    //get global url/s, if none specific url is set
    if(strlen($js_settings['individual_url']) == 0){
        $js_settings['individual_url'] = false;


        if(!isset($main_options['globalUrlIsActivated'])){
            $main_options['globalUrlIsActivated'] = 0;
        }
        if(!isset($main_options['globalUrlsIsActivated'])){
            $main_options['globalUrlsIsActivated'] = 0;
        }


        if($main_options['globalUrlIsActivated'] == '1'){
            $js_settings['global_url'] = $main_options['globalUrl'];
        } else if($main_options['globalUrlsIsActivated'] == '1'){

            $urls = explode('<br/>', $main_options['globalUrls']);

            $js_settings['global_urls'] = $urls;
        }
    }

    if($js_settings['individual_url'] == false && $js_settings['global_url'] == false && $js_settings['global_urls'] == false){
        return false;
    }

    /*
        condition settings
    */

    if(isset($condition_options['conditionReferrerContainsIsActivated']) && $condition_options['conditionReferrerContainsIsActivated'] == '1'){
        $js_settings['conditionReferrerContainsGoogle'] = ($condition_options['conditionReferrerContainsGoogle'] == 1) ? true : false;
        $js_settings['conditionReferrerContainsFacebook'] = ($condition_options['conditionReferrerContainsFacebook'] == 1) ? true : false;
        $js_settings['conditionReferrerContainsBing'] = ($condition_options['conditionReferrerContainsBing'] == 1) ? true : false;
        $js_settings['conditionReferrerContains'] = $condition_options['conditionReferrerContains'];
    }

    if(isset($condition_options['conditionDisableIfRefererContainsIsActivated']) && $condition_options['conditionDisableIfRefererContainsIsActivated'] == '1'){
        $js_settings['conditionDisableIfRefererContains'] = $condition_options['conditionDisableIfRefererContains'];
    }
    

    


    if(isset($condition_options['conditionTimeoutIsActivated'] ) && $condition_options['conditionTimeoutIsActivated'] == '1'){
        $js_settings['conditionTimeout'] = $condition_options['conditionTimeout'];
    }
    if(isset($condition_options['conditionEnableOnlyIfUrlContainsIsActivated']) && $condition_options['conditionEnableOnlyIfUrlContainsIsActivated'] == '1'){
        $js_settings['conditionEnableOnlyIfUrlContains'] = $condition_options['conditionEnableOnlyIfUrlContains'];

    }
    
    if(isset($condition_options['conditionDisableIfUrlContainsIsActivated']) && $condition_options['conditionDisableIfUrlContainsIsActivated'] == '1'){
        var_dump($condition_options['conditionDisableIfUrlContainsIsActivated']);
        $js_settings['conditionDisableIfUrlContains'] = $condition_options['conditionDisableIfUrlContains'];
    }
    
    
    if(isset($condition_options['conditionVisitedSeveralPagesIsActivated']) && $condition_options['conditionVisitedSeveralPagesIsActivated'] == '1'){
        $js_settings['conditionVisitedSeveralPages'] = $condition_options['conditionVisitedSeveralPages'];
    }  
    if(isset($condition_options['conditionByDeviceIsActivated']) && $condition_options['conditionByDeviceIsActivated'] == '1'){
        if($condition_options['conditionByDevice'] == 'Mobil'){
            $js_settings['conditionByDevice'] = 'Mobil';
        } else if($condition_options['conditionByDevice'] == 'Desktop'){
            $js_settings['conditionByDevice'] = 'Desktop';
        }
    }  

    if(isset($condition_options['conditionRedirectMobileUrlsIsActivated']) && $condition_options['conditionRedirectMobileUrlsIsActivated'] == '1'){
        $js_settings['conditionRedirectMobileUrls'] = $condition_options['conditionRedirectMobileUrls'];
    }
    if($condition_options['conditionPercentOfVisitorsIsActivated'] == '1'){
        $js_settings['conditionPercentOfVisitors'] = $condition_options['conditionPercentOfVisitors'];
    }
    if(isset($condition_options['conditionVisitorCloseIntentionIsActivated']) && $condition_options['conditionVisitorCloseIntentionIsActivated'] == '1'){
        $js_settings['conditionVisitorCloseIntention'] = true;
    }
    if(isset($condition_options['conditionRedirectVisitorOnlyOnceIsActivated']) && $condition_options['conditionRedirectVisitorOnlyOnceIsActivated'] == '1'){
        $js_settings['conditionRedirectVisitorOnlyOnce'] = $condition_options['conditionRedirectVisitorOnlyOnce'];
    }


    /* 
        Advanced Settings 
    */

    //redirect by referer
    if(isset($advanced_options['groupsReferer'])){
        if(count($advanced_options['groupsReferer']) >0){
            $js_settings['advancedSettingsRedirectByReferer'] = $advanced_options['groupsReferer'];
        }
    } else {
        $options = get_option( 'zerobounce-redirectByReferer-group' );
         if(!isset($options['groups'])){
            $options['groups'] = array();
        }
        $groups = $options['groups'];
        if(count($groups) >0){
            $js_settings['advancedSettingsRedirectByReferer'] = $groups;
        }
    }
        
    //redirect by country
    if(isset($advanced_options['groupsCountry'])){
        if(count($advanced_options['groupsCountry']) >0){
            $js_settings['advancedSettingsRedirectByCountry'] = $advanced_options['groupsCountry'];
        }
    } else {
        $options = get_option( 'zerobounce-redirectByCountry-group' );
         if(!isset($options['groups'])){
            $options['groups'] = array();
        }
        $groups = $options['groups'];
        if(count($groups) >0){
            $js_settings['advancedSettingsRedirectByCountry'] = $groups;
        }
    }

    //redirect by tag
    if(isset($advanced_options['groupsTag'])){
        if(count($advanced_options['groupsTag']) >0){
            $js_settings['advancedSettingsRedirectByTag'] = $advanced_options['groupsTag'];
        }
    } else {
        $options = get_option( 'zerobounce-redirectByTag-group' );
         if(!isset($options['groups'])){
            $options['groups'] = array();
        }
        $groups = $options['groups'];
        if(count($groups) >0){
            $js_settings['advancedSettingsRedirectByTag'] = $groups;
        }
    }

     //redirect by category

    if(isset($advanced_options['groupsCategory'])){
        if(count($advanced_options['groupsCategory']) >0){
            $js_settings['advancedSettingsRedirectByCategory'] = $advanced_options['groupsCategory'];
        }
    } else {
        $options = get_option( 'zerobounce-redirectByCategory-group' );
         if(!isset($options['groups'])){
            $options['groups'] = array();
        }
        $groups = $options['groups'];
        if(count($groups) >0){
            $js_settings['advancedSettingsRedirectByCategory'] = $groups;
        }
    }



     
    return $js_settings;
}



function zerobounce_getJSCode($js_settings = null){
    if($js_settings == null){
         $js_settings = zerobounce_get_js_settingsArray();
    }


    $function_getUrl = "";
    $function_redirectIsActivated = '';

     
     /*
        conditions
    */
     $js_var_redirectIsActivate = 'var zerobounce_redirectIsActivated = true';
     $conditionTimeout = '';
     $conditionReferrerContains = '';
     $conditionDisableIfRefererContains = '';
     $conditionVisitedSeveralPages = '';
     $conditionByDevice = '';
     $conditionRedirectMobileUrls = '';
     $conditionPercentOfVisitors = '';
     $conditionVisitorCloseIntention = '';
     $conditionRedirectVisitorOnlyOnce = '';
     $conditionRedirectVisitorOnlyOnceSetCookie = '';
     $conditionVisitorFromExtern = '';

     

     if($js_settings['conditionTimeout'] != false){ 
        $timeout = $js_settings['conditionTimeout'];
        $conditionTimeout =" 
            setTimeout(function(){zerobounce_redirectIsActivated = false;}, ($timeout * 1000));
        ";
     }



     if($js_settings['conditionReferrerContains'] != false || $js_settings['conditionReferrerContainsGoogle'] || $js_settings['conditionReferrerContainsFacebook'] || $js_settings['conditionReferrerContainsBing']){
        $strings = array();
        if(strlen($js_settings['conditionReferrerContains']) != 0){
            $array = explode('<br/>', $js_settings['conditionReferrerContains']);
            $strings = array_merge($strings, $array);
        }
        
        if($js_settings['conditionReferrerContainsGoogle']){
            $strings[] = 'google';
        }
        if($js_settings['conditionReferrerContainsFacebook']){
            $strings[] = 'facebook';
        }
        if($js_settings['conditionReferrerContainsBing']){
            $strings[] = 'bing';
        }

        $strings = json_encode($strings);

        $conditionReferrerContains = "
            //condition referrer contains
            if(zerobounce_redirectIsActivated){
                var referrers = $strings;
                var match = false;

                if(document.referrer !== undefined && document.referrer.length > 0){
                    for(var i = 0; i < referrers.length; ++i){
                        if(document.referrer.indexOf(referrers[i]) > -1){
                            match = true;
                            break;
                        }   
                    } 
                }
                
                if(!match){
                    zerobounce_redirectIsActivated = false;
                }
            }";
     }


    if($js_settings['conditionDisableIfRefererContains'] == false){
        $conditionDisableIfRefererContains = "";
     } else {
        $strings = explode('<br/>', $js_settings['conditionDisableIfRefererContains']);
        $strings = json_encode($strings);

        $conditionDisableIfRefererContains = "if(zerobounce_redirectIsActivated){
            var a=document.createElement('a');
            a.href=document.referrer;
            a.hostname;

            var referers = $strings;
            if(document.referrer !== undefined && document.referrer.length > 0){
                for(var i = 0; i < referers.length; ++i){
                    if(document.referrer.indexOf(referers[i]) > -1){
                        zerobounce_redirectIsActivated = false;
                        break;
                    }
                }
            }
        }";
     }
     


     if($js_settings['conditionVisitedSeveralPages'] != false){
        $conditionVisitedSeveralPages = "";
     } else {
        $conditionVisitedSeveralPages = "if(zerobounce_redirectIsActivated){
            var a=document.createElement('a');
            a.href=document.referrer;
       

            if(document.referrer !== undefined && document.referrer.length > 0 && a.hostname == window.location.hostname){
                zerobounce_redirectIsActivated = false;
            }
        }";
     }


     if($js_settings['conditionEnableOnlyIfUrlContains'] == false){
        $conditionEnableOnlyIfUrlContains = "";
     } else {
        $strings = explode('<br/>', $js_settings['conditionEnableOnlyIfUrlContains']);
        $strings = json_encode($strings);

        $conditionEnableOnlyIfUrlContains = "if(zerobounce_redirectIsActivated){

            var url = window.location.href;

            var strings = $strings;
            var match = false;
            if(document.referrer !== undefined && document.referrer.length > 0){
                for(var i = 0; i < strings.length; ++i){
                    if(url.indexOf(strings[i]) > -1){
                        match = true;
                        break;
                    }
     
                }
            }

            if(!match){

                zerobounce_redirectIsActivated = false;
            } 
        }";
     }


     if($js_settings['conditionDisableIfUrlContains'] == false){
        $conditionDisableIfUrlContains = "";
     } else {
        $strings = explode('<br/>', $js_settings['conditionDisableIfUrlContains']);
        $strings = json_encode($strings);

        $conditionDisableIfUrlContains = "if(zerobounce_redirectIsActivated){
            var url = window.location.href;

            var strings = $strings;
            if(document.referrer !== undefined && document.referrer.length > 0){
                for(var i = 0; i < strings.length; ++i){
                    if(url.indexOf(strings[i]) > -1){
                       zerobounce_redirectIsActivated = false;
                    } 
                }
            }
        }";
     }



     if($js_settings['conditionByDevice'] != false){
        $desktop = ($js_settings['conditionByDevice'] == 'Desktop') ? 'true' : 'false';
        $mobil = ($js_settings['conditionByDevice'] == 'Mobil') ? 'true' : 'false';
        $conditionByDevice = "if(zerobounce_redirectIsActivated){
            var isMobil = js_zerobounce_isMobile();
           
            var conditionByDeviceDesktop = $desktop;
            var conditionByDeviceMobile = $mobil;

            if(isMobil && !conditionByDeviceMobile){
                zerobounce_redirectIsActivated = false;
            } else if(!isMobil && !conditionByDeviceDesktop){
                zerobounce_redirectIsActivated = false;
            }
        }";
     }

     if($js_settings['conditionRedirectMobileUrls'] == false){
        $conditionRedirectMobileUrls = "";
     } else {
        $strings = explode('<br/>', $js_settings['conditionRedirectMobileUrls']);
        $strings = json_encode($strings);
        if(count($strings) > 0){
            $conditionRedirectMobileUrls = "
                if(js_zerobounce_isMobile()){
                    urls = $strings;
                    var x = Math.floor(Math.random() * urls.length);
                    return urls[x];
                }";
        } else {
            $conditionRedirectMobileUrls = "";
        }

        
     }

     if($js_settings['conditionPercentOfVisitors'] != false){
        $percent = $js_settings['conditionPercentOfVisitors'];
        $conditionPercentOfVisitors = "if(zerobounce_redirectIsActivated){
            var percent = $percent;
            var x = Math.floor(Math.random() * 100);

            if(x > percent){
                zerobounce_redirectIsActivated = false;
            }
        }";
     }

     if($js_settings['conditionRedirectVisitorOnlyOnce'] != false){
        $time = $js_settings['conditionRedirectVisitorOnlyOnce'];
        $conditionRedirectVisitorOnlyOnceSetCookie = "zbsetCookie('bounced','bounced', $time);
            localStorage.setItem('zb', 'bounced');
        ";
        $conditionRedirectVisitorOnlyOnce = "
             if(zerobounce_redirectIsActivated && zbgetCookie('bounced') != null){
                zerobounce_redirectIsActivated = false;            
            }
            if(zerobounce_redirectIsActivated && localStorage.getItem('zb') != null){
                zerobounce_redirectIsActivated = false;            
            }
        ";
     }

     if($js_settings['conditionVisitorCloseIntention'] == true) {
        $conditionVisitorCloseIntention = "
            function addEvent(obj, evt, fn) {
                if (obj.addEventListener) {
                    obj.addEventListener(evt, fn, false);
                }
                else if (obj.attachEvent) {
                    obj.attachEvent('on' + evt, fn);
                }
            }

            addEvent(document, 'mouseout', function(e) {
                e = e ? e : window.event;
                var from = e.relatedTarget || e.toElement;
                if (!from || from.nodeName == 'HTML') {
                    js_zerobounce_evaluateRedirectIsActivated();
                    if(zerobounce_redirectIsActivated){
                        if(document.referrer.indexOf('wp-admin') > -1 || document.referrer.indexOf('wp-login') > -1) return;
                        $conditionRedirectVisitorOnlyOnceSetCookie 
                        
                        var targeturl = js_zerobounce_get_url();
                        if(targeturl == null){
                            return;
                        }
                        
                        window.location = targeturl;
                    }
                }
            });


        ";
     }

     

     /*
        advanced settings
    */

    $redirectByReferer = '';
    $redirectByCountry = '';
    $redirectbyCountryGetFunction = '';
    $redirectByTag = '';
    $redirectByCategory = '';

    if($js_settings['advancedSettingsRedirectByReferer'] != false){
        $groups = $js_settings['advancedSettingsRedirectByReferer'];
        $refToUrl = array();
        foreach($groups as $group){
            $refToUrl[] = $group;
        }
        $refToUrl = json_encode($refToUrl);

        $redirectByReferer = " var redByRef = $refToUrl;
                if(document.referrer !== undefined && document.referrer.length > 0){
                    for(var i = 0; i < redByRef.length; ++i){
                        if(document.referrer.indexOf(redByRef[i]['referer']) > -1){
                            return redByRef[i]['url'];
                        } 
                    }
                }
        ";
    }

    if($js_settings['advancedSettingsRedirectByCountry'] != false){
        $url = admin_url( 'admin-ajax.php' ).'?action=zbjs&task=getloc';
        $redirectbyCountryGetFunction = "
            
            function zeroBounceIpToCountry()
                {
                    if(typeof country_backup !== 'undefined'){
                            return country_backup;
                    }
                    
                        var xmlHttp = null;
                        var url = '$url';
                        xmlHttp = new XMLHttpRequest();
                        xmlHttp.open( 'GET', url, false );
                        xmlHttp.send( null );
                        return xmlHttp.responseText;
                   
                        
                    
                }
            var zerobounce_country = zeroBounceIpToCountry();";


        $groups = $js_settings['advancedSettingsRedirectByCountry'];
        $countryToUrl = array();
        foreach($groups as $group){
            $countryToUrl[] = $group;
        }
        $countryToUrl = json_encode($countryToUrl);

        $redirectByCountry = " var redByCountry = $countryToUrl;
                if(zerobounce_country != null &&  zerobounce_country != 'NULL'){
                    for(var i = 0; i < redByCountry.length; ++i){
                        if(redByCountry[i]['country'] == zerobounce_country){
                            return redByCountry[i]['url'];
                        }
                    }
                }
        ";
    }

    if($js_settings['advancedSettingsRedirectByTag'] != false){
        $groups = $js_settings['advancedSettingsRedirectByTag'];
        $postId = get_the_ID();

        $tagToUrl = array();
        foreach($groups as $group){
            $tagToUrl[] = $group;
        }
        $tagToUrl = json_encode($tagToUrl);

        $redirectByTag = " var redByTag = $tagToUrl;
                            var tags = zbjs.tags;
                for(var i = 0; i < redByTag.length; ++i){
                    if(tags.indexOf(redByTag[i]['tag']) > -1){
                        return redByTag[i]['url'];
                    } 
                }
        ";
    }

     if($js_settings['advancedSettingsRedirectByCategory'] != false){
        $groups = $js_settings['advancedSettingsRedirectByCategory'];
        $postId = get_the_ID();


        $categoryToUrl = array();
        foreach($groups as $group){
            $categoryToUrl[] = $group;
        }
        $categoryToUrl = json_encode($categoryToUrl);

        $redirectByCategory = " var redirectByCategory = $categoryToUrl;
                            var categories = zbjs.categories;
                for(var i = 0; i < redirectByCategory.length; ++i){
                    if(categories.indexOf(redirectByCategory[i]['Category']) > -1){
                        return redirectByCategory[i]['url'];
                    } 
                }
        ";
    }


    /*
        get url
    */
     

     $urls = '';
     if($js_settings['individual_url'] != false){
        $urls = $js_settings['individual_url'];
         $function_getUrl = "
            $redirectbyCountryGetFunction
            function js_zerobounce_get_url(){
                $redirectByReferer
                
                $redirectByCountry

                return '$urls';
            }";
     } else if ($js_settings['global_url'] != false){
        $urls = $js_settings['global_url'];
        $function_getUrl = "
            $redirectbyCountryGetFunction
            function js_zerobounce_get_url(){
                $redirectByReferer
               
                $redirectByCountry
                return '$urls';
            }";
     } else {

        $urls = json_encode($js_settings['global_urls']);
        $function_getUrl = "
            $redirectbyCountryGetFunction
            function js_zerobounce_get_url(){
                $redirectByReferer
                
                $redirectByCountry

                $redirectByTag

                $redirectByCategory

                var urls = null;

                $conditionRedirectMobileUrls

                urls = $urls;                
                var containsPercentage = false;
                for(var i=0; i< urls.length; i++){
                    if(urls[i].indexOf('\\\\') > -1){
                        containsPercentage = true;
                        break;
                    }
                }

                var x = null;
                if(containsPercentage){
                   

                    var sum = 0;
                    x = Math.floor(Math.random() * 100);

                    for(var i=0; i< urls.length; i++){
                        var split = urls[i].split('\\\\');
                        var url = split[0];

                        var percent = parseInt(split[1]);

                       sum += percent;
                       if(x <= sum){
                         
                            return url;
                       }
                    }

                } else {
                    if(urls.length == 0){
                        return null;
                    }
                    x = Math.floor(Math.random() * urls.length);
 
                    return urls[x];
                }                
            }";
     }
     

     
       
    return "
    var zerobounce_redirectIsActivated  = true;

    function zbsetCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = 'expires='+d.toUTCString();
        document.cookie = cname + '=' + cvalue + '; ' + expires;
    }

    function zbgetCookie(cname) {
        var name = cname + '=';
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return null;
    }

    $function_getUrl

    function js_zerobounce_isMobile() {
        var check = false;
        (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }

    $conditionTimeout

    $conditionVisitorCloseIntention


    function js_zerobounce_evaluateRedirectIsActivated(){
       
        $conditionReferrerContains
       
        $conditionDisableIfRefererContains
 
        $conditionVisitorFromExtern
 
        $conditionByDevice

        $conditionEnableOnlyIfUrlContains

        $conditionDisableIfUrlContains
 
        $conditionPercentOfVisitors
 
        $conditionRedirectVisitorOnlyOnce
 
        if(document.referrer.indexOf('wp-admin') > -1 || document.referrer.indexOf('wp-login') > -1) {
            zerobounce_redirectIsActivated = false;
        }

       
    }
    

    (function() {        
                  if (!window.addEventListener)
                      return;
                  var blockPopstateEvent = document.readyState!='complete';
                  window.addEventListener('load', function() {
                      setTimeout(function(){ blockPopstateEvent = false; }, 0);
                  }, false);
                  window.addEventListener('popstate', function(evt) {
                      if (blockPopstateEvent && document.readyState=='complete') {
                          evt.preventDefault();
                          evt.stopImmediatePropagation();
                      }
                  }, false);
    })();

    

    function zerobounce_setRedirect(){
        var rhash = '#forward';
        var currentUrl = window.location.href;
        var targeturl = js_zerobounce_get_url();
        js_zerobounce_evaluateRedirectIsActivated();
        if(!zerobounce_redirectIsActivated || targeturl == null) return;

        window.history.replaceState(null, null, currentUrl + rhash);
        window.history.pushState(null, null, currentUrl);

        window.addEventListener('popstate', function() {
          if (location.hash == rhash) {

                $conditionRedirectVisitorOnlyOnceSetCookie 
                
                history.replaceState(null, null, location.pathname);
                location.replace(targeturl);
               

               
          }
        });

    }
    zerobounce_setRedirect();

";
}



//checks if the necessary js file exist. If not it create a new one.
function zerobounce_createJS_File($extSiteId = null){
    global $wpdb;
    $js_settings = null;
    $md5_of_settings = null;

    if($extSiteId == null){
        $js_settings = zerobounce_get_js_settingsArray();
    } else { //external sites
        if(!is_int($extSiteId)){
            $extSiteId = intval($extSiteId);
        }
        
        $js_settings = zerobounce_get_js_settingsArray($extSiteId);
    }

    
    if($js_settings == false){
        return false;
    }

    $js = null;
    if($extSiteId == null){
        $md5_of_settings = md5(serialize($js_settings));
        $js = ZeroBounceDB_Js::get($md5_of_settings);
    }
   
    
    if($js == null){
        $jsCode = null;
        if($extSiteId == null){
            $jsCode = zerobounce_getJSCode();
        } else {
            $jsCode = zerobounce_getJSCode($js_settings);
        }
        
        $jsCode = zerobounce_encryptJS($jsCode);
    

        if($extSiteId == null){
            ZeroBounceDB_Js::add($md5_of_settings, $jsCode);
        } else {
            $md5_of_settings = ZeroBounceDB_ExtSites::getMD5($extSiteId);
           
            if($md5_of_settings != false){
            
                 $wpdb->show_errors();
                ZeroBounceDB_Js::add($md5_of_settings, $jsCode);
                
            }   
        }

        
    }

    return $md5_of_settings;
}


function zerobounce_encryptJS($js){
    $mainVar = "zerobounce_redirectIsActivated";
    $mainRandStr = zerobounce_generateRandomString(14);
    $js = str_replace($mainVar, $mainRandStr, $js);

    $country = "zerobounce_country";
    $countryRandStr = zerobounce_generateRandomString(14);
    $js = str_replace($country, $countryRandStr, $js);

    $matches = array();
    preg_match_all('/function ([a-zA-Z]|_| )*?\(\)/', $js , $matches);

    $matches = $matches[0];

    foreach($matches as $match){
        $function_Name = zerobounce_cleanMatch($match);
        $randomFunctionName = 'a'.sha1($match.mt_rand());
        $js = str_replace($function_Name, $randomFunctionName, $js);
    }

    return $js;
}

function zerobounce_cleanMatch($match){
    $pattern = '/(function |\(|\))/';
    return preg_replace($pattern, '', $match);
}



